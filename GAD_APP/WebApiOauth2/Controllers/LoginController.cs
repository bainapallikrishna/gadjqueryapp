﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace WebApiOauth2.Controllers
{
    public class LoginController : ApiController
    {
        #region User login
        [HttpGet]
        [Route("GetUserLogin_Details")]
        public IHttpActionResult GetUserLogin_Details(string Username, string Password)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                var useval = new clsQuery().DecryptStringAES(Username);
                var pawdval = new clsQuery().DecryptStringAES(Password);
                if (useval == "keyError" && pawdval == "keyError")
                {
                    objResponse.status = "4";
                    objResponse.reason = "UserName or Password incorrect";
                }
                else
                {
                    DataTable dt = new clsQuery().GetUserLogin_Data(username: useval, password: pawdval);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        objResponse.status = dt.Rows[0]["STATUS"].ToString();
                        objResponse.reason = "";
                        objResponse.userId = useval;
                        objResponse.pwd = pawdval;
                        
                    }
                    else
                    {
                        objResponse.status = "Fail";
                        objResponse.reason = "UserName or Password incorrect";
                    }

                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }
        }
        #endregion

        #region username loading
        [HttpGet]
        [Route("GetUserPermissionNames")]
        public IHttpActionResult GetUserNames()
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetUserName_Master();

                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.status = "Success";
                    objResponse.data = dt;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }
        #endregion

        #region Get user pages
        [HttpGet]
        [Route("GetUserPages")]
        public IHttpActionResult GetUserPages()
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetUserPages_Master();

                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.status = "Success";
                    objResponse.data = dt;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }


        #endregion

        [HttpPost]
        [Route("InsertUser")]
        public IHttpActionResult InsertUser(Insertuserscreenmodel _Userpagedata)
        {
            dynamic objResponse = new ExpandoObject();
            //var json = new JavaScriptSerializer().Serialize(_Userpagedata);
            try
            {
                //string mappath = HttpContext.Current.Server.MapPath("InsertUserLogs");
                //Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().Insert_userrolesData(_Userpagedata);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "Userpages Names Added Sucessfully";
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "Error While Submitting Details!!";
                }
            }
            catch (Exception ex)
            {
                //string mappath = HttpContext.Current.Server.MapPath("Exception_GuesthouseDailyStatusUpdateLogs");
                //Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                //objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("UpdateUser")]
        public IHttpActionResult UpdateUser(Insertuserscreenmodel _Userpagedata)
        {
            dynamic objResponse = new ExpandoObject();
            //var json = new JavaScriptSerializer().Serialize(_Userpagedata);
            try
            {
                //string mappath = HttpContext.Current.Server.MapPath("InsertUserLogs");
                //Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().Update_userrolesData(_Userpagedata);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "Userpages Names Delete Sucessfully";
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "Error While Submitting Details!!";
                }
            }
            catch (Exception ex)
            {
                //string mappath = HttpContext.Current.Server.MapPath("Exception_GuesthouseDailyStatusUpdateLogs");
                //Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                //objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }


        #region username loading
        [HttpGet]
        [Route("GetUserscreenmappedList")]
        public IHttpActionResult GetUserscreenmappedList(string USER_ID)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetUserscreenmappedList_Master(USER_ID);
                clsscreenmapDetails objres = new clsscreenmapDetails();
                objres.screenmapList = new List<clsscreenmapList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsscreenmapList _obj = new clsscreenmapList();
                        _obj.pagename = dr["PAGE_NAME"].ToString();
                        _obj.pageid = dr["PAGE_ID"].ToString();
                        _obj.groupid = dr["GROUP_ID"].ToString();
                        _obj.userid = dr["USER_ID"].ToString();
                        objres.screenmapList.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }
        #endregion
    }
}
