﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace WebApiOauth2.Controllers
{
    [Authorize]
    public class WebApiController : ApiController
    {
        #region Accommodation 

        [HttpGet]
        [Route("DropDowns")]
        public IHttpActionResult DropDowns(string type)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().LoadDropdowns(type);
                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("RoomList")]
        public IHttpActionResult RoomList(string bname, string rmtype, string cat)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().RoomList(bname, rmtype, cat);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("ROOM_ABSTRACT")]
        public IHttpActionResult ROOM_ABSTRACT(string bname, string cat)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().ROOM_ABSTRACT(bname, cat);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("ROOM_ABSTRACT_List")]
        public IHttpActionResult ROOM_ABSTRACT_List()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().ROOM_ABSTRACT_List();
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("Alloted_GET")]
        public IHttpActionResult Alloted_GET()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Alloted_GET();
                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }


        [HttpPost]
        [Route("RoomDetails_Update")]
        public IHttpActionResult RoomDetails_Update(clsRoomDetails obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("RoomDetailsUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().updateRoomDetails(obj);
                if (dt != null && dt.Rows.Count > 0 && dt.Rows[0][0].ToString() == "0")
                {
                    // foreach(string s in obj.AMENITIES.Split(','))
                    // {
                    dt = new clsQuery().updateRoom_Amenites(obj);
                    //}
                    if (dt != null && dt.Rows.Count > 0 && dt.Rows[0][0].ToString() == "0")
                    {
                        objResponse.data = dt;
                        objResponse.ResponseCode = 1;
                        objResponse.ResponseMessage = "";
                    }
                }
                else
                {
                    objResponse.ResponseCode = 0;
                    objResponse.ResponseMessage = "!Oops there was an error please try after some time...";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_RoomDetailsUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }
        #endregion

        #region Cabinet 



        [HttpGet]
        [Route("Cabinet_List")]
        public IHttpActionResult Cabinet_List(string fromdate, string todate, string deptid, string search)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Cabinet_List(fromdate, todate, deptid, search);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Add_New_Cabinet")]
        public IHttpActionResult Add_New_Cabinet(clsCabinet obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("CabinetInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Add_New_Cabinet(obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = dt.Rows[0]["status"].ToString();
                    objResponse.ResponseMessage = dt.Rows[0]["msg"].ToString();

                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error while Adding new Cabinet";
                }


            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_CabinetInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Update_Cabinet")]
        public IHttpActionResult Update_Cabinet(clsCabinet obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("CabinetUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Update_Cabinet(obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";

                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_CabinetUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }
        #endregion

        #region VIP
        [HttpGet]
        [Route("Category_details")]
        public IHttpActionResult Category_details()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Category_details();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Category_Add")]
        public IHttpActionResult Category_Add(clsSchedule obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("CategoryInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Create_new_schedule(obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Added Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_CategoryInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }
        [HttpPost]
        [Route("Category_Update")]
        public IHttpActionResult Category_Update(clsSchedule obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("CategoryUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Category_Update(obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Updated Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Updating please try after some time!!";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_CategoryUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Category_List")]
        public IHttpActionResult Category_List(string fromdate, string todate, string catid)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Category_List(fromdate, todate, catid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Category_List_report")]
        public IHttpActionResult Category_List_report(string fromdate, string todate, string catid)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Category_List_report(fromdate, todate, catid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    List<clsVipReport> obj = new System.Collections.Generic.List<clsVipReport>();
                    //obj.objVipReport = new System.Collections.Generic.List<clsVip>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        bool status = false;
                        if (obj == null || obj.Count == 0)
                        {

                            clsVip tempobj = new clsVip();
                            tempobj.date = dr["date"].ToString();
                            tempobj.Times = dr["start_time"].ToString() + " to " + dr["end_time"].ToString();
                            tempobj.Notice = dr["subject"].ToString();
                            tempobj.Category = dr["category_name"].ToString();

                            List<clsVip> tempobjlist = new List<clsVip>();
                            tempobjlist.Add(tempobj);
                            obj.Add(new clsVipReport()
                            {
                                Date = dr["date"].ToString(),
                                objVipReport = tempobjlist
                            });
                        }
                        else
                        {
                            foreach (clsVipReport o in obj)
                            {
                                if (o.Date == dr["date"].ToString())
                                {
                                    status = true;
                                    o.objVipReport.Add(new clsVip()
                                    {
                                        date = dr["date"].ToString(),
                                        Times = dr["start_time"].ToString() + " to " + dr["end_time"].ToString(),
                                        Category = dr["category_name"].ToString(),
                                        Notice = dr["subject"].ToString()
                                    });
                                }
                            }
                            if (status == false)
                            {
                                clsVip tempobj = new clsVip();
                                tempobj.date = dr["date"].ToString();
                                tempobj.Times = dr["start_time"].ToString() + " to " + dr["end_time"].ToString();
                                tempobj.Notice = dr["subject"].ToString();
                                tempobj.Category = dr["category_name"].ToString();

                                List<clsVip> tempobjlist = new List<clsVip>();
                                tempobjlist.Add(tempobj);
                                obj.Add(new clsVipReport()
                                {
                                    Date = dr["date"].ToString(),
                                    objVipReport = tempobjlist
                                });
                            }
                        }

                    }
                    objResponse.data = obj;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        #endregion

        #region Grievance
        [HttpPost]
        [Route("Grievance_Add")]
        public IHttpActionResult Grievance_Add(clsGrievance obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("GrievanceInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Grievance_add(obj, HttpContext.Current.Request.UserHostAddress);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Grievance Submitted Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Grievance Request.!!";
                }


            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_GrievanceInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }
        #endregion

        #region Save File
        [HttpPost]
        [Route("SaveFile_GrievanceFile")]
        public async Task<IHttpActionResult> PostFormData()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");

            var provider = new MultipartFormDataStreamProvider(root);
            dynamic objdata = new ExpandoObject();
            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);



                string pushdata = JsonConvert.SerializeObject(provider.FormData);
                NameValueCollection ob = provider.FormData;

                byte[] filedata = File.ReadAllBytes(provider.FileData[0].LocalFileName);

                string path = Save_file(filedata);




                if (path != null && path.Length > 0)
                {
                    objdata.Status = "Success";
                    objdata.Reason = "Data Inserted Successfully.";
                    objdata.filepath = path;
                    return Ok(objdata);
                }

                else
                {
                    objdata.Status = "Failure";
                    objdata.Reason = "Error while uploading file..";
                    objdata.filepath = "";
                    return Ok(objdata);
                }


            }
            catch (System.Exception e)
            {
                objdata.Status = "Failure";
                objdata.Reason = e.Message;
                objdata.filepath = "";
                return Ok(objdata);
            }

        }

        public string Save_file(byte[] base64, string type = "pdf")
        {
            try
            {

                try
                {
                    System.Threading.Thread.Sleep(1000);
                    string imagepath = "";

                    string directoryPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/GrievanceFiles/"));
                    Random rn = new Random();
                    int num = rn.Next(100000, 999999);
                    if (base64 != null)
                    {
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        using (MemoryStream ms = new MemoryStream(base64))
                        {
                            FileStream fs = new FileStream(directoryPath + "/" + num.ToString() + "." + type, FileMode.Create);
                            ms.WriteTo(fs);
                            ms.Close();
                            fs.Close();
                            fs.Dispose();
                            imagepath = directoryPath + "/" + num.ToString() + type;
                        }




                        return imagepath;
                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region File_Code_Master 
        [HttpGet]
        [Route("FH_Dept_Master")]
        public IHttpActionResult FH_Dept_Master()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Dept_get();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Unit_Details")]
        public IHttpActionResult UNIT_DETAILS()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().UNIT_DETAILS();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("File_code_details")]
        public IHttpActionResult File_code_details(string unit, string dept_code)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().File_code_details(dept_code, unit);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Employee_details")]
        public IHttpActionResult Employee_details(string dept_code)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Employee_details(dept_code);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Map_FileCode_details")]
        public IHttpActionResult Map_FileCode_details(string empno, string filecode, string peshi)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().map_file_code(empno, filecode, peshi);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Edit_FileCode_details")]
        public IHttpActionResult Edit_FileCode_details(string empno, string filecode, string mappedsno, string peshi)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().edit_file_code(empno, filecode, mappedsno, peshi);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }
        #endregion

        #region Sec Attendance 
        [HttpGet]
        [Route("Att_Dash_Board")]
        public IHttpActionResult Att_Dash_Board()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Sec_att_dashboard();
                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }
        #endregion

        #region E-Office 
        [HttpGet]
        [Route("Last_sync_date")]
        public IHttpActionResult Last_sync_date()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Last_sync_date();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Secretariat_departments_list")]
        public IHttpActionResult Secretariat_departments_list()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Secretariat_departments_list();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("file_disposal_report")]
        public IHttpActionResult file_disposal_report(string fromdate, string todate, string deptcode)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().file_disposal_report(fromdate, todate, deptcode);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        #endregion

        #region  AIS
        [HttpGet]
        [Route("Get_Service_Types")]
        public IHttpActionResult Service_types()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Service_types();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Get_Officers")]
        public IHttpActionResult Get_Officers(string servicetype, string searchkey)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Get_Officers(servicetype, searchkey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("Get_Officers_Posting")]
        public IHttpActionResult Get_Officers_Posting(string servicetype)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Get_Officers_Posting(servicetype);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        #endregion

        #region Guest House 

        [HttpGet]
        [Route("GuestHouse_Master")]
        public IHttpActionResult GuestHouse_Master(string type)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Master(type);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("GuestHouse_Category_Master")]
        public IHttpActionResult GuestHouse_Category_Master(string type)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Master(type);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("GuestHouse_User_Request")]
        public IHttpActionResult GuestHouse_User_Request(clsGuestHouse_Request objghouse)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(objghouse);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("GuestHouseUserRequestInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().GuestHouse_User_Request(objghouse);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Added Successfully !!";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "!Oops there was an error please try after some time....";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_GuestHouseUserRequestInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpPost]
        [Route("GuestHouse_Admin_approve")]
        public IHttpActionResult GuestHouse_Admin_approve(clsGuestHouse_Admin_Request objghouse)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(objghouse);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("GuestHouseAdminapproveLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().GuestHouse_Admin_approve(objghouse);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Approved Successfully !!";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "!Oops there was an error please try after some time....";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_GuestHouseAdminapproveLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("GuestHouse_RequestsList")]
        public IHttpActionResult GuestHouse_RequestsList()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Details("2");
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("GuestHouse_RequestsListByID")]
        public IHttpActionResult GuestHouse_RequestsListByID(string id)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Details("1", id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("GuestHouse_Request_Cancel")]
        public IHttpActionResult GuestHouse_Request_Cancel(string id, string reason)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Request_Cancel(id, reason);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Cancelled Successfully !!!";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("GuestHouse_Rooms_GET")]
        public IHttpActionResult GuestHouse_Rooms_GET(string gid)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Rooms_GET(gid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Cancelled Successfully !!!";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        #endregion

        #region Catering 
        [HttpPost]
        [Route("Catering_Add")]
        public IHttpActionResult Catering_Add(clsCatering obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("CateringInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Catering_Add(obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Catering Details Added Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_CateringInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("Catering_Details_Get")]
        public IHttpActionResult Catering_Details_Get()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                List<clsCatering> obj = new clsQuery().Catering_Details_Get();
                if (obj != null && obj.Count > 0)
                {
                    objResponse.data = obj;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available !";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("Catering_Details_Get_By_Id")]
        public IHttpActionResult Catering_Details_Get_By_Id(string id)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                List<clsitems> obj = new clsQuery().Catering_Details_Get_By_Id(id);
                if (obj != null && obj.Count > 0)
                {
                    objResponse.data = obj;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available !";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }
        #endregion

        #region Room_Master 
        [HttpPost]
        [Route("Room_Master_Add")]
        public IHttpActionResult Room_Master_Add(clsRoomMaster obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("RoomMasterInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Room_Master(obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Room Details Added Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }


            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_RoomMasterInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("Room_Master_Get")]
        public IHttpActionResult Room_Master_Get()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {

                DataTable dt = new clsQuery().Room_Master_Get();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "!Oops there was an error please try a";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("Room_Master_Get_count")]
        public IHttpActionResult Room_Master_Get_count()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {

                DataTable dt = new clsQuery().Room_Master_Get_count();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "!Oops there was an error please try a";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        #endregion

        #region ESR SEC
        [HttpGet]
        [Route("ESR_Officers_Master")]
        public IHttpActionResult ESR_Officers_Master(string role, string type)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {

                DataTable dt = new clsQuery().SEC_ESR_Officers_Master(role, type);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("ESR_employee_Details")]
        public IHttpActionResult ESR_employee_Details(string empid)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {

                DataTable dt = new clsQuery().SEC_ESR_employee_Details(empid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "!Oops there was an error please try a";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        #endregion

        #region State Guest house
        [HttpGet]
        [Route("StateGuestHouse_GET")]
        public IHttpActionResult StateGuestHouse_GET(string year, string month, string date)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().State_GuestHouse_GET(year, month, date);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Cancelled Successfully !!!";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }
        #endregion

        #region WEB State Guest

        [HttpPost]
        [Route("GetStateGuestsDetails")]
        public IHttpActionResult GetStateGuestsDetails(clsGetStateGuest root)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetStateGuestsData(root);
                clsStateGuest objres = new clsStateGuest();
                objres.stateGuestlists = new List<clsStateGuestlist>();
                if (dt != null && dt.Rows.Count > 0)
                {

                    objres.Status = "Success";
                    objres.Reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsStateGuestlist obj = new clsStateGuestlist();
                        obj.DateOfOrder = (_clsQuery.Replacedateformat(dr["DATE_OF_ISSUE"].ToString())).ToString();
                        obj.Guests = dr["GUESTS_DETAILS"].ToString();
                        obj.VisitPlaces = dr["PLACES_TO_VISIT"].ToString();
                        obj.Fromdate = (_clsQuery.Replacedateformat(dr["FROM_DATE"].ToString())).ToString();
                        obj.ToDate = (_clsQuery.Replacedateformat(dr["TO_DATE"].ToString())).ToString();
                        obj.id = dr["ID"].ToString();
                        obj.Remark = dr["NOTE"].ToString();
                        objres.stateGuestlists.Add(obj);
                    }
                }
                else
                {
                    objres.Status = "Fail";
                    objres.Reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.Status = "Fail";
                objResponse.Reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpPost]
        [Route("InsertStateGuestsDetails")]
        public IHttpActionResult InsertStateGuestsDetails(clsStateGuestlist root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("StateGuestsInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().InsertStateGuestData(root);
                if (Status == true)
                {
                    objResponse.Status = "Success";
                    objResponse.Reason = "";
                    objResponse.fromDate = root.Fromdate;
                }
                else
                {
                    objResponse.Status = "Fail";
                    objResponse.Reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_StateGuestsInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.Status = "Fail";
                objResponse.Reason = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("UpdateStateGuestsDetails")]
        public IHttpActionResult UpdateStateGuestsDetails(clsStateGuestlist root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("StateGuestsUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().UpdateStateGuestData(root);
                if (Status == true)
                {

                    objResponse.Status = "Success";
                    objResponse.Reason = "";
                    objResponse.fromDate = root.Fromdate;
                }
                else
                {
                    objResponse.Status = "Fail";
                    objResponse.Reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_StateGuestsUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.Status = "Fail";
                objResponse.Reason = ex.Message;
            }
            return Ok(objResponse);
        }

        #endregion

        #region Vehicles Transport Status 
        [HttpGet]
        [Route("GetTransportStatusDetails")]
        public IHttpActionResult GetVehicleStatusDetails()
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetVehicleStatusData();
                clsVehicleStatus objres = new clsVehicleStatus();
                objres.vehicleStatuslist = new List<clsVehicleStatuslist>();
                if (dt != null && dt.Rows.Count > 0)
                {

                    objres.status = "Success";
                    objres.reason = "";

                    foreach (DataRow dr in dt.Rows)
                    {
                        clsVehicleStatuslist obj = new clsVehicleStatuslist();

                        obj.vehicleno = dr["REGISTRATION_NO"].ToString();
                        obj.vehiclemodel = dr["VEHICLE_MODEL"].ToString();
                        obj.driver = dr["DRIVER"].ToString();
                        obj.vehicletype = dr["VEHICLE_TYPE"].ToString();
                        if (obj.driver == "")
                        {
                            obj.driver = "--";
                        }
                        obj.allotedname = dr["ALLOTTED_NAME"].ToString();
                        if (obj.allotedname == "")
                        {
                            obj.allotedname = "--";
                        }
                        obj.remark = dr["REMARKS"].ToString();
                        if (obj.remark == "")
                        {
                            obj.remark = "--";
                        }
                        objres.vehicleStatuslist.Add(obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetTransportVehiclesCounts")]
        public IHttpActionResult GetTransportVehiclesCounts(string type)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetTransportVehicleCounts(type);

                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.status = "Success";
                    objResponse.reason = "";
                    objResponse.condemnation = dt.Rows[0]["CONDEMNATION"].ToString();
                    objResponse.operationalvehicle = dt.Rows[0]["OPERATIONAL_VEHICLE"].ToString();
                    objResponse.to_be_condemned = dt.Rows[0]["TO_BE_CONDEMNED"].ToString();
                    objResponse.under_repair = dt.Rows[0]["UNDER_REPAIR"].ToString();
                    objResponse.total = dt.Rows[0]["TOTAL"].ToString();
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    clsvehiclecountlist obj = new clsvehiclecountlist();
                    //    obj.condemnation = dr["CONDEMNATION"].ToString();
                    //    obj.operationalvehicle = dr["OPERATIONAL_VEHICLE"].ToString();
                    //    obj.to_be_condemned = dr["TO_BE_CONDEMNED"].ToString();
                    //    obj.under_repair = dr["UNDER_REPAIR"].ToString();
                    //    obj.total = dr["TOTAL"].ToString();
                    //    objres.countlist.Add(obj);
                    //}
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetTransportIndividualData")]
        public IHttpActionResult GetTransportIndividualData(string ID)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetTransportVehicleCounts(ID);

                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.status = "Success";
                    objResponse.reason = "";
                    objResponse.data = dt;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                    objResponse.data = new DataTable();
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                objResponse.data = new DataTable();
                return Ok(objResponse);
            }

        }

        [HttpPost]
        [Route("UpdateVehiclestatusDetails")]
        public IHttpActionResult UpdateVehiclestatusDetails(clsVehicleStatuslist root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("VehiclesStatusUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().UpdateVehicleStatusData(root);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";

                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_VehiclesStatusUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }
        #endregion

        #region Web Guesthouse Daily Status Report
        [HttpGet]
        [Route("GetGuesthouseDRPDetails")]
        public IHttpActionResult GetGuesthouseDRPDetails(string type)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetGuesthouseDRPData(type);

                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.status = "Success";
                    objResponse.data = dt;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetGuesthouseRoomDetails")]
        public IHttpActionResult GetGuesthouseRoomDetails(string type, string ghsid)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GuestHouse_Room_Master(type, ghsid);

                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.status = "Success";
                    objResponse.data = dt;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetGuesthouseCounts")]
        public IHttpActionResult GetGuesthouseCounts(string ghs_id, string date)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetGuesthouse_CountsData(ghs_id, date);

                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.status = "Success";
                    objResponse.reason = "";

                    objResponse.vacant = dt.Rows[0]["VACANT"].ToString();
                    objResponse.occupied = dt.Rows[0]["OCCUPIED"].ToString();
                    objResponse.total = dt.Rows[0]["TOTAL"].ToString();
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetGuesthouse_Details")]
        public IHttpActionResult GetGuesthouse_Details(string ghs_id, string date)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetGuesthouse_Data(ghs_id, date);
                clsGuesthouseDaily objres = new clsGuesthouseDaily();
                objres.GuesthouseList = new List<clsGuesthouseList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsGuesthouseList _obj = new clsGuesthouseList();
                        _obj.roomtype = dr["ROOM_TYPE"].ToString();
                        _obj.roomno = dr["ROOM_NO"].ToString();
                        _obj.name_Guest = dr["NAME_OF_THE_GUEST"].ToString();
                        _obj.designation_Guest = dr["DESIGNATION_OF_GUEST"].ToString();
                        _obj.name_designation_Guest = dr["NAME_OF_THE_GUEST"].ToString() + "<br/>" + dr["DESIGNATION_OF_GUEST"].ToString();
                        _obj.dateofarrival = new clsQuery().ReplaceDDMMYYYYdateformat(dr["DATE_OF_ARRIAVAL"].ToString());
                        _obj.dateofdeparture = new clsQuery().ReplaceDDMMYYYYdateformat(dr["DATE_OF_DEPARTURE"].ToString());
                        _obj.category = dr["CATEGORY"].ToString();
                        _obj.reamrks = dr["REMARKS"].ToString();
                        if (dr["REMARKS"].ToString() == "O")
                        {
                            _obj.reamrks = "OCCUPIED";
                        }
                        else if (dr["REMARKS"].ToString() == "V")
                        {
                            _obj.reamrks = "VACANT";
                        }
                        _obj.ghsid = dr["GHS_ID"].ToString();
                        _obj.id = dr["ID"].ToString();
                        _obj.roomfloor = dr["ROOM_FLOOR"].ToString();


                        objres.GuesthouseList.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpPost]
        [Route("InsertGuesthousestatusDetails")]
        public IHttpActionResult InsertGuesthousestatusDetails(clsGuesthouseList root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("GuesthouseDailyStatusAddLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable Status = new clsQuery().ADD_GuesthouseStatusData(root);
                if (Status == null)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";
                    objResponse.gid = root.ghsid;
                }
                else if (Status != null && Status.Rows.Count > 0)
                {
                    objResponse.status = "2";
                    objResponse.reason = Status.Rows[0]["DATES"].ToString();
                    objResponse.gid = root.ghsid;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_GuesthouseDailyStatusAddLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }


        [HttpPost]
        [Route("UpdateGuesthousestatusDetails")]
        public IHttpActionResult UpdateGuesthousestatusDetails(clsGuesthouseList root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("GuesthouseDailyStatusUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().Update_GuesthouseStatusData(root);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";
                    objResponse.gid = root.ghsid;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_GuesthouseDailyStatusUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("GuesthouseCheckoutDetails")]
        public IHttpActionResult GuesthouseCheckoutDetails(clsGuesthousecheckoutList root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("GuesthouseCheckoutLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().GuesthouseCheckoutData(root);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";
                    objResponse.gid = root.ghsid;
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_GuesthouseCheckoutLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }

        #endregion

        #region Vehicles
        [HttpGet]
        [Route("Vehicles_Get")]
        public IHttpActionResult Vehicles_Get()
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().Vehicles_Get();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "!Oops there was an error please try again";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Add_Vehicle_Details")]
        public IHttpActionResult Add_Vehicle_Details(clsVehicleDetails obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {

                string mappath = HttpContext.Current.Server.MapPath("VehiclesInsertionLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Add_New_Vehicledetails(obj);
                if (dt != null && dt.Rows[0]["Column1"].ToString() == "saved sucessfully")
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Vehicle Details Added Sucessfully";
                }
                else if (dt != null && dt.Rows[0]["Column1"].ToString() == "coupon exists")
                {
                    objResponse.ResponseCode = 6;
                    objResponse.ResponseMessage = dt.Rows[0]["Column1"].ToString();
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }

            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_VehiclesInsertionLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Update_Vehicle_Details")]
        public IHttpActionResult Update_Vehicle_Details(clsVehicleDetailsUpdate obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("VehiclesUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().update_New_Vehicledetails(obj);
                if (dt == null)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Vehicle Details Updated Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }


            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_VehiclesUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("Year_Dropdown")]
        public IHttpActionResult Year_Dropdown(string type)
        {
            dynamic objResponse = new ExpandoObject();
            Connection clsCon = new Connection();
            clsQuery clsQuery = new clsQuery();
            try
            {
                DataTable dt = clsCon.retdt(clsQuery.Year_Get(type, ""), "");
                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Month_Dropdown")]
        public IHttpActionResult Month_Dropdown(clsYearMonth details)
        {
            dynamic objResponse = new ExpandoObject();




            try
            {
                DataTable dt = new clsQuery().month_Get(details);
                if (dt != null && dt.Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("BillReport")]
        public IHttpActionResult BillReport(string year, string month)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().BillReport(year, month);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("BillReport_Individual")]
        public IHttpActionResult BillReport_Individual(string year, string month, string vhl_no)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().BillReport_Individual(year, month, vhl_no);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpGet]
        [Route("POLIndentReport")]
        public IHttpActionResult POL_Indent_Report(string year, string month)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().POL_Indent_Report(year, month);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("Add_expenditure_Details")]
        public IHttpActionResult Add_expenditure_Details(clsExpenditureDetails obj)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(obj);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("AddexpenditureLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                DataTable dt = new clsQuery().Add_New_Expenditure_Details(obj);
                if (dt == null)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "Details Saved Sucessfully";
                }

                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_AddexpenditureLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        [HttpGet]
        [Route("ExpenditureReport")]
        public IHttpActionResult ExpenditureReport(string year, string month)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().ExpenditureReport(year, month);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [Route("Update_bill_Details")]
        public IHttpActionResult Update_bill_Details(clsbillDetailsUpdate obj)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().update_billdetails(obj);
                if (dt == null)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.reg_no = obj.REGISTRATION_NO;
                    objResponse.ResponseMessage = "Details Updated Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }
        [HttpGet]
        [Route("ExpenditureReport_Individual")]
        public IHttpActionResult ExpenditureReport_Individual(string year, string month, string vhl_no)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataSet dt = new clsQuery().ExpenditureReport_Individual(year, month, vhl_no);
                if (dt != null && dt.Tables[0].Rows.Count > 0)
                {

                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.ResponseMessage = "";
                }
                else
                {

                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;
            }
            return Ok(objResponse);
        }

        [Route("Update_expenditure_Details")]
        public IHttpActionResult Update_expenditure_Details(clsexpenditureDetailsUpdate obj)
        {
            dynamic objResponse = new ExpandoObject();
            try
            {
                DataTable dt = new clsQuery().update_expendituredetails(obj);
                if (dt == null)
                {
                    objResponse.data = dt;
                    objResponse.ResponseCode = 1;
                    objResponse.reg_no = obj.REGISTRATION_NO;
                    objResponse.ResponseMessage = "Details Updated Sucessfully";
                }
                else
                {
                    objResponse.ResponseCode = 7;
                    objResponse.ResponseMessage = "Error While Submitting Details!!";
                }


            }
            catch (Exception ex)
            {
                objResponse.ResponseCode = 0;
                objResponse.ResponseMessage = ex.Message;

            }
            return Ok(objResponse);
        }


        #endregion

        #region Web Inventory

        [HttpGet]
        [Route("GetInventory_Details")]
        public IHttpActionResult GetInventory_Details()
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetInventory_Data();
                clsInventoryDetails objres = new clsInventoryDetails();
                objres.inventoryList = new List<clsinventoryList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsinventoryList _obj = new clsinventoryList();
                        _obj.categoryid = dr["CATEGORY_ID"].ToString();
                        _obj.categoryname = dr["CATEGORY"].ToString();
                        _obj.itemid = dr["ITEM_ID"].ToString();
                        _obj.itemname = dr["ITEM_NAME"].ToString();
                        _obj.v01name = dr["V01_NAME"].ToString();
                        _obj.v01maker = dr["V01_MAKER"].ToString();
                        _obj.v01price = dr["V01_PRICE"].ToString();
                        _obj.v01status = dr["V01_STATUS"].ToString();
                        _obj.v02name = dr["V02_NAME"].ToString();
                        _obj.v02maker = dr["V02_MAKER"].ToString();
                        _obj.v02price = dr["V02_PRICE"].ToString();
                        _obj.v02status = dr["V02_STATUS"].ToString();
                        objres.inventoryList.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetMobInventory_Details")]
        public IHttpActionResult GetMobInventory_Details(string Categoryid, string Itemid)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetMobInventory_Data(Categoryid, Itemid);
                clsInventoryDetails objres = new clsInventoryDetails();
                objres.inventoryList = new List<clsinventoryList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsinventoryList _obj = new clsinventoryList();
                        _obj.categoryid = dr["CATEGORY_ID"].ToString();
                        _obj.categoryname = dr["CATEGORY"].ToString();
                        _obj.itemid = dr["ITEM_ID"].ToString();
                        _obj.itemname = dr["ITEM_NAME"].ToString();
                        _obj.v01name = dr["V01_NAME"].ToString();
                        _obj.v01maker = dr["V01_MAKER"].ToString();
                        _obj.v01price = dr["V01_PRICE"].ToString();
                        _obj.v01status = dr["V01_STATUS"].ToString();
                        _obj.v02name = dr["V02_NAME"].ToString();
                        _obj.v02maker = dr["V02_MAKER"].ToString();
                        _obj.v02price = dr["V02_PRICE"].ToString();
                        _obj.v02status = dr["V02_STATUS"].ToString();
                        objres.inventoryList.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetItemCategoryDrpdwn")]
        public IHttpActionResult GetItemCategoryDrpdwn()
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetItemCategoryDrpdwnlist();
                clsItemCategorydrp objres = new clsItemCategorydrp();
                objres.itemCategorylist = new List<clsItemCategorylist>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsItemCategorylist _obj = new clsItemCategorylist();
                        _obj.category = dr["CATEGORY"].ToString();
                        _obj.categoryid = dr["CATEGORY_ID"].ToString();
                        objres.itemCategorylist.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }
        }

        [HttpGet]
        [Route("GetItemSDrpdwn")]
        public IHttpActionResult GetItemSDrpdwn(string Categoryid)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetItemSDrpdwnlist(Categoryid);
                clsItemsdrp objres = new clsItemsdrp();
                objres.itemslist = new List<clsItemSlist>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsItemSlist _obj = new clsItemSlist();
                        _obj.itemname = dr["ITEM_NAME"].ToString();
                        _obj.itemid = dr["ITEM_ID"].ToString();
                        objres.itemslist.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }
        }

        [HttpGet]
        [Route("GetVendorDetails")]
        public IHttpActionResult GetVendorDetails()
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetVendorlist();

                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.status = "Success";
                    objResponse.vendornames = dt;
                    objResponse.reason = "";
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }
        }

        [HttpPost]
        [Route("InsertinventoryDetails")]
        public IHttpActionResult InsertinventoryDetails(clsinventoryList root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("inventoryInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().InsertInventoryData(root);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_inventoryInsertLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }

        [HttpPost]
        [Route("UpdateInventoryDetails")]
        public IHttpActionResult UpdateInventoryDetails(clsinventoryList root)
        {
            dynamic objResponse = new ExpandoObject();
            var json = new JavaScriptSerializer().Serialize(root);
            try
            {
                string mappath = HttpContext.Current.Server.MapPath("InventoryUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log(json, mappath));
                bool Status = new clsQuery().UpdateInventoryData(root);
                if (Status == true)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
            }
            catch (Exception ex)
            {
                string mappath = HttpContext.Current.Server.MapPath("Exception_InventoryUpdateLogs");
                Task WriteTask = Task.Factory.StartNew(() => new clsQuery()._Log("Exception Mesg:" + ex.Message + "," + json, mappath));
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
            }
            return Ok(objResponse);
        }
        #endregion

        #region FTR FORM 16 Report
        [HttpGet]
        [Route("GetFTRReport_Details")]
        public IHttpActionResult GetFTRReport_Details(string year, string month)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetFTRReport_Data(year, month);

                if (dt != null && dt.Rows.Count > 0)
                {
                    objResponse.status = "Success";
                    objResponse.reason = "";
                    objResponse.applied = dt.Rows[0]["applied"].ToString();
                    objResponse.approved = dt.Rows[0]["approved"].ToString();
                }
                else
                {
                    objResponse.status = "Fail";
                    objResponse.reason = "No Data Found";
                }
                return Ok(objResponse);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        [HttpGet]
        [Route("GetFTRappliedReport_Details")]
        public IHttpActionResult GetFTRappliedReport_Details(string year, string month)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetFTRformappliedReport_Data(year, month);
                clsFTRappliedDetails objres = new clsFTRappliedDetails();
                objres.ftrappliedlist = new List<clsFtrappliedList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsFtrappliedList _obj = new clsFtrappliedList();
                        _obj.refnum = dr["ref_no"].ToString();
                        _obj.formnum = dr["form_no"].ToString();
                        _obj.departname = dr["Departmentname"].ToString();
                        _obj.personcount = dr["person_count"].ToString();
                        _obj.createddate = dr["created_date"].ToString();
                        _obj.subject = dr["Subject"].ToString();
                        objres.ftrappliedlist.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }


        [HttpGet]
        [Route("GetFTRapprovedReport_Details")]
        public IHttpActionResult GetFTRapprovedReport_Details(string year, string month)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetFTRformapprovedReport_Data(year, month);
                clsFTRapprovedDetails objres = new clsFTRapprovedDetails();
                objres.ftrapprovedlist = new List<clsFtrapprovedList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsFtrapprovedList _obj = new clsFtrapprovedList();
                        _obj.refnum = dr["ref_no"].ToString();
                        _obj.formnum = dr["form_no"].ToString();
                        _obj.departname = dr["Departmentname"].ToString();
                        _obj.personcount = dr["person_count"].ToString();
                        _obj.createddate = dr["created_date"].ToString();
                        _obj.subject = dr["Subject"].ToString();
                        objres.ftrapprovedlist.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }


        [HttpGet]
        [Route("GetFTRgroupmembersReport_Details")]
        public IHttpActionResult GetFTRgroupmembersReport_Details(string formnum)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetFTRgroupmembersReport_Data(formnum);
                clsFTRmemberDetails objres = new clsFTRmemberDetails();
                objres.ftrmemberlist = new List<clsFtrmemberList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsFtrmemberList _obj = new clsFtrmemberList();
                        _obj.gdname = dr["Gd_name"].ToString();
                        _obj.gdservice = dr["Gd_Service"].ToString();
                        _obj.gduid = dr["Gd_UID"].ToString();
                        _obj.gddepartment = dr["Gd_Department"].ToString();
                        _obj.gddesignation = dr["Gd_Designation"].ToString();
                        objres.ftrmemberlist.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }

        #endregion

        #region Dynamic Menu After Login
        [HttpGet]
        [Route("GetLoginMenu_Details")]
        public IHttpActionResult GetLoginMenu_Details(string Username)
        {
            dynamic objResponse = new ExpandoObject();
            clsQuery _clsQuery = new clsQuery();
            try
            {
                DataTable dt = new clsQuery().GetLoginMenu_Data(Username);
                clsLoginMenuDetails objres = new clsLoginMenuDetails();
                objres.menuList = new List<clsmenuList>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    objres.status = "Success";
                    objres.reason = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        clsmenuList _obj = new clsmenuList();
                        _obj.userid = dr["USER_ID"].ToString();
                        _obj.username = dr["USERNAME"].ToString();
                        _obj.url = dr["URL"].ToString();
                        _obj.groupid = dr["GROUP_ID"].ToString();
                        _obj.groupname = dr["GROUP_NAME"].ToString();
                        _obj.pageid = dr["PAGE_ID"].ToString();
                        _obj.pagename = dr["PAGE_NAME"].ToString();
                        if (dr["GROUP_NAME"].ToString()== "HRMS")
                        {
                            if (dr["URL"].ToString()== "Officers")
                            {
                                _obj.url = "#";
                            }
                            else if (dr["URL"].ToString() == "Postings")
                            {
                                _obj.url = "#";
                            }
                            else if (dr["URL"].ToString() == "Vacancies")
                            {
                                _obj.url = "#";
                            }
                            else if (dr["URL"].ToString() == "Leaves")
                            {
                                _obj.url = "#";
                            }
                            else if (dr["URL"].ToString() == "ACR")
                            {
                                _obj.url = "#";
                            }
                        }
                        objres.menuList.Add(_obj);
                    }
                }
                else
                {
                    objres.status = "Fail";
                    objres.reason = "No Data Found";
                }
                return Ok(objres);
            }
            catch (Exception ex)
            {
                objResponse.status = "Fail";
                objResponse.reason = ex.Message;
                return Ok(objResponse);
            }

        }
        #endregion



    }
}
