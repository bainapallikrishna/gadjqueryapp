﻿var data;
var curr_filecode;
var mapped_sno;
$(document).ready(function () {
    $("#btn_update").hide();

    dept_master();
    unit_details();

    $("#ddl_Peshi").val('0');

    $("#ddl_dept").change(function (e) {
        loademp();
    })

    $("#ddl_unit").change(function (e) {
        load_file_codes();
    })

    $("#btn_submit").click(function (e) {
        File_Map();
    });

    $("#btn_update").click(function (e) {
        File_Map_update();
    });
    
});


function dept_master() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/FH_Dept_Master",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].deptcode + "'>" + response.data[index].deptname + "</option>"
            }
            $("#ddl_dept").html(html);
            $('#ddl_dept').selectpicker();
        } else {
            $("#ddl_dept").html(html);
            $('#ddl_dept').selectpicker();
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function unit_details() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Unit_Details",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].unit_id + "'>" + response.data[index].unit_name + "</option>"
            }
            $("#ddl_unit").html(html);

        } else {
            $("#ddl_unit").html('');

            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function loademp() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Employee_details?dept_code=" + $("#ddl_dept").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {

            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].sno + "'>" + response.data[index].firstname +" "+ response.data[index].lastname + "</option>"
            }
            $("#ddl_emp").html(html);
            $('#ddl_emp').selectpicker();
            data = response.data;

            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": false,
                "pageLength": 10,
                data: response.data,
                columns: [


                    { data: "firstname" },
                    { data: "lastname" },
                    { data: "designation" },
                    { data: "unit_name" },
                    { data: "file_code" },
                {
                    "data": "Sno",
                    "type": "sno",
                    "render": function (data, type, row, meta) {
                        if (row.mapping_status == 1) {
                            data = "<ul class='list-inline'><li class='list-inline-item'><a href='#' class='edit' Sno='" + row.sno + "' ><i class='far fa-edit'></i></a></li></ul>";
                        } else {
                            data = "-"
                        }
                        return data;
                    }
                }
                ]
            });
            $('#dataTable').on('click', '.edit', function () {
                Edit_details($(this).attr('Sno'));
            });
        } else {
            $("#ddl_emp").html('');
            $('#ddl_emp').selectpicker();
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function load_file_codes() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/File_code_details?unit=" + $("#ddl_unit").val() + "&dept_code=" + $("#ddl_dept").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {

            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].sno + "'>" + response.data[index].file_code + "</option>"
            }
            $("#ddl_filecodes").html(html);
            if (curr_filecode != undefined) {
                $("#ddl_filecodes").val(curr_filecode);
            }

        } else {
            $("#ddl_filecodes").html('');

            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function File_Map() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Map_FileCode_details?empno=" + $("#ddl_emp").val() + "&filecode=" + $("#ddl_filecodes").val() + "&peshi=" + $("#ddl_Peshi").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            loademp();
            $("#ddl_unit").val('');
            $("#ddl_filecodes").val('');
        } else {
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function File_Map_update() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Edit_FileCode_details?empno=" + $("#ddl_emp").val() + "&filecode=" + $("#ddl_filecodes").val() + "&mappedsno="+mapped_sno+ "&peshi=" + $("#ddl_Peshi").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            loademp();
            $("#ddl_unit").val('');
            $("#ddl_filecodes").val('');
        } else {
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Edit_details(sno) {
    var obj;
    for (var index = 0; index < data.length; index++) {
        if (sno == data[index].sno) {
            obj = data[index];
            break;
        }
    }

    $("#ddl_emp").val(obj.sno);
    $("#ddl_emp").selectpicker('refresh');

    $("#ddl_unit").val(obj.unit_id.toLowerCase());

    curr_filecode = obj.file_code_sno;
    load_file_codes();
    mapped_sno = obj.mapping_sno;

    if (obj.is_peshi != undefined && obj.is_peshi != '') {
        $("#ddl_Peshi").val(obj.is_peshi);
    }

    $("#btn_submit").hide();
    $("#btn_update").show();
    
}