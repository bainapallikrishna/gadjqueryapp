﻿var obj = {
    "sno": "",
    "Date": "",
    "Cr_no": "",
    "Dept_id": "",
    "Title": "",
    "Resolution": "",
    "Status": "",
    "Remarks": ""
}
var bdata = "";
var curr_sno;
$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#Cabinet').addClass('active');

    loaddept();

    $("#impdate").hide();
    $("#btncreatenew").click(function () {
        $("#txtcrnum").val('');
        $("#txtmod_date").val('');
        $("#txtmd_dept").val('');
        $("#txtremarks").val('');
        $("#txt_resultion").val('');
        $("#txt_status").val('');
        $("#txttitle").val('');
        $("#AddCabinet").modal('show');
    })

    //$("#ddl_status").change(function () {
    //    if ($("#ddl_status").val() == "1") {
    //        $("#impdate").show();
    //    } else {
    //        $("#impdate").hide();
    //    }
    //})
    // Add New Cabinet 
    $("#btn_save").click(function (e) {

        clearValues();

        obj.Cr_no = $("#txtcrnum").val();
        obj.Date = $("#txtmod_date").val();
        obj.Dept_id = $("#txtmd_dept").val();
        obj.Remarks = $("#txtremarks").val();
        obj.Resolution = $("#txt_resultion").val();
        obj.Status = $("#txt_status").val();
        obj.Title = $("#txttitle").val();
        obj.sno = "";
        Cabinet_add(obj);
    })


    // Update New Cabinet 
    $("#btn_update_save").click(function (e) {

        clearValues();


        obj.Title = $("#txtedtitle").val();
        obj.Resolution = $("#txt_edresultion").val();
        obj.Status = $("#txt_edstatus").val();
        obj.Remarks = $("#txtedremarks").val();
        obj.sno = curr_sno;
        Update_Cabinet(obj);
    })

    // Load Cabinet Details
    $("#btnsearch").click(function (e) {
        Load_Cabinet_Details();
    })
});
function clearValues() {
    obj.Cr_no = "";
    obj.Date = "";
    obj.Dept_id = "";
    obj.Remarks = "";
    obj.Resolution = "";
    obj.sno = "";
    obj.Status = "";
    obj.Title = "";

}

function loaddept() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=5",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].orG_CODE + "'>" + response.data[index].orG_NAME + "</option>"
            }
            $("#txtdept,#txtmd_dept").html(html);
            $('#txtdept,#txtmd_dept').selectpicker();
        } else {
            $("#txtdept,#txtmd_dept").html(html);
            $('#txtdept,#txtmd_dept').selectpicker();
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Cabinet_add(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Add_New_Cabinet",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);

        if (response.responseCode == 1) {
            alert(response.responseMessage);
            $("#AddCabinet").modal('hide');
            Load_Cabinet_Details();
        } else {
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Update_Cabinet(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Update_Cabinet",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.responseCode == 1) {
            alert("SUCCESS");
            $("#EditCabinet").modal('hide');
            Load_Cabinet_Details();
        } else {
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Load_Cabinet_Details() {
    var query = "fromdate=" + $("#txt_fromdate").val() + "&todate=" + $("#txt_todate").val() + "&deptid=" + $("#txtdept").val() + "&search=''";
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Cabinet_List?" + query,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            bdata = response.data;
            var i = 1;
            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },

                    { data: "orG_NAME" },
                    { data: "cr_no" },
                    { data: "date" },
                    { data: "title" },
                    { data: "resolution" },
                    { data: "status" },
                    { data: "remarks" },



                {
                    "data": "sno",
                    "type": "sno",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='edit' sno='" + row.sno + "' data-toggle='modal' data-target='#exampleModal'><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                CabinetDetails_edit($(this).attr('sno'));
            });
        } else {
            $('#dataTable').DataTable().clear().destroy();
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function CabinetDetails_edit(sno) {


    $("#txtcrnum").val('');
    $("#txtmod_date").val('');
    $("#txtmd_dept").val('');
    $("#txtremarks").val('');
    $("#txt_resultion").val('');
    $("#txt_status").val('');
    $("#txttitle").val('');


    var obj;
    curr_sno = sno;
    for (var index = 0; index < bdata.length; index++) {
        if (bdata[index].sno == sno) {
            obj = bdata[index];
            break;
        }

    }

    $("#EditCabinet").modal('show');

    $("#txtedtitle").val(obj.title);
    $("#txt_edresultion").val(obj.resolution);
    $("#txt_edstatus").val(obj.status);
    $("#txtedremarks").val(obj.remarks);

}