﻿var dataloaded = false;
var bdata;

var roomdetails = {
    "BULDING_NO": "",
    "FLOOR": "",
    "DEPT_CODE": "",
    "ROOM_NO": "",
    "ROOM_TYPE": "",
    "ROOM_CATEGORY": "",
    "ROOM_ALLOTTED": "",
    "NOTE": "",
    "ROOM_MODIFICATION": "",
    "AMENITIES": ""

}
$(document).ready(function () {
    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#Accommodation').addClass('active');

    LoadBuildingDropdown();
    LoadRoomDropdown();
    loadroomcat();
    loadRoomType();
    loadAlloted_GET();
    loaddept();
    loadAmenities()

    $("#ddl_room").hide();
    $("#ddl_rm_cat").hide();

    $("#ddl_building").change(function (e) {
        if ($("#ddl_building").val() != "0") {
            if (dataloaded == false) {
                //LoadRoomDropdown();
                // loadroomcat();
                dataloaded = true;
            }
            $("#ddl_room").show();
            $("#ddl_rm_cat").show();
        } else {
            $("#ddl_room").hide();
            $("#ddl_rm_cat").hide();
        }
        loadReport();
    })
    $("#ddl_room,#ddl_rm_cat").change(function (e) {
        loadReport();
    });


    $("#btn_save").click(function (e) {

        Cleardetails();

        roomdetails.BULDING_NO = $('#txtbnum').val();
        roomdetails.FLOOR = $('#txtfrnum').val();
        roomdetails.DEPT_CODE = $('#txtdept').val();
        roomdetails.ROOM_NO = $('#txtrm_num').val();
        roomdetails.ROOM_TYPE = $('#ddl_rm_type').val();
        roomdetails.ROOM_CATEGORY = $('#ddl_rm_cat_modal').val();
        roomdetails.ROOM_ALLOTTED = $('#ddl_rm_alloted').val();
        roomdetails.NOTE = $('#txt_note').val();
        roomdetails.ROOM_MODIFICATION = $('#txtmod_date').val();
        roomdetails.AMENITIES = $('#txtamen').val().toString();

        RoomDetails_Update();
    })
});
function Cleardetails() {
    roomdetails.BULDING_NO = "";
    roomdetails.FLOOR = "";
    roomdetails.DEPT_CODE = "";
    roomdetails.ROOM_NO = "";
    roomdetails.ROOM_TYPE = "";
    roomdetails.ROOM_CATEGORY = "";
    roomdetails.ROOM_ALLOTTED = "";
    roomdetails.NOTE = "";
    roomdetails.ROOM_MODIFICATION = "";
    roomdetails.AMENITIES = "";
}

function LoadBuildingDropdown() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=1",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].buldinG_SHORT + "'>" + response.data[index].buldinG_FULL + "</option>"
            }
            $("#ddl_building").html(html);
        } else {
            $("#ddl_building").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function LoadRoomDropdown() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=2",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //  console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].statuS_ID + "'>" + response.data[index].status + "</option>"
            }
            $("#ddl_room").html(html);
        } else {
            $("#ddl_room").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loadroomcat() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=3",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_CAT_ID + "'>" + response.data[index].rooM_CATEGORY + "</option>"
            }
            $("#ddl_rm_cat,#ddl_rm_cat_modal").html(html);
        } else {
            $("#ddl_rm_cat,#ddl_rm_cat_modal").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loadRoomType() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=4",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_TYPE_ID + "'>" + response.data[index].rooM_TYPE + "</option>"
            }
            $("#ddl_rm_type").html(html);
        } else {
            $("#ddl_rm_type").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loadAlloted_GET() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Alloted_GET",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].id + "'>" + response.data[index].name + "</option>"
            }
            $("#ddl_rm_alloted").html(html);
            $('#ddl_rm_alloted').selectpicker();
        } else {
            $("#ddl_rm_alloted").html(html);
            $('#ddl_rm_alloted').selectpicker();
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loadReport() {
    var query = "bname=";
    query += $("#ddl_building").val();

    query += "&rmtype=";
    query += $("#ddl_room").val() == undefined ? 0 : $("#ddl_room").val();

    query += "&cat=";
    query += $("#ddl_rm_cat").val() == undefined ? 0 : $("#ddl_rm_cat").val();




    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/RoomList?" + query,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            bdata = response.data.table;
            var i = 1;
            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data.table,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },

                    { data: "depT_CODE" },
                    { data: "rooM_NO" },
                    { data: "rooM_TYPE" },
                    { data: "rooM_CATEGORY" },
                    { data: "rooM_ALLOTTED" },
                    { data: "rooM_AMENITIES" },
                    { data: "note" },



                {
                    "data": "rooM_NO",
                    "type": "name",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='edit' rooM_NO='" + row.rooM_NO + "' data-toggle='modal' data-target='#exampleModal'><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                RoomDetails_edit($(this).attr('rooM_NO'));
            });
            $("#all").html(response.data.table1[0].alL_ROOMS);
            $("#occ").html(response.data.table1[0].occupieD_ROOMS);
            $("#vac").html(response.data.table1[0].vacanT_ROOMS);

        } else {
            $("#all").html(0);
            $("#occ").html(0);
            $("#vac").html(0);

            $('#dataTable').DataTable().clear().destroy();
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loaddept() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=5",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].orG_CODE + "'>" + response.data[index].orG_NAME + "</option>"
            }
            $("#txtdept").html(html);
            $('#txtdept').selectpicker();
        } else {
            $("#txtdept").html(html);
            $('#txtdept').selectpicker();
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loadAmenities() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/DropDowns?type=6",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].amenitieS_ID + "'>" + response.data[index].amenities + "</option>"
            }
            $("#txtamen").html(html);
            $('#txtamen').SumoSelect({
                forceCustomRendering: true,
                search: true
            });
        } else {
            $("#txtamen").html(html);

            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function RoomDetails_edit(rooM_NO) {
    //alert(rooM_NO);

    $("#exampleModal input[type='text'],select").val('')

    $("#txtdept").val(0)
    $("#txtdept").selectpicker('refresh');

    $("#ddl_rm_alloted").val(0)
    $("#ddl_rm_alloted").selectpicker('refresh');

    $("#txtamen").SumoSelect({
        forceCustomRendering: true,
        selectAll: true
    });
    $('#txtamen')[0].sumo.reload();

    $("#exampleModal").modal("show");
    for (var index = 0; index < bdata.length; index++) {

        if (bdata[index].rooM_NO == rooM_NO) {
            var obj = bdata[index];
            console.log(obj);
            $("#txtbnum").val(obj.buldinG_NO);
            $("#txtfrnum").val(obj.floor)


            if (obj.depT_CODE != undefined && obj.depT_CODE != null) {
                $("#txtdept").val($.trim(obj.depT_CODE.split('-')[0]));
            }
            $("#txtdept").selectpicker('refresh');
            $("#txtrm_num").val(obj.rooM_NO)

            $("#ddl_rm_type").val(obj.rooM_TYPE_ID)
            $("#ddl_rm_cat_modal").val($.trim(obj.rooM_CAT_ID))

            $("#ddl_rm_alloted").val(obj.namE_ID)

            //$("#ddl_rm_alloted option").each(function () {
            //    if ($.trim($(this).text()) == $.trim(obj.rooM_ALLOTTED)) {
            //        $(this).attr('selected', 'selected');
            //    }
            //});

            $("#ddl_rm_alloted").selectpicker('refresh');


            var data = obj.rooM_AMENITIES.split(',');
            var valarr = [];
            for (var i = 0; i < data.length; i++) {
                $("#txtamen option").each(function () {
                    if (($.trim($(this).text()) == ($.trim(data[i])))) {
                        $(this).attr('selected', 'selected');
                        valarr.push($(this).val());
                    }
                });
            }
            $('#txtamen').val(valarr);
            $('#txtamen')[0].sumo.reload();
            //setTimeout(function () {
            //    $("#txtamen").SumoSelect({
            //        forceCustomRendering: true,
            //        selectAll: true
            //    });

            //}, 50);
            //$('#txtamen').SumoSelect({
            //    forceCustomRendering: true,
            //    search: true
            //});


            //$("#txtamen").val(obj.rooM_AMENITIES)

            $("#txtmod_date").val(obj.rooM_MODIFICATION)
            $("#txt_note").val(obj.note)







        }
    }
}

function RoomDetails_Update() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/RoomDetails_Update",
        "method": "POST",
        "data": JSON.stringify(roomdetails),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        if (response.responseCode == 1) {
            $("#exampleModal").modal("hide");
            alert(response.data[0].msg);
            window.location.reload();
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}