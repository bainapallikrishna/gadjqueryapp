﻿$(document).ready(function () {
    $("#ddl_UserName").select2();
    $("#submitpages").hide();
    var checkarray = [];
    loadUserNames();
   // GetUserscreenmappedList();
    $("#getpages").click(function () {
        if ($('#ddl_UserName').val() == "0") {
            _n_plain_mes_1("<strong>info!</strong>", "Please select User Name", "danger"); $('#ddl_UserName').focus(); return;
        }
        //GetUserscreenmappedList($('#ddl_UserName').val());
        GetUserPages();
    });
    $("#submitpages").click(function () {


        var allVals = [];
        $('.chkListItem:checked').each(function () {
         //   debugger;
            var _obj = { userid: $("#ddl_UserName").find("option:selected").text(), groupid: $(this).attr('Groupid'), pageid: $(this).val() };
            allVals.push(_obj);

        });
        //_Userpagedata.userid = $("#ddl_UserName").val();
        _Userpagedata.userscreenname = allVals;
      //  insertpagenames(_Userpagedata);
        //console.log(_Userpagedata.userscreenname);

        // $("#result").text("Selected Values: " + allVals);
        //https://makitweb.com/make-a-dropdown-with-search-box-using-jquery/
        if (_Userpagedata.userscreenname.length > 0) {

            insertpagenames(_Userpagedata);
        } else {
            _n_plain_mes_1("<strong>info!</strong>", "Please select at least one PageName", "danger"); return;
        }
    });
});
var _Userpagedata = {

    "userscreenname": ""

};

function loadUserNames() {


    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetUserPermissionNames",
        "method": "GET",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        var html = "<option value='0'>Choose...</option>";

        if (response.status == "Success") {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].useR_ID + "'>" + response.data[index].office + "</option>"
            }
            $("#ddl_UserName").html(html);
            //$('select option:contains("State guest house")').prop('selected', true);


        } else {
            $("#ddl_UserName").html(html);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            // alert("Data Not Found");
            alert(response.reason);
            // window.location = "Login";
        }

    });
}

function GetUserPages() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetUserPages",
        "method": "GET",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {
        var table = $('<div class="row"></div>');
        var table1 = $('<div class="row"></div>');
     
        if (response.status == "Success") {
        
            table = $('<div><label>HRMS</label></div>');
         
        
            for (var index = 0; index < response.data.length; index++) {

           
                    
                    table.append($('<div class="col-md-2"></div>').append($('<div class="custom-control custom-checkbox"></div>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: response.data[index].pagE_ID, Groupid: response.data[index].grouP_ID, Groupname: response.data[index].grouP_NAME, id: response.data[index].id, 'class': 'custom-control-input chkListItem'
                    })).append(
                    $('<label>').attr({
                        for: response.data[index].id, 'class': 'custom-control-label'
                    }).text(response.data[index].pagE_NAME))));
               

            

             
            }
            $("#getcheckvalues").append(table);
            //$('select option:contains("State guest house")').prop('selected', true);
            $("#submitpages").show();

        } else {
            $("#getcheckvalues").append(table);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            // alert("Data Not Found");
            alert(response.reason);
            // window.location = "Login";
        }

    });
}

function insertpagenames(_Userpagedata) {
    var settings = {
        "async": true,
        "crossDomain": true,

        "method": "POST",
        "url": "../InsertUser",

        "data": JSON.stringify(_Userpagedata),
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
            //"Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }
    $.ajax(settings).done(function (response) {
        console.log(_Userpagedata);
        if (response.status == "Success") {
            alert(response.reason);
            window.location.reload();

        } else {

            alert(response.reason);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            alert(response.reason);
        }

    });



}

function GetUserscreenmappedList(USER_ID) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetUserscreenmappedList?user_id="+USER_ID,
        "method": "GET",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
           
            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            $('#user_table').dataTable().fnClearTable();
            var i = 1;
            $('#user_table').DataTable({
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
                "displayLength": 100,
                destroy: true,
                paging: true,
                bInfo: true,
                "ordering": true,
                data: response.screenmapList,


                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    { data: "pagename", "sWidth": "5%" },
                    { data: "pageid", "sWidth": "5%" },
                    //{ data: "v01name", "sWidth": "9%" },
                    { data: "groupid" },
                { data: "userid" },
                    {
                        "mData": null,
                        "mRender": function (data, type, s) {
                            return '<ul class="list-inline"><li class="list-inline-item"><a href="javascript: void (0);" class="EditAttributes" ><i class="far fa-edit" ></i></a></li></ul>'
                        }
                    }
                ]


            });

        } else {
            $('#user_table').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            // window.location = "Login";
        }

    });
}
$('#user_table').on('click', '.EditAttributes', function () {


   });








