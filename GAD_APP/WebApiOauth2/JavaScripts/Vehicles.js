﻿var obj = {
    "sno": "",
    "REGISTRATION_NO": "",
   " VEHICLE_NO":"",
    "NAME_OF_PERTOL_BUNK": "",
    "VHL_DATE": "",
    "COUPON_NO": "",
    "QUANTITY": "",
    "PRICE": "",
    "POL_TYPE": "",
    "TOTAL_COST": "",
    "OFFICER_USING_VEHICLE": "",
    "CEILING_LIMIT_FIXED": "",
    "EXCESS_CONSUMPTION": "",
    "CONSUMPTION_OF_POL": "",
    "REQUESTED_ID": "",
    "DEPT_NAME": "",
    "DESIGNATION": "",
    "NAME": "",
    "MOBILE":"",
    "REMARKS": ""
}
var bdata = "";
$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#Vehicles').addClass('active');

    Vehicles_Get();

    $("#btn_save").click(function (e) {

        clearValues();

        obj.REGISTRATION_NO = $("#txt_vehicleno").val();
        obj.NAME_OF_PERTOL_BUNK = $("#txt_ptrlbunk_name").val();
        obj.VHL_DATE = $("#txt_date").val();
        obj.COUPON_NO = $("#txt_couponno").val();
        obj.QUANTITY = $("#txt_quantity").val();
        obj.PRICE = $("#txt_price").val();
        obj.POL_TYPE = $("#ddl_poltype option:selected").text();
        obj.TOTAL_COST = $("#txt_totalcost").val();
        //obj.OFFICER_USING_VEHICLE=$("#txt_officer_vhl").val();
        //obj.CEILING_LIMIT_FIXED=$("#txt_ceilinglimit").val();
        //obj.EXCESS_CONSUMPTION=$("#txt_excess_consumption").val();
        //obj.CONSUMPTION_OF_POL=$("#txt_pol_consumption").val();
        obj.sno = "";
        vehicledetails_add(obj);

    })

    $("#btn_save1").click(function (e) {

        clearValues();

        obj.REGISTRATION_NO = $("#txt_vehicleno1").val();
        obj.CAPTURE_DATE = $("#txt_date1").val();
        obj.REPLACED_SERVICING = $("#txt_replace_service1").val();
        obj.NAME_OF_THE_GARAGE = $("#txt_garage_name1").val();
        obj.AMOUNT = $("#txt_amount1").val();

        obj.sno = "";
        expendituredetails_add(obj);

    })
    $("#btn_update").click(function (e) {

        clearValues();

        obj.VEHICLE_NO = $("#txt_vehicleno_edit").val();
        obj.DEPT_NAME = $("#txt_dept_edit").val();
        obj.DESIGNATION = $("#txt_designation_edit").val();
        obj.CEILING_LIMIT_FIXED = $("#txt_ceilinglimit").val();
        obj.NAME = $("#txt_name_edit").val();
        obj.MOBILE = $("#txt_mobile_edit").val();
        obj.REMARKS = $("#txt_reamrks_edit").val();
       
        obj.sno = "";
        vehicledetails_update(obj);

    })

    $("#txt_quantity").keyup(function () {
        if ($("#txt_quantity").val() != "") {
            TotalAmount_Get();
        }
       
    })
    $("#txt_price").keyup(function () {
        if ($("#txt_price").val() != "") {
            TotalAmount_Get();
        }
       
    })
});
function clearValues() {
    obj.REGISTRATION_NO = "";
    obj.NAME_OF_PERTOL_BUNK = "";
    obj.VHL_DATE = "";
    obj.COUPON_NO = "";
    obj.QUANTITY = "";
    obj.PRICE = "";
    obj.POL_TYPE = "";
    obj.TOTAL_COST = "";
    //obj.OFFICER_USING_VEHICLE = "";
    //obj.CEILING_LIMIT_FIXED = "";
    //obj.EXCESS_CONSUMPTION = "";
    //obj.CONSUMPTION_OF_POL = "";

}

function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function expendituredetails_add(details) {
    $('#expenditure_modal').parsley().validate();
    if ($('#expenditure_modal').parsley().isValid()) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/Add_expenditure_Details",
            "method": "POST",
            "data": JSON.stringify(details),
            "headers": {
                "Authorization": "bearer " + Cookies.get('token'),
                "Cache-Control": "no-cache",
                "Content-Type": "application/json",
            }
        }

        $.ajax(settings).done(function (response) {
            console.log(response);

            if (response.responseCode == 1) {
                //alert(response.responseMessage);
                $("#addnew_vehicledetails").modal('hide');
                swal("Details Added Successfully!", {
                    icon: "success",
                }).then(function () {
                    Vehicles_Get();
                });


            } else {
                swal("Submission Fail!", {
                    icon: "error",
                }).then(function () {
                    Vehicles_Get();
                });

                //alert(response.responseMessage);
            }
        }).fail(function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }

        });
    }
}
function Vehicles_Get() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Vehicles_Get",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);

        if (response.responseCode == 1) {

            bdata = response.data;

            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [

                    {
                        "data": "desc_and_vhno",
                        "type": "sno",
                        "render": function (data, type, row, meta) {
                            data = "<ul class='list-inline'><li class='list-inline-item'><a href='row.desc_and_vhno' class='edit' sno='" + row.desc_and_vhno + "' data-toggle='modal' data-target='#exampleModal'>" + row.desc_and_vhno + "</a></li></ul>";
                            return data;
                        }
                    },
                    { data: "vehicle_model" },
                    { data: "vehicle_type" },
                    { data: "alloted_department" },
                    { data: "user_designation" },
                    { data: "ceiling_limit" },
                    {
                        data: "driver_name",
                        
                        render: function (data, type, row) {
                            return row.driver_name + '<br>' + row.driver_mobile + '';
                        }
                    },
                    { data: "remarks" },

                {
                    "data": "desc_and_vhno",
                    "type": "sno",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='update' sno='" + row.desc_and_vhno + "' data-toggle='modal' data-target='#exampleModal'><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                Vehicles_Add($(this).attr('sno'));
            });
            $('#dataTable').on('click', '.update', function () {
                Vehicles_Update($(this).attr('sno'));
            });
        } else {

           // alert(response.ResponseMessage);
            _n_plain_mes_1("<strong>alert!</strong>", response.ResponseMessage, "warning"); 
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function vehicledetails_add(details) {
    $('#vehicles_modal').parsley().validate();
    if ($('#vehicles_modal').parsley().isValid()) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/Add_Vehicle_Details",
            "method": "POST",
            "data": JSON.stringify(details),
            "headers": {
                "Authorization": "bearer " + Cookies.get('token'),
                "Cache-Control": "no-cache",
                "Content-Type": "application/json",
            }
        }

        $.ajax(settings).done(function (response) {
            console.log(response);

            if (response.responseCode == 1) {
                //alert(response.responseMessage);
                $("#addnew_vehicledetails").modal('hide');
                swal("Vehicle Details Added Successfully!", {
                    icon: "success",
                }).then(function () {
                    Vehicles_Get();
                });


            }
            else if (response.responseCode == 6) {
                swal(response.responseMessage, {
                    icon: "error",
                }).then(function () {
                    Vehicles_Get();
                });
            }
            else {
                swal("Submission Fail!", {
                    icon: "error",
                }).then(function () {
                    Vehicles_Get();
                });

                //alert(response.responseMessage);
            }
        }).fail(function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }

        });
    }
}
function Vehicles_Add(desc_and_vhno) {


    $("#addnew_vehicledetails input[type='text'],select").val('');
    $("#addnew_vehicledetails").modal('show');
    for (var index = 0; index < bdata.length; index++) {

        if (bdata[index].desc_and_vhno == desc_and_vhno) {
            var obj = bdata[index];
           // console.log(obj);
            $("#txt_vehicleno").val(obj.desc_and_vhno);
            $("#txt_vehicleno1").val(obj.desc_and_vhno);
        }

    }


}
function vehicledetails_update(details) {
    $('#vehicles_modal_edit').parsley().validate();
    if ($('#vehicles_modal_edit').parsley().isValid()) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/Update_Vehicle_Details",
            "method": "POST",
            "data": JSON.stringify(details),
            "headers": {
                "Authorization": "bearer " + Cookies.get('token'),
                "Cache-Control": "no-cache",
                "Content-Type": "application/json",
            }
        }

        $.ajax(settings).done(function (response) {
            console.log(response);

            if (response.responseCode == 1) {
                //alert(response.responseMessage);
                $("#Update_vehicledetails").modal('hide');
                swal("Vehicle Details Upated Successfully!", {
                    icon: "success",
                }).then(function () {
                    Vehicles_Get();
                });


            } else {
                swal("Submission Fail!", {
                    icon: "error",
                }).then(function () {
                    Vehicles_Get();
                });

                //alert(response.responseMessage);
            }
        }).fail(function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }

        });
    }
}
function Vehicles_Update(desc_and_vhno) {


    $("#Update_vehicledetails input[type='text'],select").val('');
    $("#Update_vehicledetails").modal('show');
    for (var index = 0; index < bdata.length; index++) {

        if (bdata[index].desc_and_vhno == desc_and_vhno) {
            var obj = bdata[index];
            //console.log(obj);
            $("#txt_vehicleno_edit").val(obj.desc_and_vhno);
            $("#txt_dept_edit").val(obj.alloted_department);
            $("#txt_designation_edit").val(obj.user_designation);
            $("#txt_ceilinglimit").val(obj.ceiling_limit);
            $("#txt_name_edit").val(obj.driver_name);
            $("#txt_mobile_edit").val(obj.driver_mobile);
            $("#txt_reamrks_edit").val(obj.remarks);

        }

    }


}
function TotalAmount_Get() {
    obj.TOTAL_COST = parseFloat($('#txt_quantity').val()).toFixed(2) * parseFloat($('#txt_price').val()).toFixed(2);
    $('#txt_totalcost').val(obj.TOTAL_COST);
}
