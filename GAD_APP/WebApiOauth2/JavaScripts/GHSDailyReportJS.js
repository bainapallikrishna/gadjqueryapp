﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#GDStausid').addClass('active');

    loadGuesthouseDetails();
    var getdate = null;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var todaydate = date.split('-');
    if (todaydate[1] != "10" && todaydate[1] != "11" && todaydate[1] != "12") {
        getdate = todaydate[2] + '/' + "0" + todaydate[1] + '/' + todaydate[0];
        if (todaydate[2] == "1" || todaydate[2] == "2" || todaydate[2] == "3" || todaydate[2] == "4" || todaydate[2] == "5" || todaydate[2] == "6" || todaydate[2] == "7" || todaydate[2] == "8" || todaydate[2] == "9") {
            getdate = "0" + todaydate[2] + '/' + "0" + todaydate[1] + '/' + todaydate[0];
        }
        $('#txt_ddate').val(getdate);
    }
    if (todaydate[1] == "10" || todaydate[1] == "11" || todaydate[1] == "12") {
        getdate = todaydate[2] + '/' + todaydate[1] + '/' + todaydate[0];
        if (todaydate[2] == "1" || todaydate[2] == "2" || todaydate[2] == "3" || todaydate[2] == "4" || todaydate[2] == "5" || todaydate[2] == "6" || todaydate[2] == "7" || todaydate[2] == "8" || todaydate[2] == "9") {
            getdate = "0" +todaydate[2] + '/' +  todaydate[1] + '/' + todaydate[0];
        }
        $('#txt_ddate').val(getdate);
    }

    //var 

   



    
    //loadGHSDailyStatusDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());
   

    load_GHRoomCategory();
});

var _Udata = {
    "ghsid": "",
    "roomno": "",
    "roomtype": "",
    "name_designation_Guest": "",
    "name_Guest": "",
    "designation_Guest": "",
    "dateofarrival": "",
    "dateofdeparture": "",
    "category": "",
    "reamrks": "",
    "id": "",
    "roomfloor": ""
};
var _ChOdata = {
    "ghsid": "",
    "id": "",
    "checkout_status": "",
    "checkout_date": ""
};

function loadGuesthouseDetails() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetGuesthouseDRPDetails?type=1",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        var html = "<option value='0'>Choose...</option>";
        if (response.status == "Success") {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].ghS_ID + "'>" + response.data[index].ghS_NAME + "</option>"
            }
            $("#ddl_Guesthouse").html(html);
            //$('select option:contains("State guest house")').prop('selected', true);
            $('#ddl_Guesthouse option').each(function () {
                if ($(this).val() == "GH-001") {
                    $(this).prop('selected', true);
                }
            });
            loadGuesthouseRoomDetails($('#ddl_Guesthouse').val());
            loadGuesthouseCountsDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());
            loadGHSDailyStatusDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());
        } else {
            $("#ddl_Guesthouse").html(html);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "Login";
        }

    });
}

function loadGuesthouseCountsDetails(ghs_id, date) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetGuesthouseCounts?ghs_id=" + ghs_id + "&date=" + date,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }
    $.ajax(settings).done(function (response) {
        if (response.status == "Success") {
            $('#txttotal').text(response.total);
            $('#txtoccupied').text(response.occupied);
            $('#txtvacant').text(response.vacant);
        }
        else {
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}


function loadGuesthouseRoomDetails(ghsid) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetGuesthouseRoomDetails?type=7" + "&ghsid=" + ghsid,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        var html = "<option value='0'>Choose...</option>";
        if (response.status == "Success") {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_NO + "'>" + response.data[index].rooM_NO + "</option>"
            }
            $("#ddl_RoomNo").html(html);
            //$('select option:contains("State guest house")').prop('selected', true);


        } else {
            $("#ddl_RoomNo").html(html);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "Login";
        }

    });
}

$('#ddl_Guesthouse').change(function (e) {
    if ($('#ddl_Guesthouse').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Guesthouse Name", "danger"); $('#ddl_Guesthouse').focus(); return;
    }
    else if ($('#txt_ddate').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date", "danger"); $('#txt_ddate').focus(); return;
    }
    else {


        //var cDate = $('#txt_ddate').val();

        //var todate = cDate.split('/');
        //getdate = todate[1] + '/' + todate[0] + '/' + todate[2];
        //const CTDate = getdate;

        loadGuesthouseCountsDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());

        loadGHSDailyStatusDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());
    }
});

//$('#txt_ddate').change(function () {
//    if ($('#txt_ddate').val() != "" && $('#ddl_Guesthouse').val() != "0") {
//        loadGuesthouseCountsDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());

//        loadGHSDailyStatusDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());
//    }
//    else {
//        _n_plain_mes_1("<strong>info!</strong>", "Please select atleast one Guesthousename", "danger"); return;
//    }
//});

$('#btn_search').click(function () {

    if ($('#ddl_Guesthouse').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Guesthouse Name", "danger"); $('#ddl_Guesthouse').focus(); return;
    }
    else if ($('#txt_ddate').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date", "danger"); $('#txt_ddate').focus(); return;
    }
    else {

       
        //var cDate = $('#txt_ddate').val();

        //var todate = cDate.split('/');
        //getdate = todate[1] + '/' + todate[0] + '/' + todate[2];
        //const CTDate = getdate;

        loadGuesthouseCountsDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());

        loadGHSDailyStatusDetails($('#ddl_Guesthouse').val(), $('#txt_ddate').val());
    }
});

function load_GHRoomCategory() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Category_Master?type=6",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_CATEGORY + "'>" + response.data[index].rooM_CATEGORY + "</option>"
            }
            $("#ddl_Category").html(html);
        } else {
            $("#ddl_Category").html(html);
            //alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "Login";
        }

    });
}


$('#btn_submit').click(function () {


    if ($('#ddl_Guesthouse').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Guesthouse Name", "danger"); $('#ddl_Guesthouse').focus(); return;
    }
    else {
        $('#exampleModalLabel').text("Add new Guest");
        $('#ddl_RoomNo').val("0");
        $('#txtNtGuest').val("");
        $('#txtDOtGuest').val("");
        $('#txtDOArrival').val("");
        $('#txtDODeparture').val("");
        $('#txtRemarks').val("");
        //$('input[type="text"]').val("");
        $('#ddl_Category').val("0");
        $('#Guestadd').show();
        $('#Guestedit').hide();
        $('#GuestCheckout').hide();
       
        $("#exampleModal").modal('show');
    }
});
function loadGHSDailyStatusDetails(gid, date) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetGuesthouse_Details?ghs_id=" + gid + "&date=" + date,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            //  bdata = response.data;
            $('#dataTable1').dataTable().fnClearTable();
            var i = 1;
            var groupColumn = 1;
            var table = $('#dataTable1').DataTable({
                //dom: 'Bfrtip',
                "dom": '<"dt-buttons"Bf><"clear">lirtp',
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
                "order": [[groupColumn, 'DESC']],
                "displayLength": 100,
                //"paging": true,

                "autoWidth": true,
                destroy: true,
                "scrollY": "62vh",
                "scrollCollapse": true,

                data: response.guesthouseList,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    { data: "roomfloor" },
                    { data: "roomtype", "sWidth": "10%" },
                    { data: "roomno", "sWidth": "10%" },
                    { data: "name_designation_Guest" },
                    { data: "dateofarrival" },
                    { data: "dateofdeparture" },
                    { data: "category" },
                    { data: "reamrks" },
                    
                    {
                        "mData": null,
                        "mRender": function (data, type, s) {
                            return '<ul class="list-inline"><li class="list-inline-item"><a href="javascript: void (0);" class="EditAttributes" Eid="'+s["id"]+'"  Eroomtype="' + s["roomtype"] + '" Eroomno="' + s["roomno"] + '" Ename="' + s["name_Guest"] + '" Edesignation="' + s["designation_Guest"] + '" Earrival="' + s["dateofarrival"] + '" Edeparture="' + s["dateofdeparture"] + '" Ecategory="' + s["category"] + '" Eghsid="' + s["ghsid"] + '" Eremarks="' + s["reamrks"] + '"><i class="far fa-edit" ></i></a></li></ul>'
                        }
                    }
                ],
                "bInfo": true,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="10" align="center" class="tbl-mdhdr">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                },
                "buttons": [
                    {

                        extend: 'pdfHtml5',
                        text: 'Download Pdf',
                        filename: 'Guesthouse_Daily_Status',
                        title: 'Guesthouse Daily Status',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6,7,8],
                            search: 'applied',
                            order: 'applied'
                        },
                        customize: function (doc) {

                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                            doc['header'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            text: ['Created on: ', { text: jsDate.toString() }]
                                        },
                                        {
                                            alignment: 'right',
                                            text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                        }
                                    ],
                                    margin: 20
                                }
                            });


                            var tblBody = doc.content[1].table.body;
                            for (var i = 0; i < tblBody[0].length; i++) {
                                tblBody[0][i].fillColor = '#FFFFFF';
                                tblBody[0][i].color = 'black';
                            }

                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) { return .5; };
                            objLayout['vLineWidth'] = function (i) { return .5; };
                            objLayout['hLineColor'] = function (i) { return '#aaa'; };
                            objLayout['vLineColor'] = function (i) { return '#aaa'; };
                            objLayout['paddingLeft'] = function (i) { return 4; };
                            objLayout['paddingRight'] = function (i) { return 4; };
                            doc.content[0].layout = objLayout;
                            doc.content[1].layout = 'Borders';

                        }
                        //exportOptions: {
                        //    modifier: {
                        //        page: 'current'
                        //    }
                        //}
                    }
                ]
                //"drawCallback": function (settings) {
                //var api = this.api();
                //var rows = api.rows({ page: 'current' }).nodes();
                //var last = null;

                //api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                //    if (last !== group) {
                //        $(rows).eq(i).before(
                //            '<tr class="group"><td colspan="6" align="center" class="tbl-mdhdr">' + group + '</td></tr>'
                //        );

                //        last = group;
                //    }
                //});
                //}

            });
            //$('#dataTable1 tbody').on('click', 'tr.group', function () {
            //    var currentOrder = table.order()[0];
            //    if (currentOrder[0] === groupColumn && currentOrder[1] === 'DESC') {
            //        table.order([groupColumn, 'asc']).draw();
            //    }
            //    else {
            //        table.order([groupColumn, 'DESC']).draw();
            //    }
            //});
            _n_plain_mes_1("<strong>Data Loaded Successfully</strong>", "", "success");

        } else {
            $('#dataTable1').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}


$('#dataTable1').on('click', '.EditAttributes', function () {


    $('#exampleModalLabel').text("Edit");

    $('#Guestadd').hide();
    $('#Guestedit').show();
    $('#GuestCheckout').show();

    UpdateGuesthouseStatus($(this).attr('Eroomtype'), $(this).attr('Eid'), $(this).attr('Eroomno'), $(this).attr('Ename'), $(this).attr('Edesignation'), $(this).attr('Earrival'), $(this).attr('Edeparture'), $(this).attr('Ecategory'), $(this).attr('Eghsid'), $(this).attr('Eremarks'));
});


function UpdateGuesthouseStatus(Eroomtype, Eid, Eroomno, Ename, Edesignation, Earrival, Edeparture, Ecategory, Eghsid, Eremarks ) {


    sessionStorage.setItem("GHS_ID", Eghsid);

    sessionStorage.setItem("S_ID", Eid);
    //$('#txtTypeofRoom').prop('disabled', true);
    //$('#Eroomno').prop('disabled', true);
    //$('#txtNtGuest').prop('disabled', false);
    //$('#txtDOtGuest').prop('disabled', false);
    //$('#txtDOArrival').prop('disabled', false);
    //$('#txtDODeparture').prop('disabled', false);
    //$('#txtCategory').prop('disabled', false);

   // $('#txtTypeofRoom').val(Eroomtype);
    //$('#txtRoomNo').val(Eroomno);

    $('#txtRemarks').val(Eremarks);

    if (Eroomno != "") {
        $('#ddl_RoomNo option').each(function () {
            if ($(this).val() == Eroomno) {
                $(this).prop('selected', true);

            }
        });
    }
    if (Ecategory == "") {
        $('#txtCategory').val("0");
    }

    if (Ecategory != "") {
        $('#ddl_Category option').each(function () {
            if ($(this).val() == Ecategory) {
                $(this).prop('selected', true);

            }
        });
    }
    

    if (Ename == "") {
        $('#txtNtGuest').val("");
    }
    if (Ename != "") {
        //  $('#txtNtGuest').prop('disabled', true);
        $('#txtNtGuest').val(Ename);
    }
    if (Edesignation == "") {
        $('#txtDOtGuest').val("");
    }
    if (Edesignation != "") {
        // $('#txtDOtGuest').prop('disabled', true);
        $('#txtDOtGuest').val(Edesignation);
    }
    if (Earrival == "null") {
        $('#txtDOArrival').val("");
    }
    if (Earrival != "null") {
        // $('#txtDOArrival').prop('disabled', true);
        $('#txtDOArrival').val(Earrival);
    }
    if (Edeparture == "null") {
        $('#txtDODeparture').val("");
    }
    if (Edeparture != "null") {
        //  $('#txtDODeparture').prop('disabled', true);
        $('#txtDODeparture').val(Edeparture);
    }
    
    

    $('#exampleModal').modal('show');

}

$('#Guestadd').click(function () {
    if ($('#ddl_RoomNo').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Room Number", "danger"); $('#ddl_RoomNo').focus(); return;
    }
    else if ($('#txtNtGuest').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Name of the Guest", "danger"); $('#txtNtGuest').focus(); return;
    }
    else if ($('#txtDOtGuest').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Designation of the Guest", "danger"); $('#txtDOtGuest').focus(); return;
    }
    else if ($('#txtDOArrival').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date of Arrival", "danger"); $('#txtDOArrival').focus(); return;
    }
    else if ($('#txtDODeparture').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date of Departure", "danger"); $('#txtDODeparture').focus(); return;
    }
    else if ($('#ddl_Category').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Category", "danger"); $('#ddl_Category').focus(); return;
    }
    else if ($('#txtRemarks').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Remarks", "danger"); $('#txtRemarks').focus(); return;
    }

    else {
        _Udata.ghsid = $('#ddl_Guesthouse').val();
        _Udata.roomno = $('#ddl_RoomNo').val();
        _Udata.name_Guest = $('#txtNtGuest').val();
        _Udata.designation_Guest = $('#txtDOtGuest').val();
        _Udata.dateofarrival = $('#txtDOArrival').val();
        _Udata.dateofdeparture = $('#txtDODeparture').val();
        _Udata.category = $('#ddl_Category').val();
        _Udata.reamrks = $('#txtRemarks').val();
        InsertGuesthouseData(_Udata);
    }
});

$('#GuestCheckout').click(function () {
    if ($('#ddl_RoomNo').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Room Number", "danger"); $('#ddl_RoomNo').focus(); return;
    }
    else if ($('#txtNtGuest').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Name of the Guest", "danger"); $('#txtNtGuest').focus(); return;
    }
    else if ($('#txtDOtGuest').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Designation of the Guest", "danger"); $('#txtDOtGuest').focus(); return;
    }
    else if ($('#txtDOArrival').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date of Arrival", "danger"); $('#txtDOArrival').focus(); return;
    }
    else if ($('#txtDODeparture').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date of Departure", "danger"); $('#txtDODeparture').focus(); return;
    }
    else if ($('#ddl_Category').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Category", "danger"); $('#ddl_Category').focus(); return;
    }
    else if ($('#txtRemarks').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Remarks", "danger"); $('#txtRemarks').focus(); return;
    }
    else {
        _ChOdata.ghsid = sessionStorage.getItem("GHS_ID");
        _ChOdata.id = sessionStorage.getItem("S_ID");
        _ChOdata.checkout_date = $('#txtDODeparture').val();
        GuesthouseCheckoutData(_ChOdata);
    }
});


$('#Guestedit').click(function () {
    if ($('#ddl_RoomNo').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Room Number", "danger"); $('#ddl_RoomNo').focus(); return;
    }
    else if ($('#txtNtGuest').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Name of the Guest", "danger"); $('#txtNtGuest').focus(); return;
    }
    else if ($('#txtDOtGuest').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Designation of the Guest", "danger"); $('#txtDOtGuest').focus(); return;
    }
    else if ($('#txtDOArrival').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date of Arrival", "danger"); $('#txtDOArrival').focus(); return;
    }
    else if ($('#txtDODeparture').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Date of Departure", "danger"); $('#txtDODeparture').focus(); return;
    }
    else if ($('#ddl_Category').val() == "0") {
        _n_plain_mes_1("<strong>info!</strong>", "Please select Category", "danger"); $('#ddl_Category').focus(); return;
    }
    else if ($('#txtRemarks').val() == "") {
        _n_plain_mes_1("<strong>info!</strong>", "Please enter Remarks", "danger"); $('#txtRemarks').focus(); return;
    }

    else {
        _Udata.ghsid = sessionStorage.getItem("GHS_ID");
        _Udata.id = sessionStorage.getItem("S_ID");
        _Udata.roomno = $('#ddl_RoomNo').val();
        _Udata.name_Guest = $('#txtNtGuest').val();
        _Udata.designation_Guest = $('#txtDOtGuest').val();
        _Udata.dateofarrival = $('#txtDOArrival').val();
        _Udata.dateofdeparture = $('#txtDODeparture').val();
        _Udata.category = $('#ddl_Category').val();
        _Udata.reamrks = $('#txtRemarks').val();
        UpdateGuesthouseData(_Udata);
    }

});


function InsertGuesthouseData(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../InsertGuesthousestatusDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#exampleModal").modal('hide');

            loadGHSDailyStatusDetails(response.gid, $('#txt_ddate').val());
            loadGuesthouseCountsDetails(response.gid, $('#txt_ddate').val());
            _n_plain_mes_1("<strong>success!</strong>", "Data Inserted successfully", "success"); return;
        }
        else if (response.status == "2") {
            $("#exampleModal").modal('hide');

            loadGHSDailyStatusDetails(response.gid, $('#txt_ddate').val());
            loadGuesthouseCountsDetails(response.gid, $('#txt_ddate').val());
            _n_plain_mes_1("<strong>Fail!</strong>", response.reason, "danger"); return;
        }
        else {
            $("#exampleModal").modal('hide');
            _n_plain_mes_1("<strong>fail!</strong>", "Data Insertion fail..!", "warning"); return;

        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}


function UpdateGuesthouseData(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../UpdateGuesthousestatusDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#exampleModal").modal('hide');

            loadGHSDailyStatusDetails(response.gid, $('#txt_ddate').val());
            loadGuesthouseCountsDetails(response.gid, $('#txt_ddate').val());
            _n_plain_mes_1("<strong>success!</strong>", "Data Updated successfully", "success"); return;
        } else {
            $("#exampleModal").modal('hide');
            _n_plain_mes_1("<strong>fail!</strong>", "Data Updated fail..!", "warning"); return;

        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}


function GuesthouseCheckoutData(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GuesthouseCheckoutDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#exampleModal").modal('hide');

            loadGHSDailyStatusDetails(response.gid, $('#txt_ddate').val());
            loadGuesthouseCountsDetails(response.gid, $('#txt_ddate').val());
            _n_plain_mes_1("<strong>success!</strong>", "Room Checkout successfully", "success"); return;
        } else {
            $("#exampleModal").modal('hide');
            _n_plain_mes_1("<strong>fail!</strong>", "Room Checkout fail..!", "warning"); return;

        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}