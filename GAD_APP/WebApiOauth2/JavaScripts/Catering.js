﻿
var cat_obj = {
    "objItems": [],
    "personName": "",
    "mobileNumber": "",
    "meeting": "",
    "meetingDate": "",
    "meetingTime": "",
    "venue": "",
    "OrderID": ""
}

var items = [];

$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#caterid').addClass('active');

    $('#ddl_label_others').hide();
    $('#ddl_meeting_others').hide();

    $('#txt_date').datetimepicker({
        "allowInputToggle": true,
        "showClose": true,
        "showClear": true,
        "showTodayButton": true,
        "format": "DD/MM/YYYY",
    });


    $('#txt_time').datetimepicker({
        "allowInputToggle": true,
        "showClose": true,
        "showClear": true,
        "showTodayButton": true,
        "format": "hh:mm:ss A",
    });

    $("#btn_add").click(function (e) {


        items.push({ "name": "VegMeals", "quantity": $("#txt_meals").val() });
        items.push({ "name": "NonVegMeals", "quantity": $("#txt_non_meals").val() });
        items.push({ "name": "Snacks", "quantity": $("#txt_snacks").val() });
        items.push({ "name": "Drinks", "quantity": $("#txt_drink").val() });
        items.push({ "name": "Tea", "quantity": $("#txt_tea").val() });
        items.push({ "name": "Coffee", "quantity": $("#txt_coffee").val() });
        items.push({ "name": "Others", "quantity": $("#txt_others").val() });

        cat_obj.objItems = items;
        cat_obj.personName = $("#txt_name").val();
        cat_obj.mobileNumber = $("#txt_mobile").val();
        cat_obj.meeting = $("#ddl_meeting option:selected").text().trim() == "Others" ? $('#ddl_meeting_others').val() : $("#ddl_meeting option:selected").val();
        cat_obj.meetingDate = $("#txt_date").val();
        cat_obj.meetingTime = $("#txt_time").val();
        cat_obj.venue = $("#txt_venue").val();




        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/Catering_Add",
            "method": "POST",
            "data": JSON.stringify(cat_obj),
            "headers": {
                "Authorization": "bearer " + Cookies.get('token'),
                "Cache-Control": "no-cache",
                "Content-Type": "application/json",
            }
        }

        $.ajax(settings).done(function (response) {
           // console.log(response);
            if (response.responseCode == 1) {
                alert(response.responseMessage);
                window.location.reload();
            } else {
                _n_plain_mes_1("<strong>alert</strong>", response.responseMessage, "danger");
                //alert(response.responseMessage);
            }
        }).fail(function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }

        });

    });

    load_catering_details();
});


$('#ddl_meeting').change(function () {
    if ($("#ddl_meeting option:selected").text().trim() == "Others") {
        $('#ddl_label_others').show();
        $('#ddl_meeting_others').show();
    }
    else {
        $('#ddl_label_others').hide();
        $('#ddl_meeting_others').hide();
        
    }
});

function load_catering_details() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Catering_Details_Get",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
      //  console.log(response);
        if (response.responseCode == 1) {
            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [

                    { data: "personName" },
                    { data: "mobileNumber" },
                    { data: "meeting" },
                    { data: "meetingDate" },
                    { data: "meetingTime" },
                    { data: "venue" },

                {
                    "data": "orderID",
                    "type": "sno",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='edit' sno='" + row.orderID + "' data-toggle='modal' data-target='#itemsdetails'><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                item_details($(this).attr('sno'));
            });
        } else {
            _n_plain_mes_1("<strong>alert</strong>", response.responseMessage, "danger");
            //alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }
    });
}
function item_details(sno) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Catering_Details_Get_By_Id?id="+sno,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        if (response.responseCode == 1) {
            $('#dataTable_modal').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [
                    { data: "name" },
                    { data: "quantity" }
                ]
            });
        } else {
            _n_plain_mes_1("<strong>alert</strong>", response.responseMessage, "danger");
           // alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }
    });
}