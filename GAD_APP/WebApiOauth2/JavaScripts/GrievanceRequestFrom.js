﻿var obj = {
    "EmpID": "",
    "PERSON_NAME": "",
    "DESGNATION": "",
    "REQUSET_OWNED": "",
    "DATE_OF_REQUEST": "",
    "REQUEST_GIST": "",
    "UPLOAD_PATH": ""
}
var filepath = "";

$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#GrievanceForm').addClass('active');

    $("#btn_submit").click(function (e) {
        Grievance_submit();
    })

    $("#fl_upfile").change(function (e) {
        upload_file();
    })
});

function upload_file() {

    var formData = new FormData();
    var file = $('#fl_upfile')[0];
    formData.append('file', file.files[0]);


    $.ajax({
        url: '/SaveFile_GrievanceFile',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        },
        success: function (d) {
            console.log(d)
            if (d.status == "Success") {
                filepath = d.filepath;
            } else {
                filepath = "";
            }
        },
        error: function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }
        }
    });
};


//var settings = {
//    "async": true,
//    "crossDomain": true,
//    "url": "/SaveFile_GrievanceFile",
//    "method": "POST",
//    "data": JSON.stringify(formData),
//    "processData": false,
//    "headers": {
//        "Authorization": "bearer " + Cookies.get('token'),
//        "Cache-Control": "no-cache",
//        "Content-Type": 'application/json'
//    }
//}

//$.ajax(settings).done(function (response) {
//    console.log(response);

//}).fail(function (data, textStatus, xhr) {
//    if (data.status == 401) {
//        window.location = "login";
//    }

//});
//}
function Grievance_submit() {
    obj.EmpID = 0;
    obj.PERSON_NAME = $("#txt_name").val();
    obj.DESGNATION = $("#txt_Designation").val();
    obj.REQUSET_OWNED = $("#txt_request").val();
    obj.DATE_OF_REQUEST = $("#req_date").val();
    obj.REQUEST_GIST = $("#txt_gist_pnts").val();
    obj.UPLOAD_PATH = filepath;


    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Grievance_Add",
        "method": "POST",
        "data": JSON.stringify(obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        if (response.responseCode == 1) {
            $("#exampleModal").modal("hide");
            alert(response.ResponseMessage);
            window.location.reload();
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });

}