﻿var vip_obj = {
    "category_id": "",
    "subject": "",
    "date": "",
    "start_time": "",
    "end_time": "",
    "location": "",
    "lat": "",
    "lang": "",
    "created_by": "",
    "owned_by": ""
}
var curr_id;

var v_data;

$(document).ready(function () {
    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#VIP_Schedule').addClass('active');


    $("#btn_update").hide();
    LoadCategory();
    LoadList()
    $("#btn_add").click(function (e) {
        Add_Schedule();
    });
    $("#btn_update").click(function (e) {
        Update_Schedule();
    });

    $("#btn_filter").click(function (e) {
        LoadList();
    });
});


function LoadCategory() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_details",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].id + "'>" + response.data[index].category_name + "</option>"
            }
            $("#ddl_cat,#rp_ddl_cat").html(html);
        } else {
            $("#ddl_cat,#rp_ddl_cat").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Add_Schedule() {
    vip_obj.category_id = $("#ddl_cat").val();
    vip_obj.subject = $("#txt_subject").val();
    vip_obj.date = $("#vip_date").val();
    vip_obj.start_time = $("#id_start_time").val();
    vip_obj.end_time = $("#id_end_time").val();
    vip_obj.location = "";
    vip_obj.lat = "";
    vip_obj.lang = "";
    vip_obj.created_by = "";
    vip_obj.owned_by = "";

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_Add",
        "method": "POST",
        "data": JSON.stringify(vip_obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage)
            window.location.reload();
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Edit_Vip_data(id) {
    curr_id = id;
    var obj;
    for (var index = 0; index < v_data.length; index++) {
        if (id == v_data[index].id) {
            obj = v_data[index];
            break;
        }
    }
    $("#ddl_cat option").each(function () {
        if ($.trim($(this).text()) == $.trim(obj.category_name)) {
            $(this).attr('selected', 'selected');
        }
    });
    //$("#ddl_cat").val(v_data[index].category_name);
    $("#txt_subject").val(obj.subject);
    $("#vip_date").val(obj.date.split('T')[0]);
    $("#id_start_time").val(obj.start_time);
    $("#id_end_time").val(obj.end_time);


    $("#id_start_time,#id_end_time").trigger("focus");
    $("#id_start_time,#id_end_time").trigger("blur");
    $("#btn_update").show();
    $("#btn_add").hide();
}

function Update_Schedule() {
    vip_obj.category_id = $("#ddl_cat").val();
    vip_obj.subject = $("#txt_subject").val();
    vip_obj.date = $("#vip_date").val();
    vip_obj.start_time = $("#id_start_time").val();
    vip_obj.end_time = $("#id_end_time").val();
    vip_obj.location = "";
    vip_obj.lat = "";
    vip_obj.lang = "";
    vip_obj.created_by = "";
    vip_obj.owned_by = curr_id;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_Update",
        "method": "POST",
        "data": JSON.stringify(vip_obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage)
            window.location.reload();
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function LoadList() {

    var query = "fromdate=";
    query+= $("#rp_from_date").val() != undefined ? $("#rp_from_date").val() : "";

    query+= "&todate=";
    query+= $("#rp_to_date").val() != undefined ? $("#rp_to_date").val() : "";

    query+= "&catid=";
    query += $("#rp_ddl_cat").val() != undefined && $("#rp_ddl_cat").val()!='0' ? $("#rp_ddl_cat").val() : "";
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_List?" + query,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {

            v_data = response.data;

            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": false,
                "pageLength": 10,

                data: response.data,
                columns: [


                    { data: "date" },
                    { data: "start_time" },
                    { data: "end_time" },
                    { data: "subject" },
                    { data: "category_name" },
                {
                    "data": "id",
                    "type": "name",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='#' class='edit' rid='" + row.id + "' ><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                Edit_Vip_data($(this).attr('rid'));
            });
        } else {
            $('#dataTable').DataTable().clear().destroy();
            alert(response.ResponseMessage);
        }
        //var html = "<option value='0'>Choose...</option>";
        //if (response.responseCode == 1) {
        //    for (var index = 0; index < response.data.length; index++) {
        //        html += "<option value='" + response.data[index].id + "'>" + response.data[index].category_name + "</option>"
        //    }
        //    $("#ddl_cat").html(html);
        //} else {
        //    $("#ddl_cat").html(html);
        //    alert(response.ResponseMessage);
        //}
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}