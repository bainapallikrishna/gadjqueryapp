﻿$(document).ready(function () {

   
    $("#ddl_UserName").select2();

    $("#hidetable").hide();
    loadUserNames();
    // GetUserscreenmappedList();
    $("#getpages").click(function () {
        $("#hidetable").show();
       // $("#hidecolumn1,#hidecolumn2,#hidecolumn3").hide();
        if ($('#ddl_UserName').val() == "0") {
            _n_plain_mes_1("<strong>info!</strong>", "Please select User Name", "danger"); $('#ddl_UserName').focus(); return;
        }
        GetUserscreenmappedList($('#ddl_UserName').val());
        // GetUserPages();
    });
    $("#updateuserdetails").click(function () {
      
       // var pusharray = new Object();
        //$('.checklist:checked').each(function () {
        //    //debugger;
        //    // var _obj = { userid: $("#ddl_UserName").find("option:selected").text(), groupid: $(this).attr('Groupid'), pageid: $(this).val() };
        //    pusharray.push(Userdataarray);
        //https://forums.asp.net/t/2116652.aspx?How+to+get+values+from+a+dynamically+created+Check+Box+list+
        //});
         var Userdataarray = [];
        // Userdataarray.push(data);
        $.each($('#user_table :checkbox:checked'), function (a, b) {
            var _obj = { pageid: $(this).attr('pageid'), groupid: $(this).attr('groupid'), userid: $(this).attr('userid') }
            //pusharray[a] = $(this).attr('pageid');
            Userdataarray.push(_obj);
        });
        //$('#user_table')
        console.log(Userdataarray);
        _updatepagedata.userscreenname = Userdataarray;
        if (_updatepagedata.userscreenname.length > 0) {

            UpdatePagenames(_updatepagedata);
        } else {
            _n_plain_mes_1("<strong>info!</strong>", "Please select at least one PageName", "danger"); return;
        }
    });

});

var _updatepagedata = {

    "userscreenname": ""

};

function loadUserNames() {


    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetUserPermissionNames",
        "method": "GET",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        var html = "<option value='0'>Choose...</option>";

        if (response.status == "Success") {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].useR_ID + "'>" + response.data[index].office + "</option>"
            }
            $("#ddl_UserName").html(html);
            //$('select option:contains("State guest house")').prop('selected', true);


        } else {
            $("#ddl_UserName").html(html);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            // alert("Data Not Found");
            alert(response.reason);
            // window.location = "Login";
        }

    });
}

function GetUserscreenmappedList(USER_ID) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetUserscreenmappedList?user_id=" + USER_ID,
        "method": "GET",
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"

            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            $('#user_table').dataTable().fnClearTable();
            var i = 1;
            $('#user_table').DataTable({
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
                "displayLength": 100,
                destroy: true,
                paging: true,
                bInfo: true,
                "ordering": true,
                data: response.screenmapList,
             
               

                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    { data: "pagename" },
                  //  { data: "pageid" },
                    //{ data: "v01name", "sWidth": "9%" },
                   // { data: "groupid" },
               // { data: "userid" },
                    {
                        "mData": null,
                        "mRender": function (data, type, s) {

                
                          //var Userdataarray = [];
                          // Userdataarray.push(data);
                          //  console.log(Userdataarray);

                            return '<ul class="list-inline"><li class="list-inline-item"><input type="checkbox" pageid="' + s["pageid"] + '" groupid="' + s["groupid"] + '" userid="' + s["userid"] + '" name="checklist" checked/></li></ul>'

                                //'<ul class="list-inline"><li class="list-inline-item"><a href="javascript: void (0);" class="EditAttributes" ><i class="far fa-edit" ></i></a></li></ul>'
                        }
                    }
                ]


            });

        } else {
            $('#user_table').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            // window.location = "Login";
        }

    });
}



function UpdatePagenames(_updatepagedata) {
    var settings = {
        "async": true,
        "crossDomain": true,

        "method": "POST",
        "url": "../UpdateUser",

        "data": JSON.stringify(_updatepagedata),
        "headers": {
            "Content-Type": "application/json",
            "Cache-Control": "no-cache"
            //"Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
            //"Authorization": "bearer " + Cookies.get('token'),
            //"Cache-Control": "no-cache"
        }
    }
    $.ajax(settings).done(function (response) {
        console.log(_updatepagedata);
        if (response.status == "Success") {
            alert(response.reason);
            window.location.reload();

        } else {

            alert(response.reason);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            alert(response.reason);
        }

    });



}










