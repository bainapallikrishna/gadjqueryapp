﻿var vip_obj = {
    "category_id": "",
    "subject": "",
    "date": "",
    "start_time": "",
    "end_time": "",
    "location": "",
    "lat": "",
    "lang": "",
    "created_by": "",
    "owned_by": ""
}
var curr_id;

var v_data;

$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#VIP_Report').addClass('active');

    $("#btn_update").hide();
    LoadCategory();
    LoadList()
    $("#btn_add").click(function (e) {
        Add_Schedule();
    });
    $("#btn_update").click(function (e) {
        Update_Schedule();
    });

    $("#btn_filter").click(function (e) {
        LoadList();
    });


    $('#btn_Download').click(function () {


    
            var pdf = new jsPDF('p', 'pt', 'letter')
             , source = $('#temp')[0]
             , specialElementHandlers = {
                 '#bypassme': function (element, renderer) {
                     return true
                 }
             }
            margins = {
                top: 60,
                bottom: 60,
                left: 40,
                width: 800
            };
            pdf.fromHTML(
                source
                , margins.left
                , margins.top
                , {
                    'width': margins.width
                    , 'elementHandlers': specialElementHandlers
                },
                function (dispose) {

                    pdf.save('vip_sch_report.pdf');
                },
                margins
            )
        });

       
       

        //html2canvas(document.querySelector("#reports")).then(canvas => {
        //    document.body.appendChild(canvas)
        //});

    //});
});


function LoadCategory() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_details",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].id + "'>" + response.data[index].category_name + "</option>"
            }
            $("#ddl_cat,#rp_ddl_cat").html(html);
        } else {
            $("#ddl_cat,#rp_ddl_cat").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Add_Schedule() {
    vip_obj.category_id = $("#ddl_cat").val();
    vip_obj.subject = $("#txt_subject").val();
    vip_obj.date = $("#vip_date").val();
    vip_obj.start_time = $("#id_start_time").val();
    vip_obj.end_time = $("#id_end_time").val();
    vip_obj.location = "";
    vip_obj.lat = "";
    vip_obj.lang = "";
    vip_obj.created_by = "";
    vip_obj.owned_by = "";

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_Add",
        "method": "POST",
        "data": JSON.stringify(vip_obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage)
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Edit_Vip_data(id) {
    curr_id = id;
    var obj;
    for (var index = 0; index < v_data.length; index++) {
        if (id == v_data[index].id) {
            obj = v_data[index];
            break;
        }
    }
    $("#ddl_cat option").each(function () {
        if ($.trim($(this).text()) == $.trim(obj.category_name)) {
            $(this).attr('selected', 'selected');
        }
    });
    //$("#ddl_cat").val(v_data[index].category_name);
    $("#txt_subject").val(obj.subject);
    $("#vip_date").val(obj.date.split('T')[0]);
    $("#id_start_time").val(obj.start_time);
    $("#id_end_time").val(obj.end_time);


    $("#id_start_time,#id_end_time").trigger("focus");
    $("#id_start_time,#id_end_time").trigger("blur");
    $("#btn_update").show();
    $("#btn_add").hide();
}

function Update_Schedule() {
    vip_obj.category_id = $("#ddl_cat").val();
    vip_obj.subject = $("#txt_subject").val();
    vip_obj.date = $("#vip_date").val();
    vip_obj.start_time = $("#id_start_time").val();
    vip_obj.end_time = $("#id_end_time").val();
    vip_obj.location = "";
    vip_obj.lat = "";
    vip_obj.lang = "";
    vip_obj.created_by = "";
    vip_obj.owned_by = curr_id;

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_Update",
        "method": "POST",
        "data": JSON.stringify(vip_obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage)
            window.location.reload();
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function LoadList() {

    var query = "fromdate=";
    query += $("#rp_from_date").val() != undefined ? $("#rp_from_date").val() : "";

    query += "&todate=";
    query += $("#rp_to_date").val() != undefined ? $("#rp_to_date").val() : "";

    query += "&catid=";
    query += $("#rp_ddl_cat").val() != undefined && $("#rp_ddl_cat").val() != '0' ? $("#rp_ddl_cat").val() : "";
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Category_List_report?" + query,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {

            v_data = response.data;
            var html = "";
            for (var index = 0; index < response.data.length; index++) {
                var tempobj=response.data[index];
                //html += "<div class='row'><div class='col-12'><div class='card shadow mb-4'><div class='card-body'><h5 style='text-transform: uppercase;font-size: 1.1rem;margin-bottom:16px;font-weight: bold' > " + tempobj.date + " </h5><div class='row'><div class='col-3'><div style='font-size: 0.82rem;color: #475680;font-weight: 600;border-bottom: 1px solid #d4dcf3;padding-bottom: 6px;margin-bottom: 12px;'>Time : </div>  </div><div class='col-5'><div style='font-size: 0.82rem;color: #475680;font-weight: 600;border-bottom: 1px solid #d4dcf3;padding-bottom: 6px;margin-bottom: 12px;'>Category: </div></div><div class='col-4'><div style='font-size: 0.82rem;color: #475680;font-weight: 600;border-bottom: 1px solid #d4dcf3;padding-bottom: 6px;margin-bottom: 12px;'>Meeting Notice : </div></div></div>";
                html += "<div style='flex-wrap: wrap;margin-right: -0.75rem;margin-left: -0.75rem;'><div class='col-12'><div class='card shadow mb-4'><div class='card-body'><h5 class='schdng' > " + tempobj.date + " </h5><div class='row'><div class='col-3'><div style='font-size: 0.82rem;color: #475680;font-weight: 600;border-bottom: 1px solid #d4dcf3;padding-bottom: 6px;margin-bottom: 12px;'>Time : </div>  </div><div class='col-5'><div style='font-size: 0.82rem;color: #475680;font-weight: 600;border-bottom: 1px solid #d4dcf3;padding-bottom: 6px;margin-bottom: 12px;'>Category: </div></div><div class='col-4'><div style='font-size: 0.82rem;color: #475680;font-weight: 600;border-bottom: 1px solid #d4dcf3;padding-bottom: 6px;margin-bottom: 12px;'>Meeting Notice : </div></div></div>";
                
                for (var index1 = 0; index1 < response.data[index].objVipReport.length; index1++) {
                    var tempobj1 = response.data[index].objVipReport[index1];
                    
                    html += "<div class='row'><div class='col-3'><h5  class='sctime'>" + tempobj1.times + "</h5> </div><div class='col-5'><div class='m-note'> " + tempobj1.category + "</div></div><div class='col-4'><div class='m-note'> " + tempobj1.notice + " </div></div></div>";
                
                }
                html += "</div></div></div></div>";
                
            }
            
            $("#reports").html(html);
            //$('#dataTable').DataTable({
            //    destroy: true,
            //    paging: true,
            //    bInfo: false,
            //    "ordering": false,
            //    "pageLength": 10,

            //    data: response.data,
            //    columns: [


            //        { data: "date" },
            //        { data: "start_time" },
            //        { data: "end_time" },
            //        { data: "subject" },
            //        { data: "category_name" }
                
            //    ]
            //});

            //$('#dataTable').on('click', '.edit', function () {
            //    Edit_Vip_data($(this).attr('rid'));
            //});

            
        } else {
            $('#dataTable').DataTable().clear().destroy();
            alert(response.ResponseMessage);
        }
        //var html = "<option value='0'>Choose...</option>";
        //if (response.responseCode == 1) {
        //    for (var index = 0; index < response.data.length; index++) {
        //        html += "<option value='" + response.data[index].id + "'>" + response.data[index].category_name + "</option>"
        //    }
        //    $("#ddl_cat").html(html);
        //} else {
        //    $("#ddl_cat").html(html);
        //    alert(response.ResponseMessage);
        //}
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}