﻿var obj = {
    "type": "",
    "year": "",
    "month": "",
    "registratioN_NO": "",
}
var dataloaded = false;
var bdata = "";
$(document).ready(function () {
    $('.nav-item').removeClass('active');
    $('#GENERAL').addClass('show');
    $('#POL_Indent_Report').addClass('active');


    var getdate = null;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var todaydate = date.split('-');
    if (todaydate[1] != "10" && todaydate[1] != "11" && todaydate[1] != "12") {
        getdate = todaydate[2] + '-' + "0" + todaydate[1] + '-' + todaydate[0];
        $('#ddll_yyear').val(todaydate[0]);
        $('#ddll_mmonth').val("0" + todaydate[1]);
    }
    if (todaydate[1] == "10" || todaydate[1] == "11" || todaydate[1] == "12") {
        getdate = todaydate[2] + '-' + todaydate[1] + '-' + todaydate[0];

        $('#ddll_yyear').val(todaydate[0]);
         $('#ddll_mmonth').val(todaydate[1]);
    }



    $("#ddll_yyear").change(function (e) {
        if ($("#ddll_yyear").val() != "0") {

            if (dataloaded == false) {
               
                dataloaded = true;
            }
            obj.type = '8';
            obj.year = $("#ddll_yyear").val();

            //Loadmonth(obj);
            $("#ddll_mmonth").val("");
            $("#ddll_mmonth").show();

        } else {
            $("#ddll_mmonth").hide();

        }

     
    })
    $("#ddll_mmonth").change(function (e) {
        if ($("#ddll_mmonth").val() != "0") {

            if (dataloaded == false) {
             
                dataloaded = true;
            }

        } else {

        }

        load_polindent_report();
    })
});

function loadyear() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Year_Dropdown?type=7",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].year + "'>" + response.data[index].year + "</option>"
            }
            $("#ddll_yyear").html(html);
            $('#ddll_yyear').selectpicker();
        } else {
            $("#ddll_yyear").html(html);
            $('#ddll_yyear').selectpicker();
            //alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Loadmonth(details) {
    var settings = {
        //"async": true,
        //"crossDomain": true,
        //"url": "/Month_Dropdown",
        //"method": "POST",
        type: "POST",
        url: "/Month_Dropdown",
        data: JSON.stringify(details),
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].month_no + "'>" + response.data[index].month_name + "</option>"
            }
            $("#ddll_mmonth").html(html);
        } else {
            $("#ddll_mmonth").html(html);
          //  alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function load_polindent_report() {
    var query = "year=";
    query += $("#ddll_yyear").val();

    query += "&month=";
    query += $("#ddll_mmonth").val() == undefined ? 0 : $("#ddll_mmonth").val();

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/POLIndentReport?" + query,
        "method": "GET",

        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {


        if (response.responseCode == 1) {

            bdata = response.data;
           // console.log(bdata);
            $('#dataTable').DataTable({

                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: bdata.table,
                "dom": '<"dt-buttons"Bf><"clear">lirtp',
                columns: [


                    { data: "registratioN_NO" },
                    { data: "name" },
                    { data: "petrol" },
                    { data: "diesel" },
                    { data: "ecoil" },
                    { data: "ceilinglimit" },
                    { data: "excess" },
                    { data: "totalcost" }

                  
                ],
                  "footerCallback": function (row, data, start, end, display) {
                    var api = this.api();
                    nb_cols = api.columns().nodes().length;
                    //var j = 2;
                    //while (j < nb_cols) {
                    //    var pageTotal = api
                    //.column(j, { page: 'current' })
                    //.data()
                    //.reduce(function (a, b) {
                    //    return Number(a) + Number(b);
                    //}, 0);
                    //    // Update footer
                    //    $(api.column(j).footer()).html(pageTotal.toFixed(2));
                    //    j++;
                    //}
                    var pageTotal = api
                        .column(7, { page: 'current' })
                        .data()
                        .reduce(function (a, b) {
                            return Number(a) + Number(b);
                        }, 0);
                    // Update footer
                    $(api.column(7).footer()).html(pageTotal.toFixed(2));
                },
                "buttons": [
                    {

                        extend: 'pdfHtml5',
                        footer: true,
                        text: 'Download Pdf',
                        filename: 'POL Indent_report',
                        title: 'POL Indent report',
                        exportOptions: {
                            columns: [0,1, 2, 3, 4, 5, 6, 7],
                            search: 'applied',
                            order: 'applied'
                        },
                        customize: function (doc) {

                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                            doc['header'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            text: ['Created on: ', { text: jsDate.toString() }]
                                        },
                                        {
                                            alignment: 'right',
                                            text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                        }
                                    ],
                                    margin: 20
                                }
                            });


                            var tblBody = doc.content[1].table.body;
                            for (var i = 0; i < tblBody[0].length; i++) {
                                tblBody[0][i].fillColor = '#FFFFFF';
                                tblBody[0][i].color = 'black';
                            }

                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) { return .5; };
                            objLayout['vLineWidth'] = function (i) { return .5; };
                            objLayout['hLineColor'] = function (i) { return '#aaa'; };
                            objLayout['vLineColor'] = function (i) { return '#aaa'; };
                            objLayout['paddingLeft'] = function (i) { return 4; };
                            objLayout['paddingRight'] = function (i) { return 4; };
                            doc.content[0].layout = objLayout;
                            doc.content[1].layout = 'Borders';

                        }
                        //exportOptions: {
                        //    modifier: {
                        //        page: 'current'
                        //    }
                        //}
                    }
                ]

            });
            
        } else {

            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "Login";
        }

    });

}

