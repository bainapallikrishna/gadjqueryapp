﻿$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#MATERIAL').addClass('show');
    $('#InvId').addClass('active');

    loadInventoryDetails();
    loaditemCategory();
});
var _data = {
    "categoryid": "",
    "categoryname": "",
    "itemid": "",
    "itemname": "",
    "v01name": "",
    "v01maker": "",
    "v01price": "",
    "v01status": "",
    "v02name": "",
    "v02maker": "",
    "v02price": "",
    "v02status": ""
}

function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

$('#btnaddvendor').click(function () {
    clearfields();
    loadvendordetails();
    $('#txtvendor1').attr('disabled', true);
    $('#txtvendor2').attr('disabled', true);
    $('#exampleModalLabel').text("Add New");
    $('#btnvendorUpdate').hide();
    $('#btnvendorSave').show();
    $("#exampleModal").modal('show');
});

function clearfields() {
    $('#ddlCategory').val("");
    $('#txtitem').val("");
    $('#txtvendor1').val("");
    $('#txtVendor1Maker').val("");
    $('#txtVendor1Price').val("");
    $('#txtVendor1Status').val("");
    $('#txtvendor2').val("");
    $('#txtVendor2Maker').val("");
    $('#txtVendor2Price').val("");
    $('#txtVendor2Status').val("");
}

$('#btnvendorSave').click(function () {

    if ($('#ddlCategory').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select Category", "warning"); $("#ddlCategory").focus(); return;
    }
    else if ($('#txtitem').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Item", "warning"); $("#txtitem").focus(); return;
    }
    else if ($('#txtVendor1Maker').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor1Maker", "warning"); $("#txtVendor1Maker").focus(); return;
    }
    else if ($('#txtVendor1Price').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor1Price", "warning"); $("#txtVendor1Price").focus(); return;
    }
    else if ($('#txtVendor1Status').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor1Status", "warning"); $("#txtVendor1Status").focus(); return;
    }
    else if ($('#txtVendor2Maker').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor2Maker", "warning"); $("#txtVendor2Maker").focus(); return;
    }
    else if ($('#txtVendor2Price').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor2Price", "warning"); $("#txtVendor2Price").focus(); return;
    }
    else if ($('#txtVendor2Status').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor2Status", "warning"); $("#txtVendor2Status").focus(); return;
    }
    else {
        _data.categoryid = $('#ddlCategory').val();
        _data.categoryname = $('#ddlCategory option:selected').text();
        _data.itemid = "";
        _data.itemname = $('#txtitem').val();
        _data.v01name = $('#txtvendor1').val();
        _data.v01maker = $('#txtVendor1Maker').val();
        _data.v01price = $('#txtVendor1Price').val();
        _data.v01status = $('#txtVendor1Status').val();
        _data.v02name = $('#txtvendor2').val();
        _data.v02maker = $('#txtVendor2Maker').val();
        _data.v02price = $('#txtVendor2Price').val();
        _data.v02status = $('#txtVendor2Status').val();
        InsertInventorydata(_data);
    }
});

function loadInventoryDetails() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetInventory_Details",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            $('#dataTable1').dataTable().fnClearTable();
            var i = 1;
            $('#dataTable1').DataTable({
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
                "displayLength": 100,
                destroy: true,
                "scrollY": "62vh",
                "scrollCollapse": true,
                paging: true,
                bInfo: true,
                "ordering": true,
                data: response.inventoryList,

                //"drawCallback": function (settings) {




                //    //var api = this.api();
                //    //var rows = api.rows({ page: 'current' }).nodes();
                //    //var last = null;

                //    //api.column({ page: 'current' }).data().each(function (group, i) {
                //    //    if (last !== group) {
                //    //        $(rows).eq(i).before(
                //    //            '<tr class="group"><th rowspan="2" class="tbl-mdhdr">' + group + '</td></tr>'
                //    //        );

                //    //        last = group;
                //    //    }

                //    //});
                //},
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    { data: "categoryname", "sWidth": "5%" },
                    { data: "itemname", "sWidth": "5%" },
                    //{ data: "v01name", "sWidth": "9%" },
                    { data: "v01maker" },
                    { data: "v01price" },
                    { data: "v01status" },
                    //{ data: "v02name", "sWidth": "9%" },
                    { data: "v02maker" },
                    { data: "v02price" },
                    { data: "v02status" },
                    {
                        "mData": null,
                        "mRender": function (data, type, s) {
                            return '<ul class="list-inline"><li class="list-inline-item"><a href="javascript: void (0);" class="EditAttributes" Ecategoryid="' + s["categoryid"] + '" Ecategoryname="' + s["categoryname"] + '" Eitemid="' + s["itemid"] + '" Eitemname="' + s["itemname"] + '" Ev01name="' + s["v01name"] + '" Ev01maker="' + s["v01maker"] + '" Ev01price="' + s["v01price"] + '" Ev01status="' + s["v01status"] + '" Ev02name="' + s["v02name"] + '" Ev02maker="' + s["v02maker"] + '" Ev02price="' + s["v02price"] + '" Ev02status="' + s["v02status"] + '"><i class="far fa-edit" ></i></a></li></ul>'
                        }
                    }
                ]


            });
            for (var i = 0; i < response.inventoryList.length; i++) {
                if (i == 1) {
                    $('#VENDOR1').html(response.inventoryList[i].v01name);
                    $('#VENDOR2').html(response.inventoryList[i].v02name);
                    
                  //  $('#dataTable1 tr:eq(0) th:eq(4)').html(response.inventoryList[i].v02name);
                }

            }
        } else {
            $('#dataTable1').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}

$('#dataTable1').on('click', '.EditAttributes', function () {


    EditInventorydata($(this).attr('Ecategoryid'), $(this).attr('Ecategoryname'), $(this).attr('Eitemid'), $(this).attr('Eitemname'), $(this).attr('Ev01name'), $(this).attr('Ev01maker'), $(this).attr('Ev01price'), $(this).attr('Ev01status'), $(this).attr('Ev02name'), $(this).attr('Ev02maker'), $(this).attr('Ev02price'), $(this).attr('Ev02status'));
});


function EditInventorydata(Ecategoryid, Ecategoryname, Eitemid, Eitemname, Ev01name, Ev01maker, Ev01price, Ev01status, Ev02name, Ev02maker, Ev02price, Ev02status) {

    sessionStorage.setItem("itemid", Eitemid);

    $('#txtvendor1').attr('disabled', false);
    $('#txtvendor2').attr('disabled', false);
    // $('#ddlCategory').val(Ecategoryid);

    $('#ddlCategory option').each(function () {
        if ($(this).val() == Ecategoryid) {
            $(this).prop('selected', true);

        }
    });



    //$('#ddlCategory :selected').text(Ecategoryname);
    $('#txtitem').val(Eitemname);
    $('#txtvendor1').val(Ev01name);
    $('#txtVendor1Maker').val(Ev01maker);
    $('#txtVendor1Price').val(Ev01price);
    $('#txtVendor1Status').val(Ev01status);
    $('#txtvendor2').val(Ev02name);
    $('#txtVendor2Maker').val(Ev02maker);
    $('#txtVendor2Price').val(Ev02price);
    $('#txtVendor2Status').val(Ev02status);

    $('#exampleModalLabel').text("Edit");
    $('#btnvendorUpdate').show();
    $('#btnvendorSave').hide();
    $("#exampleModal").modal('show');
}

$('#btnvendorUpdate').click(function () {
    if ($('#ddlCategory').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select Category", "warning"); $("#ddlCategory").focus(); return;
    }
    else if ($('#txtitem').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Item", "warning"); $("#txtitem").focus(); return;
    }
    else if ($('#txtVendor1Maker').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor1Maker", "warning"); $("#txtVendor1Maker").focus(); return;
    }
    else if ($('#txtVendor1Price').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor1Price", "warning"); $("#txtVendor1Price").focus(); return;
    }
    else if ($('#txtVendor1Status').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor1Status", "warning"); $("#txtVendor1Status").focus(); return;
    }
    else if ($('#txtVendor2Maker').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor2Maker", "warning"); $("#txtVendor2Maker").focus(); return;
    }
    else if ($('#txtVendor2Price').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor2Price", "warning"); $("#txtVendor2Price").focus(); return;
    }
    else if ($('#txtVendor2Status').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Vendor2Status", "warning"); $("#txtVendor2Status").focus(); return;
    }
    else {
        _data.categoryid = $('#ddlCategory').val();
        _data.categoryname = $('#ddlCategory option:selected').text();
        _data.itemid = sessionStorage.getItem("itemid");
        _data.itemname = $('#txtitem').val();
        _data.v01name = $('#txtvendor1').val();
        _data.v01maker = $('#txtVendor1Maker').val();
        _data.v01price = $('#txtVendor1Price').val();
        _data.v01status = $('#txtVendor1Status').val();
        _data.v02name = $('#txtvendor2').val();
        _data.v02maker = $('#txtVendor2Maker').val();
        _data.v02price = $('#txtVendor2Price').val();
        _data.v02status = $('#txtVendor2Status').val();
        UpdateInventorydata(_data);
    }
})

function loaditemCategory() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetItemCategoryDrpdwn?categoryid=",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "<option value=''>Choose...</option>";
        if (response.status == "Success") {
            _n_plain_mes_1("<strong>Data Loaded Successfully!</strong>", "", "success");
            for (var index = 0; index < response.itemCategorylist.length; index++) {
                html += "<option value='" + response.itemCategorylist[index].categoryid + "'>" + response.itemCategorylist[index].category + "</option>"
            }
            $("#ddlCategory").html(html);
            //$('#ddlCategory').selectpicker();
        } else {
            $("#ddlCategory").html(html);
            //$('#ddlCategory').selectpicker();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}

function loadvendordetails() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetVendorDetails",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            _n_plain_mes_1("<strong>Data Loaded Successfully!</strong>", "", "success");

            for (var index = 0; index < response.vendornames.length; index++) {
                if (index == 0) {
                    $('#txtvendor1').val(response.vendornames[index]["vendor"]);
                }
                if (index == 1) {
                    $('#txtvendor2').val(response.vendornames[index]["vendor"]);
                }
                else {

                }
            }
        } else {

            $('#txtvendor1').val("");
            $('#txtvendor2').val("");
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}

function InsertInventorydata(details) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../InsertinventoryDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#exampleModal").modal('hide');
            clearfields();
            loadInventoryDetails();
            _n_plain_mes_1("<strong>success!</strong>", "Data Inserted successfully", "success"); return;
        } else {
            $("#exampleModal").modal('hide');
            clearfields();
            _n_plain_mes_1("<strong>fail!</strong>", "Data Insertion fail..!", "warning"); return;
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            $("#exampleModal").modal('hide');
            clearfields();
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}

function UpdateInventorydata(details) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../UpdateInventoryDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#exampleModal").modal('hide');
            clearfields();
            loadInventoryDetails();
            _n_plain_mes_1("<strong>success!</strong>", "Data Updated successfully", "success"); return;
        } else {
            $("#exampleModal").modal('hide');
            clearfields();
            _n_plain_mes_1("<strong>fail!</strong>", "Data Updation fail..!", "warning"); return;
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            $("#exampleModal").modal('hide');
            clearfields();
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}