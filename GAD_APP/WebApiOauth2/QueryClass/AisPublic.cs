﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiOauth2.QueryClass
{
    public class AisPublic
    {

       
    }
    public class Ais_OfficersDetails
    {
        public string sno { get; set; }
        public string emp_id { get; set; }
        public string service_id { get; set; }
        public string emp_name { get; set; }
        public string DOA { get; set; }
        public string Source { get; set; }
        public string Qualification { get; set; }
        public string Dob { get; set; }
        public string Year { get; set; }
        public string Domicile { get; set; }
        public string Present_Post { get; set; }
        public string Present_Post_Effected_Date { get; set; }
        public string PayScale { get; set; }
        public string EmailId { get; set;}
        public string Mobileno { get; set; }
        public string Posting_Office_Phoneno { get; set; }
    }
    public class Get_Ais_OfficersDetails
    {
        public string Status { get; set; }
        public string Reason { get; set; }
        public List<Ais_OfficersDetails> AISOfficerMaster { get; set; }

    }
}