﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebApiOauth2.QueryClass;
namespace WebApiOauth2.QueryClass
{
   
    public class Ais_Validation
    {
        Ais_Connection _con = new Ais_Connection();
        AisQueries _query = new AisQueries();
        
        DataSet ds;
        public dynamic Ais_Officers_List(Ais_OfficersDetails obj)
        {
            Get_Ais_OfficersDetails Getofficers = new Get_Ais_OfficersDetails();
            Getofficers.AISOfficerMaster = new List<Ais_OfficersDetails>();
            ds = (DataSet)_con.retds(_query.GetOfficersList(obj),"");
            if(ds!=null)
            {
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    Getofficers.Status = "1";
                    Getofficers.Reason = "Success";
                    foreach (DataRow dr in ds.Tables[i].Rows)
                    {
                        Ais_OfficersDetails obj1 = new Ais_OfficersDetails();
                        obj1.sno = dr["sno"].ToString();
                        obj1.emp_id = dr["emp_id"].ToString();
                        obj1.emp_name = dr["emp_name"].ToString();                       
                        obj1.DOA = dr["date_of_entry_into_service"].ToString();
                        obj1.Source = dr["source_name"].ToString();
                        obj1.Qualification = dr["Qualification"].ToString();
                        obj.Dob = dr["dob"].ToString();
                        obj1.Year = dr["year"].ToString();
                        obj1.Domicile = dr["state_name"].ToString();
                        obj1.Present_Post = dr["post_name"].ToString();
                        obj1.Present_Post_Effected_Date = dr["doj_present_post"].ToString();
                        obj1.PayScale = dr["level_name"].ToString();
                        obj1.EmailId = dr["emailid"].ToString();
                        obj1.Mobileno = dr["mobile1"].ToString();
                        obj1.Posting_Office_Phoneno = dr["Posting_Office_Phoneno"].ToString();
                        Getofficers.AISOfficerMaster.Add(obj1);
                    }
                }
            }
            else
            {
                Getofficers.Status = "2";
                Getofficers.Reason = "No Data Available ";
            }
            return Getofficers;
        }
    }
}