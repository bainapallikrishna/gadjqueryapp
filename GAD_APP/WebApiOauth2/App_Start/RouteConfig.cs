﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApiOauth2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            
            routes.MapPageRoute("Accommodation", "Accommodation", "~/ContentPages/Accommodation.aspx");
            routes.MapPageRoute("ACRApplication", "ACRApplication", "~/ContentPages/ACRApplication.aspx");
            routes.MapPageRoute("Login", "Login", "~/ContentPages/Login.aspx");
            routes.MapPageRoute("Cabinet", "Cabinet", "~/ContentPages/Cabinet.aspx");
            routes.MapPageRoute("VIP_Schedule", "VIP_Schedule", "~/ContentPages/VIP_Schedule.aspx");
            routes.MapPageRoute("VIP_Report", "VIP_Report", "~/ContentPages/VIP_Report.aspx");
            routes.MapPageRoute("GrievanceForm", "GrievanceForm", "~/ContentPages/GrievanceForm.aspx");
            routes.MapPageRoute("File_CodeMaster", "File_CodeMaster", "~/ContentPages/File_CodeMaster.aspx");
            routes.MapPageRoute("GuestHouse_Request", "GuestHouse_Request", "~/ContentPages/GuestHouse.aspx");
            routes.MapPageRoute("GuestHouse_Approve", "GuestHouse_Approve", "~/ContentPages/GuestHouse_Approve.aspx");
            routes.MapPageRoute("Catering", "Catering", "~/ContentPages/Catering.aspx");
            routes.MapPageRoute("Room_Master", "Room_Master", "~/ContentPages/Room_Master.aspx");
            routes.MapPageRoute("State_GuestHouse", "State_GuestHouse", "~/ContentPages/StateGuests.aspx");
            routes.MapPageRoute("GuestHouse_Daily_Report", "GuestHouse_Daily_Report", "~/ContentPages/GHSDailyStatusReport.aspx");
            routes.MapPageRoute("Vehicles", "Vehicles", "~/ContentPages/Vehicles.aspx");
            routes.MapPageRoute("Vehicle_Bill_Amount", "Vehicle_Bill_Amount", "~/ContentPages/Monthly_Wise_Vehicle_Bill_Amount.aspx");
            routes.MapPageRoute("POL_Indent_Report", "POL_Indent_Report", "~/ContentPages/POL_Indent_Report.aspx");
            routes.MapPageRoute("Inventory", "Inventory", "~/ContentPages/Inventory.aspx");
            routes.MapPageRoute("Transport", "Transport", "~/ContentPages/Transport.aspx");
            routes.MapPageRoute("FTR_Report", "FTR_Report", "~/ContentPages/FTRForm16Report.aspx");
            routes.MapPageRoute("Home", "Home", "~/ContentPages/HomePage.aspx");
            routes.MapPageRoute("UserPermission", "UserPermission", "~/ContentPages/UserPermission.aspx");

            routes.MapRoute(
                name: "Default",
                url: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
