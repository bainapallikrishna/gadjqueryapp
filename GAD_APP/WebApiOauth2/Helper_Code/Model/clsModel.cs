﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebApiOauth2
{
    public class clsForm_A_Part1
    {

        string FromDate { set; get; }
        string TO_DATE { set; get; }
        string POINT_1 { set; get; }
        string POINT2_1 { set; get; }
        string POINT2_2 { set; get; }
        string POINT2_3 { set; get; }
        string POINT2_4 { set; get; }
        string POINT2_5 { set; get; }

        string POINT3_A { set; get; }
        string POINT3_B { set; get; }
        string POINT_4 { set; get; }
    }
    public class clsRoomDetails
    {
        public string BULDING_NO { get; set; }
        public string FLOOR { get; set; }
        public string DEPT_CODE { get; set; }
        public string ROOM_NO { get; set; }
        public string ROOM_TYPE { get; set; }
        public string ROOM_CATEGORY { get; set; }
        public string ROOM_ALLOTTED { get; set; }
        public string NOTE { get; set; }
        public string ROOM_MODIFICATION { get; set; }
        public string AMENITIES { get; set; }
    }

    public class clsCabinet
    {
        public string sno { get; set; }
        public string Date { get; set; }
        public string Cr_no { get; set; }
        public string Dept_id { get; set; }
        public string Title { get; set; }
        public string Resolution { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
    }
    public class clsSchedule
    {
        public string category_id { get; set; }
        public string subject { get; set; }
        public string date { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public string location { get; set; }
        public string lat { get; set; }
        public string lang { get; set; }
        public string created_by { get; set; }
        public string owned_by { get; set; }
    }
    public class clsGrievance
    {
        public string EmpID { get; set; }
        public string PERSON_NAME { get; set; }
        public string DESGNATION { get; set; }
        public string REQUSET_OWNED { get; set; }
        public string DATE_OF_REQUEST { get; set; }
        public string REQUEST_GIST { get; set; }
        public string UPLOAD_PATH { get; set; }
    }

    public class clsVip
    {
        public string date { get; set; }
        public string Times { get; set; }
        public string Notice { get; set; }
        public string Category { get; set; }
    }
    public class clsVipReport
    {
        public string Date { get; set; }
        public List<clsVip> objVipReport { get; set; }
    }

    public class clsGuestHouse_Request
    {
        public string ghs_id { get; set; }
        public string room_type { get; set; }
        public string guest_name { get; set; }
        public string room_number { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string category { get; set; }
        public string remarks { get; set; }
        public string RoomStatus { get; set; }
        public string OrderID { get; set; }
    }


    public class clsGuestHouse_Admin_Request
    {
        public string ID { get; set; }
        public string GHS_ID { get; set; }
        public string ROOM_Type { get; set; }
        public string guest_name { get; set; }
        public string designation { get; set; }
        public string mobile_no { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
        public string purpose_of_visit { get; set; }
        public string vechile_allotment { get; set; }
        public string reporting_time { get; set; }
        public string note { get; set; }


        public string Room_no { get; set; }
        public string Room_status { get; set; }
        public string V_no { get; set; }
        public string driver_id { get; set; }
        public string  order { get; set; }
        public string id_proof_type { get; set; }
        public string id_proof { get; set; }
        public string adminnote { get; set; }

    }

    public class clsCatering
    {
        public List<clsitems> ObjItems { get; set; }
        public string PersonName { get; set; }
        public string MobileNumber { get; set; }
        public string Meeting { get; set; }
        public string MeetingDate { get; set; }
        public string MeetingTime { get; set; }
        public string Venue { get; set; }
        public string OrderID { get; set; }

    }
    public class clsitems
    {
        public string Name { get; set; }
        public string Quantity { get; set; }
    }

    public class clsRoomMaster
    {
        public string GHID { get; set; }
        public string RoomType { get; set; }
        public string RoomFloor { get; set; }
        public string RoomNo { get; set; }
        //public string Status { get; set; }
        public string AMENITIES { get; set; }
        public string Category { get; set; }

    }

    public class clsStateGuest
    {
        public string Status { get; set; }
        public string Reason { get; set; }
        public List<clsStateGuestlist> stateGuestlists { get; set; }
    }

    public class clsStateGuestlist
    {
        public string DateOfOrder { get; set; }
        public string Guests { get; set; }
        public string VisitPlaces { get; set; }
        public string Fromdate { get; set; }
        public string ToDate { get; set; }
        public string Remark { get; set; }
        public string id { get; set; }
    }
    public class clsGetStateGuest
    {
        public string year { get; set; }
        public string month { get; set; }
        public string date { get; set; }
    }

    public class clsVehicleStatus
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsVehicleStatuslist> vehicleStatuslist { get; set; }
    }

    public class clsVehicleStatuslist
    {
        public string vehicleno { get; set; }
        public string vehiclemodel { get; set; }

        public string vehicletype { get; set; }
        public string driver { get; set; }
        public string allotedname { get; set; }
        public string remark { get; set; }
    }

    public class clsGuesthouseDaily
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsGuesthouseList> GuesthouseList { get; set; }
    }
    public class clsGuesthouseList
    {
        public string ghsid { get; set; }
        public string roomno { get; set; }
        public string roomtype { get; set; }
        public string name_Guest { get; set; }
        public string name_designation_Guest { get; set; }
        public string designation_Guest { get; set; }
        public string dateofarrival { get; set; }
        public string dateofdeparture { get; set; }
        public string category { get; set; }
        public string reamrks { get; set; }
        public string id { get; set; }
        public string roomfloor { get; set; }
        
    }


   
    public class clsGuesthousecheckoutList
    {
        public string checkout_status { get; set; }
        public string id { get; set; }
        public string ghsid { get; set; }
        public string checkout_date { get; set; }
    }

    public class clsVehicleDetails
    {
        public string REGISTRATION_NO { get; set; }
        public string NAME_OF_PERTOL_BUNK { get; set; }
        public string VHL_DATE { get; set; }
        public string COUPON_NO { get; set; }
        public string QUANTITY { get; set; }
        public string PRICE { get; set; }
        public string POL_TYPE { get; set; }
        public string TOTAL_COST { get; set; }
        public string OFFICER_USING_VEHICLE { get; set; }
        public string CEILING_LIMIT_FIXED { get; set; }
        public string EXCESS_CONSUMPTION { get; set; }
        public string CONSUMPTION_OF_POL { get; set; }
        public string REQUESTED_ID { get; set; }
    }

    public class clsVehicleDetailsUpdate
    {
        public string VEHICLE_NO { get; set; }
        public string DEPT_NAME { get; set; }
        public string DESIGNATION { get; set; }
        public string CEILING_LIMIT_FIXED { get; set; }
        public string NAME { get; set; }
        public string MOBILE { get; set; }
        public string REMARKS { get; set; }
    }

    public class clsYearMonth
    {
        public string type { get; set; }
        public string year { get; set; }

    }
    public class clsmonthdropdown
    {
        public string month { get; set; }
        public List<clsYearMonth> objmonth { get; set; }
    }

    public class clsInventoryDetails
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsinventoryList> inventoryList { get; set; }
    }
    public class clsinventoryList
    {
        public string categoryid { get; set; }
        public string categoryname { get; set; }
        public string itemid { get; set; }
        public string itemname { get; set; }
        public string v01name { get; set; }
        public string v01maker { get; set; }
        public string v01price { get; set; }
        public string v01status { get; set; }
        public string v02name { get; set; }
        public string v02maker { get; set; }
        public string v02price { get; set; }
        public string v02status { get; set; }
    }
    public class clsItemCategorydrp
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsItemCategorylist> itemCategorylist { get; set; }
    }
    public class clsItemCategorylist
    {
        public string category { get; set; }
        public string categoryid { get; set; }
    }
    public class clsItemsdrp
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsItemSlist> itemslist { get; set; }
    }
    public class clsItemSlist
    {
        public string itemname { get; set; }
        public string itemid { get; set; }
    }

    public class clsTransVehicleCounts
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsvehiclecountlist> countlist { get; set; }
    }
    public class clsvehiclecountlist
    {
        public string condemnation { get; set; }
        public string operationalvehicle { get; set; }
        public string to_be_condemned { get; set; }
        public string under_repair { get; set; }
        public string total { get; set; }
    }
    public class clsExpenditureDetails
    {

        public string REGISTRATION_NO { get; set; }
        public string CAPTURE_DATE { get; set; }
        public string REPLACED_SERVICING { get; set; }
        public string NAME_OF_THE_GARAGE { get; set; }
        public string AMOUNT { get; set; }
    }
    public class clsbillDetailsUpdate
    {
        public string ID { get; set; }
        public string REGISTRATION_NO { get; set; }
        public string NAME_OF_PERTOL_BUNK { get; set; }
        public string DATE { get; set; }
        public string COUPON_NO { get; set; }
        public string QUANTITY { get; set; }
        public string PRICE { get; set; }
        public string POL_TYPE { get; set; }
        public string TOTAL_COST { get; set; }
    }
    public class clsexpenditureDetailsUpdate
    {
       
        public string ID { get; set; }
        public string REGISTRATION_NO { get; set; }
        public string CAPTURE_DATE { get; set; }
        public string REPLACED_SERVICING { get; set; }
        public string NAME_OF_THE_GARAGE { get; set; }
        public string AMOUNT { get; set; }
       
    }

    public class clsFTRappliedDetails
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsFtrappliedList> ftrappliedlist { get; set; }
    }

    public class clsFtrappliedList
    {
        public string refnum { get; set; }
        public string formnum { get; set; }
        public string departname { get; set; }
        public string personcount { get; set; }
        public string createddate { get; set; }
        public string subject { get; set; }
    }


    public class clsFTRapprovedDetails
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsFtrapprovedList> ftrapprovedlist { get; set; }
    }

    public class clsFtrapprovedList
    {
        public string refnum { get; set; }
        public string formnum { get; set; }
        public string departname { get; set; }
        public string personcount { get; set; }
        public string createddate { get; set; }
        public string subject { get; set; }
    }
    public class clsFTRmemberDetails
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsFtrmemberList> ftrmemberlist { get; set; }
    }

    public class clsFtrmemberList
    {
        public string gdname { get; set; }
        public string gdservice { get; set; }
        public string gduid { get; set; }
        public string gddepartment { get; set; }
        public string gddesignation { get; set; }
    }


    public class clsLoginMenuDetails
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsmenuList> menuList { get; set; }
    }

    public class clsmenuList
    {
        public string userid { get; set; }
        public string username { get; set; }
        public string url { get; set; }
        public string groupid { get; set; }
        public string groupname { get; set; }
        public string pageid { get; set; }
        public string pagename { get; set; }
    }

    public class Insertuserscreenmodel
    {
   
  
        public List<Screennamelist> userscreenname { get; set; }
    

    }
    public class Screennamelist
    {
        public string userid { get; set; }
        public string groupid { get; set; }
        public string pageid { get; set; }

    }

    public class clsscreenmapDetails
    {
        public string status { get; set; }
        public string reason { get; set; }
        public List<clsscreenmapList> screenmapList { get; set; }
    }

    public class clsscreenmapList
    {
        public string pagename { get; set; }
        public string pageid { get; set; }
        public string groupid { get; set; }
        public string userid { get; set; }
    }

}