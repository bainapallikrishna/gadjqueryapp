﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;

namespace WebApiOauth2
{
    public class Connection
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_Path"].ToString());
        SqlCommand cmd;
        SqlDataAdapter Sda;
      //  SqlDataReader Sdr;
        DataTable dt;
        public bool Insert(string query, string keyvalue)
        {
            try
            {
                cmd = new SqlCommand();
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Open();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }
        public DataSet retds(string query, string keyarea)
        {
            DataSet ds = new DataSet();
            try
            {
                cmd = new SqlCommand(query, con);
                Sda = new SqlDataAdapter(cmd);
                Sda.Fill(ds);
                cmd.Dispose();
                Sda.Dispose();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                    Sda.Dispose();
                    ds.Dispose();
                }

                return null;
            }
        }
        public DataTable retdt(string query, string keyarea)
        {
            try
            {
                cmd = new SqlCommand(query, con);
                Sda = new SqlDataAdapter(cmd);
                dt = new DataTable();
                Sda.Fill(dt);
                cmd.Dispose();
                Sda.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }

            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                    Sda.Dispose();
                    dt.Dispose();
                }

                return null;
            }
        }
    }
}