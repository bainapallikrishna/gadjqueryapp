﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Security.Cryptography;

namespace WebApiOauth2
{
    public class clsQuery
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_Path"].ToString());
        SqlConnection cabinet = new SqlConnection(ConfigurationManager.ConnectionStrings["cabinet"].ToString());
        SqlConnection vip = new SqlConnection(ConfigurationManager.ConnectionStrings["vip"].ToString());
        SqlConnection grievance = new SqlConnection(ConfigurationManager.ConnectionStrings["grievance"].ToString());
        SqlConnection eoffice = new SqlConnection(ConfigurationManager.ConnectionStrings["eoffice"].ToString());
        SqlConnection ais = new SqlConnection(ConfigurationManager.ConnectionStrings["ais"].ToString());
        SqlConnection ESR_SEC = new SqlConnection(ConfigurationManager.ConnectionStrings["ESR_SEC"].ToString());
        SqlConnection FTR_CON = new SqlConnection(ConfigurationManager.ConnectionStrings["FTR_CON"].ToString());
        SqlConnection GADROLECON = new SqlConnection(ConfigurationManager.ConnectionStrings["GAD_ROle_CON"].ToString());
        
        SqlCommand cmd;
        SqlDataAdapter dap;

        DataTable dt;

        #region Accmmodation
        public DataTable LoadDropdowns(string type)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", type);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        public DataSet RoomList(string btype, string rmtype, string cat)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_GET_ROOMS_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BULDING_FLOOR", btype);
                cmd.Parameters.AddWithValue("@ROOM_STATUS", rmtype);
                cmd.Parameters.AddWithValue("@ROOM_CATEGORY", cat);


                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }
        public DataSet ROOM_ABSTRACT(string btype, string cat)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_GET_ROOM_ABSTRACT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BULDING_FLOOR", btype);
                cmd.Parameters.AddWithValue("@ROOM_CATEGORY", cat);
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }

        public DataSet ROOM_ABSTRACT_List()
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_GET_ROOM_ABSTRACT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BULDING_FLOOR", '0');
                cmd.Parameters.AddWithValue("@ROOM_CATEGORY", '0');
                cmd.Parameters.AddWithValue("@ALL_BULDING", '0');
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Alloted_GET()
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_GET_ALLOTTED_FOR", con);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        public DataTable updateRoomDetails(clsRoomDetails obj)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_UPD_ROOM_DETAILS", con);

                cmd.Parameters.AddWithValue("@BULDING_NO", obj.BULDING_NO);
                cmd.Parameters.AddWithValue("@FLOOR", obj.FLOOR);
                cmd.Parameters.AddWithValue("@DEPT_CODE", obj.DEPT_CODE);
                cmd.Parameters.AddWithValue("@ROOM_NO", obj.ROOM_NO);
                cmd.Parameters.AddWithValue("@ROOM_TYPE", obj.ROOM_TYPE);

                cmd.Parameters.AddWithValue("@ROOM_CATEGORY", obj.ROOM_CATEGORY);
                cmd.Parameters.AddWithValue("@ROOM_ALLOTTED", obj.ROOM_ALLOTTED);
                cmd.Parameters.AddWithValue("@NOTE", obj.NOTE);
                //CultureInfo culture = new CultureInfo("en-US");

                cmd.Parameters.AddWithValue("@ROOM_MODIFICATION", getdate(obj.ROOM_MODIFICATION));
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        public DataTable updateRoom_Amenites(clsRoomDetails obj)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_BLD_INS_ROOM_AMENITIES", con);

                cmd.Parameters.AddWithValue("@BULDING_NO", obj.BULDING_NO);
                cmd.Parameters.AddWithValue("@FLOOR", obj.FLOOR);
                cmd.Parameters.AddWithValue("@ROOM_NO", obj.ROOM_NO);
                cmd.Parameters.AddWithValue("@AMENITIES_ID", obj.AMENITIES);

                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //   writelogs(ex.Message);
                return null;
            }
        }
        #endregion

        #region Cabinet
        public DataTable Add_New_Cabinet(clsCabinet obj)
        {
            try
            {
                cabinet.Open();
                cmd = new SqlCommand("sp_create_new_cabinet_decision", cabinet);
                //DateTime result = DateTime.ParseExact(obj.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                cmd.Parameters.AddWithValue("@date", getdate(obj.Date));
                cmd.Parameters.AddWithValue("@cr_no", obj.Cr_no);
                cmd.Parameters.AddWithValue("@dept_id", obj.Dept_id);
                cmd.Parameters.AddWithValue("@title", obj.Title);
                cmd.Parameters.AddWithValue("@resolution", obj.Resolution);
                cmd.Parameters.AddWithValue("@status", obj.Status);
                cmd.Parameters.AddWithValue("@remarks", obj.Remarks);

                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cabinet.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (cabinet.State == ConnectionState.Open)
                {
                    cabinet.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        public DataTable Cabinet_List(string fromdate, string todate, string deptid, string search)
        {
            try
            {
                writelogs(fromdate + todate + deptid);
                cabinet.Open();
                cmd = new SqlCommand("sp_get_cabinet_decisions_details", cabinet);


                cmd.Parameters.AddWithValue("@from_date", getdate(fromdate));
                cmd.Parameters.AddWithValue("@to_date", getdate(todate));


                cmd.Parameters.AddWithValue("@dept_id", deptid);
                cmd.Parameters.AddWithValue("@search_string", search);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cabinet.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (cabinet.State == ConnectionState.Open)
                {
                    cabinet.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        public string getdate(string date)
        {

            if (date != null && date != "" && date.Contains('/') && date.Split('/')[0].Length == 2)
            {
                return date.Split('/')[2] + "/" + date.Split('/')[1] + "/" + date.Split('/')[0];
            }
            else if (date != null && date != "" && date.Contains('-') && date.Split('-')[0].Length == 2)
            {
                return date.Split('-')[2] + "/" + date.Split('-')[1] + "/" + date.Split('-')[0];
            }
            else if (date != null && date != "")
            {
                return date;
            }
            else
            {
                return null;
            }
        }
        public DataTable Update_Cabinet(clsCabinet obj)
        {
            try
            {
                //@sno varchar(10) ,
                //@title as nvarchar(max),
                //@resolution as nvarchar(max), @status as nvarchar(max), @remarks as nvarchar(max)
                cabinet.Open();
                cmd = new SqlCommand("sp_edit_cabinet_decision", cabinet);

                cmd.Parameters.AddWithValue("@sno", obj.sno);
                cmd.Parameters.AddWithValue("@title", obj.Title);
                cmd.Parameters.AddWithValue("@resolution", obj.Resolution);
                cmd.Parameters.AddWithValue("@status", obj.Status);
                cmd.Parameters.AddWithValue("@remarks", obj.Remarks);

                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cabinet.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (cabinet.State == ConnectionState.Open)
                {
                    cabinet.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        #endregion

        #region VIP
        public DataTable Category_details()
        {
            try
            {

                vip.Open();
                cmd = new SqlCommand("sp_get_diary_schedule_category_details", vip);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                vip.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (vip.State == ConnectionState.Open)
                {
                    vip.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        //exec diary_schedule.dbo.sp_create_new_diary_schedule 'category_id','subject','date','start_time','end_time','location','lat','long','created_by','owned_by' ;

        public DataTable Create_new_schedule(clsSchedule obj)
        {
            try
            {

                vip.Open();
                cmd = new SqlCommand("sp_create_new_diary_schedule", vip);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@category_id", obj.category_id);
                cmd.Parameters.AddWithValue("@subject", obj.subject);
                cmd.Parameters.AddWithValue("@date", getdate(obj.date));
                cmd.Parameters.AddWithValue("@start_time", obj.start_time);
                cmd.Parameters.AddWithValue("@end_time", obj.end_time);
                cmd.Parameters.AddWithValue("@location", obj.location);
                cmd.Parameters.AddWithValue("@lat", obj.lat);
                cmd.Parameters.AddWithValue("@long", obj.lang);
                cmd.Parameters.AddWithValue("@created_by", obj.created_by);
                cmd.Parameters.AddWithValue("@owned_by", obj.owned_by);


                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                vip.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (vip.State == ConnectionState.Open)
                {
                    vip.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Category_Update(clsSchedule obj)
        {
            try
            {
                // //exec diary_schedule.dbo.sp_update_diary_schedule 'id','category_id','subject','date','start_time','end_time','location','lat','long'

                vip.Open();
                cmd = new SqlCommand("sp_update_diary_schedule", vip);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@id", obj.owned_by);
                cmd.Parameters.AddWithValue("@category_id", obj.category_id);
                cmd.Parameters.AddWithValue("@subject", obj.subject);
                cmd.Parameters.AddWithValue("@date", getdate(obj.date));
                cmd.Parameters.AddWithValue("@start_time", obj.start_time);
                cmd.Parameters.AddWithValue("@end_time", obj.end_time);
                cmd.Parameters.AddWithValue("@location", obj.location);
                cmd.Parameters.AddWithValue("@lat", obj.lat);
                cmd.Parameters.AddWithValue("@long", obj.lang);



                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                vip.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (vip.State == ConnectionState.Open)
                {
                    vip.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Category_List(string from, string to, string catid)
        {
            try
            {

                vip.Open();
                cmd = new SqlCommand(" EXEC sp_get_diary_schedule_details '" + catid + "','','" + getdate(from) + "','" + getdate(to) + "','','','','0'", vip);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (vip.State == ConnectionState.Open)
                {
                    vip.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        public DataTable Category_List_report(string from, string to, string catid)
        {
            try
            {

                vip.Open();
                cmd = new SqlCommand(" EXEC sp_get_diary_schedule_details_for_report '" + catid + "','','" + getdate(from) + "','" + getdate(to) + "','','','','0'", vip);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (vip.State == ConnectionState.Open)
                {
                    vip.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }


        #endregion

        #region Grievance
        public DataTable Grievance_add(clsGrievance obj, string ipaddress)
        {

            try
            {

                con.Open();
                cmd = new SqlCommand("sp_gre_inser_details", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@EMP_ID", obj.EmpID);
                cmd.Parameters.AddWithValue("@PERSON_NAME", obj.PERSON_NAME);
                cmd.Parameters.AddWithValue("@DESGNATION", obj.DESGNATION);
                cmd.Parameters.AddWithValue("@REQUSET_OWNED", obj.REQUSET_OWNED);
                cmd.Parameters.AddWithValue("@DATE_OF_REQUEST", getdate(obj.DATE_OF_REQUEST));
                cmd.Parameters.AddWithValue("@REQUEST_GIST", obj.REQUEST_GIST);
                cmd.Parameters.AddWithValue("@UPLOAD_PATH", obj.UPLOAD_PATH);
                cmd.Parameters.AddWithValue("@REQUEST_Ip", ipaddress);


                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        #endregion

        #region FH_Masters
        public DataTable Dept_get()
        {
            try
            {

                grievance.Open();
                cmd = new SqlCommand("EXEC sp_get_department_list", grievance);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (grievance.State == ConnectionState.Open)
                {
                    grievance.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }
        public DataTable UNIT_DETAILS()
        {
            try
            {

                grievance.Open();
                cmd = new SqlCommand("EXEC sp_get_unit_desc ", grievance);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (grievance.State == ConnectionState.Open)
                {
                    grievance.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }
        public DataTable File_code_details(string dept, string unit)
        {
            try
            {

                grievance.Open();
                cmd = new SqlCommand("EXEC sp_get_file_code_details '" + unit + "','" + dept + "' ", grievance);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (grievance.State == ConnectionState.Open)
                {
                    grievance.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }


        }
        public DataTable Employee_details(string dept)
        {
            try
            {

                grievance.Open();
                cmd = new SqlCommand("EXEC sp_get_emp_details '" + dept + "' ", grievance);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (grievance.State == ConnectionState.Open)
                {
                    grievance.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }


        }
        public DataTable map_file_code(string empno, string filecode, string peshi)
        {
            try
            {

                grievance.Open();
                cmd = new SqlCommand("EXEC sp_map_employee_with_file_code  '" + empno + "','" + filecode + "','','" + peshi + "' ", grievance);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (grievance.State == ConnectionState.Open)
                {
                    grievance.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }


        }
        public DataTable edit_file_code(string empno, string filecode, string mappedno, string peshi)
        {
            try
            {

                grievance.Open();
                cmd = new SqlCommand("EXEC sp_edit_map_employee_with_file_code   '" + empno + "','" + filecode + "','','" + mappedno + "','" + peshi + "' ", grievance);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (grievance.State == ConnectionState.Open)
                {
                    grievance.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }


        }

        #endregion

        #region Sec Attendance 
        public DataTable Sec_att_dashboard()
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("EXEC sp_ATT_DASH_BOARD", con);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }
        #endregion

        #region E-Office 
        public DataTable Last_sync_date()
        {
            try
            {

                eoffice.Open();
                cmd = new SqlCommand("eorepdb.dbo.Eoffice_last_data_sync_date_get", eoffice);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (eoffice.State == ConnectionState.Open)
                {
                    eoffice.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }

        public DataTable Secretariat_departments_list()
        {
            try
            {

                eoffice.Open();
                cmd = new SqlCommand("eorepdb.dbo.Eoffice_get_sectt_department_list", eoffice);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (eoffice.State == ConnectionState.Open)
                {
                    eoffice.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }

        public DataTable file_disposal_report(string fromdate, string todate, string deptcode)
        {
            try
            {

                eoffice.Open();
                cmd = new SqlCommand("eorepdb.dbo.Eoffice_file_disposal_in_ex_global_org_dashboard_for_mob", eoffice);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@from_date", getdate(fromdate));
                cmd.Parameters.AddWithValue("@to_date", getdate(todate));
                cmd.Parameters.AddWithValue("@dept_code", deptcode);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (eoffice.State == ConnectionState.Open)
                {
                    eoffice.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }
        #endregion

        #region AIS

        public DataTable Service_types()
        {
            try
            {

                ais.Open();
                cmd = new SqlCommand("sp_for_ais_service_master", ais);
                cmd.CommandType = CommandType.StoredProcedure;
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (ais.State == ConnectionState.Open)
                {
                    ais.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        public DataTable Get_Officers(string type, string searchkey)
        {
            try
            {

                ais.Open();
                cmd = new SqlCommand("SP_FOR_GET_OFFICERS_GAD_APP", ais);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Service", Convert.ToInt16(type));
                cmd.Parameters.AddWithValue("@searchkey", searchkey);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (ais.State == ConnectionState.Open)
                {
                    ais.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Get_Officers_Posting(string type)
        {
            try
            {

                ais.Open();
                cmd = new SqlCommand("Sp_for_AIS_vacancy_Report", ais);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@service_Id", Convert.ToInt16(type));
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                if (ais.State == ConnectionState.Open)
                {
                    ais.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        #endregion

        #region GuestHouse
        public DataTable GuestHouse_Master(string type)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", type);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }

        }
        public DataTable GuestHouse_User_Request(clsGuestHouse_Request obj)
        {
            try
            {
                string orderid = System.DateTime.Now.ToString("ddMMyyyyHHmmss");
                if (obj.OrderID == null || obj.OrderID == "")
                {
                    con.Open();
                    cmd = new SqlCommand("sp_GHS_INS_ROOM_REQUEST", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@GHS_ID", obj.ghs_id);
                    cmd.Parameters.AddWithValue("@ROOM_TYPE", obj.room_type);
                    cmd.Parameters.AddWithValue("@GUEST_DETAILS", obj.guest_name);
                    cmd.Parameters.AddWithValue("@FROM_DATE", getdate(obj.from_date));
                    cmd.Parameters.AddWithValue("@TO_DATE", getdate(obj.to_date));
                    cmd.Parameters.AddWithValue("@NOTE", obj.remarks);
                    cmd.Parameters.AddWithValue("@CATEGORY", obj.category);
                    cmd.Parameters.AddWithValue("@ORDER_ID", orderid);
                    cmd.Parameters.AddWithValue("@ROOM_NO", obj.room_number);

                }
                else
                {
                    con.Open();
                    cmd = new SqlCommand("sp_GHS_UPD_ROOM_REQUEST", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@GHS_ID", obj.ghs_id);
                    cmd.Parameters.AddWithValue("@ROOM_TYPE", obj.room_type);
                    cmd.Parameters.AddWithValue("@GUEST_DETAILS", obj.guest_name);
                    cmd.Parameters.AddWithValue("@FROM_DATE", getdate(obj.from_date));
                    cmd.Parameters.AddWithValue("@TO_DATE", getdate(obj.to_date));
                    cmd.Parameters.AddWithValue("@NOTE", obj.remarks);
                    cmd.Parameters.AddWithValue("@CATEGORY", obj.category);
                    cmd.Parameters.AddWithValue("@ORDER_ID", obj.OrderID);
                    cmd.Parameters.AddWithValue("@ROOM_NO", obj.room_number);
                }
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    if (obj.OrderID == null || obj.OrderID == "")
                    {
                        updateRoom_request(obj, orderid);
                    }
                    else
                    {
                        updateRoom_request(obj, obj.OrderID);
                    }
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }

        public DataTable updateRoom_request(clsGuestHouse_Request obj, string orderid)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_UPD_ROOM_STATUS", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@GHS_ID", obj.ghs_id);
                cmd.Parameters.AddWithValue("@ROOM_NO", obj.room_number);
                cmd.Parameters.AddWithValue("@ROOM_STATUS", obj.RoomStatus);
                cmd.Parameters.AddWithValue("@ORDER_ID", orderid);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }

        public DataTable GuestHouse_Details(string type, string id = null)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_GHS_ID_ROOM_REQUEST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@GHS_ID", id);
                cmd.Parameters.AddWithValue("@TYPE", type);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }

        public DataTable GuestHouse_Request_Cancel(string id, string reason)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_REJECT_ROOM_REQUEST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@GHS_ID", id);
                cmd.Parameters.AddWithValue("@REJECT_RESON", reason);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }

        public DataTable GuestHouse_Admin_approve(clsGuestHouse_Admin_Request obj)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_UPD_ADMIN_ROOM_REQUEST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORDER_ID", obj.ID);
                cmd.Parameters.AddWithValue("@GHS_ID", obj.GHS_ID);
                cmd.Parameters.AddWithValue("@ROOM_TYPE", obj.ROOM_Type);
                cmd.Parameters.AddWithValue("@GUEST_NAME", obj.guest_name);
                cmd.Parameters.AddWithValue("@DESIGNATION", obj.designation);
                cmd.Parameters.AddWithValue("@MOBILE_NO", obj.mobile_no);
                cmd.Parameters.AddWithValue("@FROM_DATE", getdate(obj.from_date));
                cmd.Parameters.AddWithValue("@TO_DATE", getdate(obj.to_date));
                cmd.Parameters.AddWithValue("@PURPOSE_OF_VISIT", obj.purpose_of_visit);
                cmd.Parameters.AddWithValue("@VECHILE_ALLOTMENT", obj.vechile_allotment);
                cmd.Parameters.AddWithValue("@REPORTING_TIME", obj.reporting_time);
                cmd.Parameters.AddWithValue("@NOTE", obj.note);



                cmd.Parameters.AddWithValue("@ROOM_NO", obj.Room_no);
                cmd.Parameters.AddWithValue("@ROOM_STATUS", obj.Room_status);
                cmd.Parameters.AddWithValue("@VECHILE_NO", obj.V_no);
                cmd.Parameters.AddWithValue("@DRIVER_ID", obj.driver_id);

                cmd.Parameters.AddWithValue("@ORDER_OF_PRIORITY", obj.order);
                cmd.Parameters.AddWithValue("@ID_PROOF_TYPE", obj.id_proof_type);
                cmd.Parameters.AddWithValue("@ID_PROOF_PATH", obj.id_proof);
                cmd.Parameters.AddWithValue("@ADMIN_NOTE", obj.adminnote);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }

        public DataTable GuestHouse_Rooms_GET(string gid)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_ROOM_NO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@GHS_ID", gid);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }
        #endregion

        #region Catering 
        public DataTable Catering_Add(clsCatering obj)
        {

            string orderid = System.DateTime.Now.ToString("ddMMyyyyHHmmss");
            try
            {


                con.Open();
                cmd = new SqlCommand("sp_PCT_INS_ORDER_DETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ORDER_ID", orderid);
                cmd.Parameters.AddWithValue("@PURPOSE_OF_MEETING", obj.Meeting);
                cmd.Parameters.AddWithValue("@VENUE", obj.Venue);
                cmd.Parameters.AddWithValue("@MEETING_DATE", obj.MeetingDate);
                cmd.Parameters.AddWithValue("@MEETING_TIME", obj.MeetingTime);
                cmd.Parameters.AddWithValue("@CONTACT_PERSON", obj.PersonName);
                cmd.Parameters.AddWithValue("@CONTACT_NUMBER", obj.MobileNumber);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    foreach (clsitems o in obj.ObjItems)
                    {
                        Catering_Add_items(o, orderid);
                    }
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }


        public DataTable Catering_Add_items(clsitems obj, string oid)
        {
            try
            {


                con.Open();
                cmd = new SqlCommand("sp_PCT_INS_ORDER_ITEM", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ORDER_ID", oid);
                cmd.Parameters.AddWithValue("@ITEM_NAME", obj.Name);
                cmd.Parameters.AddWithValue("@QUANTITY", obj.Quantity);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        public List<clsCatering> Catering_Details_Get()
        {

            List<clsCatering> obj = new List<clsCatering>();
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_PCT_GET_ORDER_DETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.AddWithValue("@TYPE", 1);
                cmd.Parameters.AddWithValue("@DATE", "");
                cmd.Parameters.AddWithValue("@STATUS", "");
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        obj.Add(new clsCatering()
                        {
                            Meeting = dr["PURPOSE_OF_MEETING"].ToString(),
                            MeetingDate = dr["MEETING_DATE"].ToString(),
                            MeetingTime = dr["MEETING_TIME"].ToString(),
                            MobileNumber = dr["CONTACT_NUMBER"].ToString(),
                            PersonName = dr["CONTACT_PERSON"].ToString(),
                            Venue = dr["VENUE"].ToString(),
                            OrderID = dr["ORDER_ID"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                obj = null;
            }
            return obj;
        }

        public List<clsitems> Catering_Details_Get_By_Id(string id)
        {

            List<clsitems> obj = new List<clsitems>();
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_PCT_GET_ORDER_DETAILS_BY_ID", con);
                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.AddWithValue("@ORDER_ID", id);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        obj.Add(new clsitems()
                        {
                            Name = dr["ITEM_NAME"].ToString(),
                            Quantity = dr["QUANTITY"].ToString()
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                obj = null;
            }
            return obj;
        }
        #endregion

        #region Room Master 
        public DataTable Room_Master(clsRoomMaster obj)
        {
            try
            {


                con.Open();
                cmd = new SqlCommand("sp_GHS_INS_ROOM_MASTER", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ROOM_NO", obj.RoomNo);
                cmd.Parameters.AddWithValue("@GHS_ID", obj.GHID);
                cmd.Parameters.AddWithValue("@ROOM_TYPE", obj.RoomType);
                //cmd.Parameters.AddWithValue("@ROOM_STATUS", obj.Status);
                cmd.Parameters.AddWithValue("@ROOM_FLOOR", obj.RoomFloor);
                cmd.Parameters.AddWithValue("@ROOM_AMENITIES", obj.AMENITIES);
                //cmd.Parameters.AddWithValue("@ROOM_CATEGORY", obj.Category);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Room_Master_Get()
        {
            try
            {


                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_ROOM_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@DATE", date=="0"?date:getdate(date));
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Room_Master_Get_count()
        {
            try
            {


                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_ROOM_MASTER_count", con);
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Parameters.AddWithValue("@DATE", date == "0" ? date : getdate(date));


                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }

        #endregion

        #region ESR_SEC
        public DataTable SEC_ESR_Officers_Master(string role, string type)
        {
            try
            {


                ESR_SEC.Open();
                cmd = new SqlCommand("sp_officermaster", ESR_SEC);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@role", Convert.ToInt16(role));
                cmd.Parameters.AddWithValue("@selecttype", type);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                ESR_SEC.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ESR_SEC.State == ConnectionState.Open)
                {
                    ESR_SEC.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }


        public DataTable SEC_ESR_employee_Details(string emp_id)
        {
            try
            {
                ESR_SEC.Open();
                cmd = new SqlCommand("sp_for_employee_Details_GAD_APP", ESR_SEC);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@emp_id", emp_id);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                ESR_SEC.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ESR_SEC.State == ConnectionState.Open)
                {
                    ESR_SEC.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        #endregion

        #region
        public DataTable State_GuestHouse_GET(string year, string month, string date)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_STG_GET_GUEST_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@YEAR", year);
                cmd.Parameters.AddWithValue("@MONTH", month);
                cmd.Parameters.AddWithValue("@DATE", date);

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                DataTable dt = new DataTable();
                dt.Columns.Add("error");
                DataRow dr = dt.NewRow();
                dr["error"] = ex.Message;
                dt.Rows.Add(dr);
                return dt;
            }
        }
        #endregion

        #region WEB StateGuests
        public DataTable GetStateGuestsData(clsGetStateGuest obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_STG_GET_GUEST_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@YEAR", obj.year);
                cmd.Parameters.AddWithValue("@MONTH", obj.month);
                cmd.Parameters.AddWithValue("@DATE", obj.date);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null && dt.Rows[0][0].ToString() != "0")
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }
        public bool InsertStateGuestData(clsStateGuestlist obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("tbl_STG_INS_GUEST_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DATE_OF_ISSUE", getdateformat(obj.DateOfOrder));
                cmd.Parameters.AddWithValue("@GUESTS_DETAILS", obj.Guests);
                cmd.Parameters.AddWithValue("@PLACES_TO_VISIT", obj.VisitPlaces);
                cmd.Parameters.AddWithValue("@FROM_DATE", getdateformat(obj.Fromdate));
                cmd.Parameters.AddWithValue("@TO_DATE", getdateformat(obj.ToDate));
                cmd.Parameters.AddWithValue("@NOTE", obj.Remark);

                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }
        public bool UpdateStateGuestData(clsStateGuestlist obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_STG_UPD_GUEST_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ID", obj.id);
                cmd.Parameters.AddWithValue("@DATE_OF_ISSUE", getdateformat(obj.DateOfOrder));
                cmd.Parameters.AddWithValue("@GUESTS_DETAILS", obj.Guests);
                cmd.Parameters.AddWithValue("@PLACES_TO_VISIT", obj.VisitPlaces);
                cmd.Parameters.AddWithValue("@FROM_DATE", getdateformat(obj.Fromdate));
                cmd.Parameters.AddWithValue("@TO_DATE", getdateformat(obj.ToDate));
                cmd.Parameters.AddWithValue("@NOTE", obj.Remark);
                cmd.Parameters.AddWithValue("@STATUS", "1");
                cmd.Parameters.AddWithValue("@STATUS_NOTE", "");

                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }
        private dynamic getdateformat(string strdate)
        {
            try
            {
                if (strdate != null && strdate != "")
                {
                    string dateformat = strdate;
                    dateformat = dateformat.Replace("/", "-");
                    DateTime dt1 = DateTime.ParseExact(dateformat, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    return dt1;
                }
                else
                {
                    return strdate;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ":Invalid Date" + strdate);
            }
        }

        private dynamic getdatewiseformat(string strdate)
        {
            try
            {
                if (strdate != null && strdate != "")
                {
                    string dateformat = strdate;
                    if (dateformat.Contains("/"))
                    {
                        dateformat = dateformat.Replace("/", "-");
                    }
                    else if (dateformat.Contains("-"))
                    {
                        dateformat = dateformat.Replace("-", "-");
                        //var date = dateformat.Split('-');
                        //dateformat = date[2] + "-" + date[1] + "-" + date[0];
                    }
                    DateTime dt1 = DateTime.ParseExact(dateformat, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    return dt1;
                }
                else
                {
                    return strdate;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ":Invalid Date" + strdate);
            }
        }

        private string ConvertDate(string strdate)
        {
            try
            {
                if (strdate != "" && strdate != "0")
                {
                    string DOBDATE = null;
                    var date = strdate.Split('/');
                    DOBDATE = date[2] + "-" + date[1] + "-" + date[0];
                    return DOBDATE;
                }
                else
                {
                    return strdate;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ":Invalid Date" + strdate);
            }

        }
        public string Replacedateformat(string strdate)
        {
            try
            {
                string DOBDATE = null;
                if (strdate != "")
                {
                    string dateformat = strdate.Split(' ')[0].ToString();
                    var date = dateformat.Split('-');
                    DOBDATE = date[0] + "/" + date[1] + "/" + date[2];
                }
                //if (date[0] != "10" && date[0] != "11" && date[0] != "12")
                //{
                //    DOBDATE = date[1] + "/" + "0" + date[0] + "/" + date[2];
                //    if (date[1]=="1"|| date[1] == "2"|| date[1] == "3"|| date[1] == "4"|| date[1] == "5"|| date[1] == "6"|| date[1] == "7"|| date[1] == "8"|| date[1] == "9") {
                //        DOBDATE = "0"+date[1] + "/" + "0" + date[0] + "/" + date[2];
                //    }
                //}

                //else {
                //    DOBDATE = date[1] + "/" + date[0] + "/" + date[2];
                //    if (date[1] == "1" || date[1] == "2" || date[1] == "3" || date[1] == "4" || date[1] == "5" || date[1] == "6" || date[1] == "7" || date[1] == "8" || date[1] == "9")
                //    {
                //        DOBDATE = "0" + date[1] + "/" + date[0] + "/" + date[2];
                //    }
                //}

                //DateTime dt1 = DateTime.ParseExact(DOBDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return DOBDATE;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ":Invalid Date" + strdate);
            }
        }

        public string ReplaceDDMMYYYYdateformat(string strdate)
        {
            try
            {
                string DOBDATE = null;
                if (strdate != "")
                {
                    string dateformat = strdate.Split(' ')[0].ToString();
                    var date = dateformat.Split('-');
                    DOBDATE = date[2] + "-" + date[1] + "-" + date[0];
                }
                //if (date[0] != "10" && date[0] != "11" && date[0] != "12")
                //{
                //    DOBDATE = date[1] + "/" + "0" + date[0] + "/" + date[2];
                //    if (date[1]=="1"|| date[1] == "2"|| date[1] == "3"|| date[1] == "4"|| date[1] == "5"|| date[1] == "6"|| date[1] == "7"|| date[1] == "8"|| date[1] == "9") {
                //        DOBDATE = "0"+date[1] + "/" + "0" + date[0] + "/" + date[2];
                //    }
                //}

                //else {
                //    DOBDATE = date[1] + "/" + date[0] + "/" + date[2];
                //    if (date[1] == "1" || date[1] == "2" || date[1] == "3" || date[1] == "4" || date[1] == "5" || date[1] == "6" || date[1] == "7" || date[1] == "8" || date[1] == "9")
                //    {
                //        DOBDATE = "0" + date[1] + "/" + date[0] + "/" + date[2];
                //    }
                //}

                //DateTime dt1 = DateTime.ParseExact(DOBDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return DOBDATE;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ":Invalid Date" + strdate);
            }
        }
        #endregion


        #region WebVehicle Transport Status

        public DataTable GetVehicleStatusData()
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_PVH_GET_VEHICLES_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetTransportVehicleCounts(string type)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_PCT_GET_DASH_BOARD_COUNT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TYPE", type);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public bool UpdateVehicleStatusData(clsVehicleStatuslist obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_PVH_UPD_VEHICLES_ALLOTTED", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@REGISTRATION_NO", obj.vehicleno);
                cmd.Parameters.AddWithValue("@ALLOTTED_NAME", obj.allotedname);
                cmd.Parameters.AddWithValue("@DRIVER", obj.driver);
                cmd.Parameters.AddWithValue("@REMARKS", obj.remark);
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }
        #endregion

        #region Web Guesthouse Daily Status Report
        public DataTable GetGuesthouseDRPData(string type)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", type);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }


        public DataTable GuestHouse_Room_Master(string type, string ghs_id)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", type);
                cmd.Parameters.AddWithValue("@GHS_ID", ghs_id);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }

        }

        public DataTable GetGuesthouse_Data(string ghs_id, string date)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_ROOM_STATUS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@GHS_ID", ghs_id);
                cmd.Parameters.AddWithValue("@DATE", ConvertDate(date));
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetGuesthouse_CountsData(string ghs_id, string date)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_GHS_GET_ROOM_STATUS_COUNT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@GHS_ID", ghs_id);
                cmd.Parameters.AddWithValue("@DATE", ConvertDate(date));
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable ADD_GuesthouseStatusData(clsGuesthouseList obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_GHS_INS_ROOM_GUEST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@GHS_ID", obj.ghsid);
                cmd.Parameters.AddWithValue("@ROOM_NO", obj.roomno);
                cmd.Parameters.AddWithValue("@NAME_OF_THE_GUEST", obj.name_Guest);
                cmd.Parameters.AddWithValue("@DESIGNATION_OF_GUEST", obj.designation_Guest);
                cmd.Parameters.AddWithValue("@DATE_OF_ARRIAVAL", getdateformat(obj.dateofarrival));
                cmd.Parameters.AddWithValue("@DATE_OF_DEPARTURE", getdateformat(obj.dateofdeparture));
                cmd.Parameters.AddWithValue("@CATEGORY", obj.category);
                cmd.Parameters.AddWithValue("@REMARKS", obj.reamrks);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }


        public bool Update_GuesthouseStatusData(clsGuesthouseList obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_GHS_UPD_ROOM_GUEST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", obj.id);
                cmd.Parameters.AddWithValue("@GHS_ID", obj.ghsid);
                cmd.Parameters.AddWithValue("@ROOM_NO", obj.roomno);
                cmd.Parameters.AddWithValue("@NAME_OF_THE_GUEST", obj.name_Guest);
                cmd.Parameters.AddWithValue("@DESIGNATION_OF_GUEST", obj.designation_Guest);
                cmd.Parameters.AddWithValue("@DATE_OF_ARRIAVAL", getdatewiseformat(obj.dateofarrival));
                cmd.Parameters.AddWithValue("@DATE_OF_DEPARTURE", getdatewiseformat(obj.dateofdeparture));
                cmd.Parameters.AddWithValue("@CATEGORY", obj.category);
                cmd.Parameters.AddWithValue("@REMARKS", obj.reamrks);
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }


        public bool GuesthouseCheckoutData(clsGuesthousecheckoutList obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_GHS_UPD_ROOM_VACATE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", obj.id);
                cmd.Parameters.AddWithValue("@CHECKOUT_STATUS", "1");
                cmd.Parameters.AddWithValue("@CHECKOUT_DATE", getdatewiseformat(obj.checkout_date));
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }

        #endregion

        #region Vehicles
        public DataTable Vehicles_Get()
        {
            try
            {


                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_GADVEHICLELIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                writelogs(ex.Message);
                return null;
            }
        }
        public DataTable Add_New_Vehicledetails(clsVehicleDetails obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_VHL_INS_FULE", con);

                cmd.Parameters.AddWithValue("@REGISTRATION_NO", obj.REGISTRATION_NO);
                cmd.Parameters.AddWithValue("@NAME_OF_PERTOL_BUNK", obj.NAME_OF_PERTOL_BUNK);
                cmd.Parameters.AddWithValue("@DATE", getdate(obj.VHL_DATE));
                cmd.Parameters.AddWithValue("@COUPON_NO", obj.COUPON_NO);
                cmd.Parameters.AddWithValue("@QUANTITY", obj.QUANTITY);
                cmd.Parameters.AddWithValue("@PRICE", obj.PRICE);
                cmd.Parameters.AddWithValue("@POL_TYPE", obj.POL_TYPE);
                cmd.Parameters.AddWithValue("@TOTAL_COST", obj.TOTAL_COST);

                cmd.Parameters.AddWithValue("@REQUESTED_ID", HttpContext.Current.Request.UserHostAddress);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }

        public DataTable update_New_Vehicledetails(clsVehicleDetailsUpdate obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_VHL_UPD_VECHICALES", con);

                cmd.Parameters.AddWithValue("@registred_no", obj.VEHICLE_NO);
                cmd.Parameters.AddWithValue("@dept_code", obj.DEPT_NAME);
                cmd.Parameters.AddWithValue("@desgnation_user", obj.DESIGNATION);
                cmd.Parameters.AddWithValue("@celing_limt", obj.CEILING_LIMIT_FIXED);
                cmd.Parameters.AddWithValue("@driver_name", obj.NAME);
                cmd.Parameters.AddWithValue("@driver_mobile", obj.MOBILE);
                cmd.Parameters.AddWithValue("@remarks", obj.REMARKS);
                cmd.Parameters.AddWithValue("@requested_ip", HttpContext.Current.Request.UserHostAddress);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }

        public string Year_Get(string type, string year)
        {
            return "exec [dbo].[sp_VHL_GET_DROPDOWN] '" + type + "','" + year + "'";
            //try
            //{

            //    con.Open();
            //    cmd = new SqlCommand("sp_GET_DROP_DOWN", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.AddWithValue("@year", year);
            //    cmd.Parameters.AddWithValue("@DDO_TYPE", type);

            //    dap = new SqlDataAdapter(cmd);
            //    dt = new DataTable();
            //    dap.Fill(dt);
            //    con.Close();
            //    cmd.Dispose();
            //    dap.Dispose();
            //    if (dt.Rows.Count > 0)
            //    {
            //        return dt;
            //    }
            //    else
            //    {
            //        return null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (con.State == ConnectionState.Open)
            //    {
            //        con.Close();
            //    }
            //    if (cmd != null)
            //    {
            //        cmd.Dispose();
            //    }
            //    // writelogs(ex.Message);
            //    return null;
            //}
        }
        public DataTable month_Get(clsYearMonth obj)
        {

            try
            {

                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", obj.type);
                cmd.Parameters.AddWithValue("@year", obj.year);


                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        public DataSet BillReport(string year, string month)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("SP_VHL_GET_VECHICAL_BILL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@YEAR_SEL", year);
                cmd.Parameters.AddWithValue("@MONTH_SEL", month);
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }
        public DataSet BillReport_Individual(string year, string month, string vhl_no)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("SP_VHL_GET_VECHICAL_BILL_INDIVIDUAL_WISE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@year_sel", year);
                cmd.Parameters.AddWithValue("@month_sel", month);
                cmd.Parameters.AddWithValue("@registred_no", vhl_no);
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }
        public DataSet POL_Indent_Report(string year, string month)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_POL_USAGE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@YEAR", year);
                cmd.Parameters.AddWithValue("@MONTH", month);
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }

        public DataTable Add_New_Expenditure_Details(clsExpenditureDetails obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_VHL_INS_EXPENDITURE", con);

                cmd.Parameters.AddWithValue("@REGISTRATION_NO", obj.REGISTRATION_NO);
                cmd.Parameters.AddWithValue("@CAPTURE_DATE", getdate(obj.CAPTURE_DATE));
                cmd.Parameters.AddWithValue("@REPLACED_SERVICING", obj.REPLACED_SERVICING);
                cmd.Parameters.AddWithValue("@NAME_OF_THE_GARAGE", obj.NAME_OF_THE_GARAGE);
                cmd.Parameters.AddWithValue("@AMOUNT", obj.AMOUNT);

                //cmd.Parameters.AddWithValue("@REQUESTED_ID", HttpContext.Current.Request.UserHostAddress);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }


        public DataTable update_billdetails(clsbillDetailsUpdate obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("SP_VHL_UPD_FULE", con);

                cmd.Parameters.AddWithValue("@ID", obj.ID);
                cmd.Parameters.AddWithValue("@REGISTRATION_NO", obj.REGISTRATION_NO);
                cmd.Parameters.AddWithValue("@NAME_OF_PERTOL_BUNK", obj.NAME_OF_PERTOL_BUNK);
                cmd.Parameters.AddWithValue("@DATE", getdate(obj.DATE));
                cmd.Parameters.AddWithValue("@COUPON_NO", obj.COUPON_NO);
                cmd.Parameters.AddWithValue("@QUANTITY", obj.QUANTITY);
                cmd.Parameters.AddWithValue("@PRICE", obj.PRICE);
                cmd.Parameters.AddWithValue("@POL_TYPE", obj.POL_TYPE);
                cmd.Parameters.AddWithValue("@TOTAL_COST", obj.TOTAL_COST);
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }


        public DataSet ExpenditureReport(string year, string month)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_EXPENDITURE_YEAR_MONTH", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@YEAR_SEL", year);
                cmd.Parameters.AddWithValue("@MONTH_SEL", month);
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }
        public DataSet ExpenditureReport_Individual(string year, string month, string vhl_no)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_EXPENDITURE_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@year_sel", year);
                cmd.Parameters.AddWithValue("@month_sel", month);
                cmd.Parameters.AddWithValue("@REGISTRATION_NO", vhl_no);
                dap = new SqlDataAdapter(cmd);
                DataSet dt = new DataSet();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Tables[0].Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                //writelogs(ex.Message);
                return null;
            }
        }
        public DataTable update_expendituredetails(clsexpenditureDetailsUpdate obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_VHL_UPD_EXPENDITURE ", con);
                cmd.Parameters.AddWithValue("@ID", obj.ID);
                cmd.Parameters.AddWithValue("@REGISTRATION_NO", obj.REGISTRATION_NO);
                cmd.Parameters.AddWithValue("@CAPTURE_DATE", getdate(obj.CAPTURE_DATE));
                cmd.Parameters.AddWithValue("@REPLACED_SERVICING", obj.REPLACED_SERVICING);
                cmd.Parameters.AddWithValue("@NAME_OF_THE_GARAGE", obj.NAME_OF_THE_GARAGE);
                cmd.Parameters.AddWithValue("@AMOUNT", obj.AMOUNT);

                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }
        }
        #endregion

        #region Web Inventory
        public DataTable GetInventory_Data()
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_INV_GET_ITEMS_MASTER", con);
                cmd.CommandType = CommandType.StoredProcedure;
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetMobInventory_Data(string Categoryid, string Itemid)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_INV_GET_ITEMS_MASTER", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CATEGORY_ID", Categoryid);
                cmd.Parameters.AddWithValue("@ITEM_ID", Itemid);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetItemCategoryDrpdwnlist()
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", "9");
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetItemSDrpdwnlist(string Categoryid)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", "11");
                cmd.Parameters.AddWithValue("@CATEGORY_ID", Categoryid);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }


        public DataTable GetVendorlist()
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_VHL_GET_DROPDOWN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DD_TYPE", "10");
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }


        public bool InsertInventoryData(clsinventoryList obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_INV_INS_ITM_MASTER", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CATEGORY_ID", obj.categoryid);
                cmd.Parameters.AddWithValue("@CATEGORY", obj.categoryname);
                cmd.Parameters.AddWithValue("@ITEM_NAME", obj.itemname);
                cmd.Parameters.AddWithValue("@V01_NAME", obj.v01name);
                cmd.Parameters.AddWithValue("@V01_MAKER", obj.v01maker);
                cmd.Parameters.AddWithValue("@V01_PRICE", obj.v01price);
                cmd.Parameters.AddWithValue("@V01_STATUS", obj.v01status);
                cmd.Parameters.AddWithValue("@V02_NAME", obj.v02name);
                cmd.Parameters.AddWithValue("@V02_MAKER", obj.v02maker);
                cmd.Parameters.AddWithValue("@V02_PRICE", obj.v02price);
                cmd.Parameters.AddWithValue("@V02_STATUS", obj.v02status);

                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }

        public bool UpdateInventoryData(clsinventoryList obj)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_INV_UPD_ITEMS_MASTER", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CATEGORY_ID", obj.categoryid);
                cmd.Parameters.AddWithValue("@CATEGORY", obj.categoryname);
                cmd.Parameters.AddWithValue("@ITEM_ID", obj.itemid);
                cmd.Parameters.AddWithValue("@ITEM_NAME", obj.itemname);
                cmd.Parameters.AddWithValue("@V01_NAME", obj.v01name);
                cmd.Parameters.AddWithValue("@V01_MAKER", obj.v01maker);
                cmd.Parameters.AddWithValue("@V01_PRICE", obj.v01price);
                cmd.Parameters.AddWithValue("@V01_STATUS", obj.v01status);
                cmd.Parameters.AddWithValue("@V02_NAME", obj.v02name);
                cmd.Parameters.AddWithValue("@V02_MAKER", obj.v02maker);
                cmd.Parameters.AddWithValue("@V02_PRICE", obj.v02price);
                cmd.Parameters.AddWithValue("@V02_STATUS", obj.v02status);

                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }
        #endregion


        #region  FTR Report
        public DataTable GetFTRReport_Data(string year, string month)
        {
            try
            {
                FTR_CON.Open();
                cmd = new SqlCommand("sp_for_form16_report", FTR_CON);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@year", year);
                cmd.Parameters.AddWithValue("@month", month);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                FTR_CON.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (FTR_CON.State == ConnectionState.Open)
                {
                    FTR_CON.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetFTRformappliedReport_Data(string year, string month)
        {
            try
            {
                FTR_CON.Open();
                cmd = new SqlCommand("sp_formf_applied_report", FTR_CON);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@year", year);
                cmd.Parameters.AddWithValue("@month", month);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                FTR_CON.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (FTR_CON.State == ConnectionState.Open)
                {
                    FTR_CON.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }


        public DataTable GetFTRformapprovedReport_Data(string year, string month)
        {
            try
            {
                FTR_CON.Open();
                cmd = new SqlCommand("sp_formf_approved_report", FTR_CON);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@year", year);
                cmd.Parameters.AddWithValue("@month", month);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                FTR_CON.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (FTR_CON.State == ConnectionState.Open)
                {
                    FTR_CON.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        public DataTable GetFTRgroupmembersReport_Data(string formnum)
        {
            try
            {
                FTR_CON.Open();
                cmd = new SqlCommand("sp_for_get_group_members", FTR_CON);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@formno", formnum);
                // cmd.Parameters.AddWithValue("@month", month);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                FTR_CON.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (FTR_CON.State == ConnectionState.Open)
                {
                    FTR_CON.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }
        #endregion

        #region Login Menu Details
        public DataTable GetLoginMenu_Data(string username)
        {
            try
            {
                con.Open();
                cmd = new SqlCommand("sp_USR_GET_SCREEN_PERMISSION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@USERNAME", username);
                // cmd.Parameters.AddWithValue("@month", month);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        #endregion
        #region UserLogin Details
        public DataTable GetUserLogin_Data(string username, string password)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_USR_GET_LOGIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@USERNAME", username);
                cmd.Parameters.AddWithValue("@PASSWORD", password);
                // cmd.Parameters.AddWithValue("@month", month);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0 && dt != null)
                {
                    return dt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return null;
            }
        }

        #endregion

        public string DecryptStringAES(string cipherText)
        {
            string recipherText = null;
            if (cipherText.Contains(" "))
            {
                recipherText = cipherText.Replace(" ", "+");
            }
            else
            {
                recipherText = cipherText;
            }
            var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
            var iv = Encoding.UTF8.GetBytes("8080808080808080");

            var encrypted = Convert.FromBase64String(recipherText);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return string.Format(decriptedFromJavascript);
        }


        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }
        public DataTable GetUserName_Master()
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_get_service_codes", con);
                cmd.CommandType = CommandType.StoredProcedure;
             
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }

        }

        public DataTable GetUserPages_Master()
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GET_SCREEN_LOGIN_MAPPED", con);
               
                cmd.CommandType = CommandType.StoredProcedure;

                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }

        }

        public bool Insert_userrolesData(Insertuserscreenmodel userscreenname)
        {
            try
            {
                con.Open();
                var listroot = userscreenname.userscreenname;

                foreach (Screennamelist obj in listroot)
                {
                 
                    cmd = new SqlCommand("sp_USR_INS_SCREEN_PERMISSION", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USERNAME", obj.userid);
                    cmd.Parameters.AddWithValue("@GROUP_ID", obj.groupid);
                    cmd.Parameters.AddWithValue("@PAGE_ID", obj.pageid);
                    cmd.ExecuteNonQuery();
                 
                }
                con.Close();
                cmd.Dispose();

                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }
        public bool Update_userrolesData(Insertuserscreenmodel userscreenname)
        {
            try
            {
                con.Open();
                var listroot = userscreenname.userscreenname;
          
                foreach (Screennamelist obj in listroot)
                {
                  
                    cmd = new SqlCommand("sp_USER_UPD_SCREEN_PERMISSION", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@USER_ID", obj.userid);
                    cmd.Parameters.AddWithValue("@GROUP_ID", obj.groupid);
                    cmd.Parameters.AddWithValue("@PAGE_ID", obj.pageid);
                    cmd.ExecuteNonQuery();
                  
                }
                con.Close();
                cmd.Dispose();

                return true;

            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                return false;
            }
        }

        public DataTable GetUserscreenmappedList_Master(string USER_ID)
        {
            try
            {

                con.Open();
                cmd = new SqlCommand("sp_GET_USER_SCREEN_MAPPED_LIST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@USER_ID",USER_ID);
                dap = new SqlDataAdapter(cmd);
                dt = new DataTable();
                dap.Fill(dt);
                con.Close();
                cmd.Dispose();
                dap.Dispose();
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                // writelogs(ex.Message);
                return null;
            }

        }


        #region logs_code
        public void writelogs(string message)
        {
            try
            {
                //if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(@"Errors")))
                //{
                //    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(@"Errors"));
                //}
                string path = @"D:\abc.txt";

                StreamWriter swLog = new StreamWriter(path, true);

                swLog.WriteLine(message);

                swLog.Close();
                swLog.Dispose();
            }
            catch (Exception ex)
            {

            }
        }
        public static void sendErroeLogs(string message, string stacktrace, string methodname)
        {
            try
            {

                string from = "meenakrishna.codetree@gmail.com";
                string frompassword = "Mkrish@021";
                string to_mail = "meenakrishna.codetree@gmail.com";
                using (MailMessage mm = new MailMessage(from, to_mail))
                {

                    //mm.CC.Add(new MailAddress(FromMail));

                    mm.Subject = "Exception from " + methodname;
                    mm.Body = message + "\n" + stacktrace;

                    mm.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(from, frompassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }


            }
            catch (Exception ex)
            {

            }
        }

        public object _Log(dynamic strMsg, string mappath)
        {
            try
            {
                string strPath = mappath + "\\" + DateTime.Now.ToString("MMddyyyy") + "\\" + DateTime.Now.ToString("HH").ToString();
                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);
                string path2 = strPath + "\\" + "submittedData" + DateTime.Now.ToString("yyyyMMddhhmmssmmm").ToString();
                StreamWriter swLog = new StreamWriter(path2 + ".txt", true);
                swLog.WriteLine(strMsg);
                swLog.Close();
                swLog.Dispose();
                return "Success";
            }
            catch
            {
                return "Fail";
            }
        }
        #endregion
    }
}