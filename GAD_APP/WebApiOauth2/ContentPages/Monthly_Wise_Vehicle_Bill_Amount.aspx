﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Monthly_Wise_Vehicle_Bill_Amount.aspx.cs" Inherits="WebApiOauth2.ContentPages.Monthly_Wise_Vehicle_Bill_Amount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Bill amounts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#expenditure_tab" role="tab" aria-controls="expenditure_tab" aria-selected="false">Expenditure Report</a>
                </li>


            </ul>
            <%--   <h5 class="page-heading mb-0 text-gray-800">  Bill amounts </h5>--%>
        </div>

        <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active " id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="form-row">

                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label for="">Year</label>
                                            <input class="form-control date" id="ddl_year" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label for="">Month</label>
                                            <input class="form-control date" id="ddl_month" />
                                            <%--<select id="ddl_month" class="form-control">
                                    </select>--%>
                                        </div>
                                    </div>
                                </div>

                                <br />
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm table-stripped" id="dataTable" width="100%" cellspacing="0">

                                        <thead>
                                            <tr>

                                                <th>Vehicle No </th>
                                                <th>Total Amount </th>
                                                <th>BIll amounts for the  MONTH</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="expenditure_tab" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow mb-4">
                            <div class="card-body">
                                <div class="form-row">

                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label for="">Year</label>
                                            <input class="form-control date" id="ddl_year1" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label for="">Month</label>
                                            <input class="form-control date" id="ddl_month1" />
                                            <%--<select id="ddl_month" class="form-control">
                                    </select>--%>
                                        </div>
                                    </div>
                                </div>

                                <br />
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm table-stripped" id="dataTable2" width="100%" cellspacing="0">

                                        <thead>
                                            <tr>

                                                <th>Vehicle No </th>
                                                <th>Total Amount </th>
                                                <th>View</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="billreport_modal" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Day Wise Report </h5>

                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h6>
                            <label>Vehicle No:<b><span id="lbl_vhlno"></span></b></label></h6>
                        <hr />
                        <div class="form-row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-sm table-stripped" id="dataTable1" width="100%" cellspacing="0">

                                    <thead>
                                        <tr>

                                            <th>Date</th>
                                            <th>POL Type</th>
                                            <th>Quantity</th>
                                            <th>Price(per litre)</th>
                                            <th>Total Cost(in rupees)</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot align="left">
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="expenditurereport_modal" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel1">Day Wise Report </h5>

                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h6>
                            <label>Vehicle No:<b><span id="lbl_vhlno1"></span></b></label></h6>
                        <hr />
                        <div class="form-row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-sm table-stripped" id="dataTable3" width="100%" cellspacing="0">

                                    <thead>
                                        <tr>

                                            <th>Date</th>
                                            <th>Items Replace/Servicing</th>
                                            <th>Name Of The Garage</th>
                                            <th>Amount</th>

                                             <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                     <tfoot align="left">
		<tr>
            <th></th>
            <th>Total</th>
            <th></th>
            <th></th>
            <th></th></tr>
	</tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="billupdate_modal" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabe2">Edit Report </h5>

                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="bill_modal_edit" data-parsley-validate="parsley">

                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="">Vehicle No </label>
                                        <input type="text" id="txt_vehicleno" class="form-control" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label>Name of Petrol bunk</label>
                                        <input type="text" id="txt_ptrlbunk_name" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Name Of Petrol Bunk">
                                    </div>
                                    <div class="form-group">
                                        <label>Date  </label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <input type="text" id="txt_date" class="form-control form-control-sm" />
                                                <div class="input-group-addon input-group-append">
                                                    <div class="input-group-text">
                                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <label>Coupon No</label>
                                        <input type="text" id="txt_couponno" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Coupon Number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>POL Type  </label>
                                        <select id="ddl_poltype" class="form-control form-control" data-parsley-required="true" data-parsley-error-message="Please Select POL Type">
                                            <option value="">--Choose--</option>
                                            <option value="1">PETROL</option>
                                            <option value="2">DIESEL</option>
                                            <option value="3">ENGINE OIL</option>
                                            <option value="4">COOLANT OIL</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label>Quantity </label>
                                        <input type="text" id="txt_quantity" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Qunatity" placeholder="0">
                                    </div>
                                    <div class="form-group">
                                        <label>Price(per litre in Rupees)  </label>
                                        <input type="text" id="txt_price" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Price Per ltr" placeholder="0">
                                    </div>
                                    <div class="form-group">
                                        <label>Total Cost(in rupees)   </label>
                                        <input type="text" id="txt_totalcost" class="form-control" readonly="readonly" placeholder="0">
                                    </div>


                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="button" id="btn_update" class="btn btn-success btn-sm">Update</button>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="expenditureupdate_modal" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabe3">Edit Report </h5>

                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form id="expenditure_modal_edit" data-parsley-validate="parsley">

                            <div class="form-row">
                             <div class="col-md-12">
                                <div class="form-group ">
                                    <label for="">Vehicle No </label>
                                    <input type="text" id="txt_vehicleno1" class="form-control" readonly="readonly">
                                </div>
                     <div class="form-group">
                                    <label>Date  </label>
                                    <div class="form-group">
                                        <div class="input-group date" id="">
                                            <input type="text" id="txt_date1" class="form-control form-control-sm" />
                                            <div class="input-group-addon input-group-append">
                                                <div class="input-group-text">
                                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Replaced/Servicing:</label>
                                    <input type="text" id="txt_replace_service1" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Name Of Petrol Bunk">
                                </div>
                               


                                <div class="form-group">

                                    <label>Name of the garage:</label>
                                    <input type="text" id="txt_garage_name1" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Coupon Number">
                                </div>
                    <div class="form-group">

                                    <label>Amount:</label>
                                    <input type="text" id="txt_amount1" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Coupon Number">
                                </div>
                            </div>
                
                            </div>
                            <div class="modal-footer">

                                <button type="button" id="btn_update1" class="btn btn-success btn-sm">Update</button>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/BillAmount.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
