﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Accommodation.aspx.cs" Inherits="WebApiOauth2.ContentPages.Acco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Secretariat Accommodation </h5>
    </div>


    <div class="dydata">
<div class="row">
  <div class="col-4"><small>ALL  </small> <div id="all"> </div></div>
    <div class="col-4"><small>OCCUPIED  </small>  <div id="occ">  </div></div>
 <div class="col-4"><small>VACANT   </small>  <div id="vac">  </div></div>
</div>
</div>  
    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                 <div class="tbl-hdr">
                    <div class="row">
                        <label class="col-sm-auto col-form-label"><strong>Building  </strong><span class="float-right"></span></label>
                        <div class="col-sm-2">
                            <select id="ddl_building" class="form-control form-control-sm">
                            </select>
                        </div>

                        <label class="col-sm-auto col-form-label"><strong>Room Status  </strong><span class="float-right"></span></label>
                        <div class="col-sm-2">
                            <select id="ddl_room" class="form-control form-control-sm">
                            </select>
                        </div>
                        <label class="col-sm-auto col-form-label"><strong>Room Category  </strong><span class="float-right"></span></label>
                        <div class="col-sm-2">
                            <select id="ddl_rm_cat" class="form-control form-control-sm">
                            </select>
                        </div>
                    </div>
                        </div>

                <div class="card-body">
                   
                
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID </th>
                                 
                                    <th>Department</th>
                                    <th>Room No </th>
                                    <th>Room Type   </th>
                                    <th>Room Category  </th>
                                    <th>Alloted   </th>
                                    <th>Amenities  </th>
                                    <th>Note </th>
                                    <th>Actions </th>

                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Building  No  :</label>
                                <input type="text" class="form-control" readonly="true"  id="txtbnum"/>
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Floor :</label>
                                <input type="text" class="form-control" readonly="true"  id="txtfrnum"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Dept Code :</label>
                                <%--<input type="text" class="form-control" id="txtdept"/>--%>
                                <select id="txtdept" data-live-search="true" class="form-control">
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Room No   :</label>
                                <input type="text" class="form-control" readonly="true" id="txtrm_num"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Room Type     :</label>
                                <select class="form-control form-control-sm" id="ddl_rm_type">
                                    
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Room Category :</label>
                                <select id="ddl_rm_cat_modal" class="form-control">
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Allotted :</label>
                             
                                 <select class="form-control form-control-sm" data-live-search="true" id="ddl_rm_alloted">
                                    
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Amenities :</label>
                                <div>
                                <select class="form-control form-control-sm"  multiple="multiple" data-live-search="true" id="txtamen">
                                    
                                </select>
                                    </div>
                            </div>


                            <div class="col-md-6">
                                <label class="col-form-label">Room Modification   :</label>
                              <%--  <input type="text" class="form-control" id="txtmod_date"/>--%>
                                   <div class="form-group">
                                <div class="input-group date" id="txt_fromdate">
                                    <input type="text" id="txtmod_date"  class="form-control form-control-sm" required=""/>
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Notes  :</label>
                                <textarea class="form-control" rows="4" id="txt_note"></textarea>
                                <%--<input type="text" class="form-control" >--%>
                            </div>





                        </div>

                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" id="btn_save" class="btn btn-success btn-sm">SAVE</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts"
    runat="server">
    <script src="../JavaScripts/Accommodation.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
