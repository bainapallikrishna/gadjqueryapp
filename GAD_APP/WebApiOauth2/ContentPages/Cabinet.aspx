﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Cabinet.aspx.cs" Inherits="WebApiOauth2.ContentPages.Cabinet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Cabinet </h5>
    </div>

    <div class="form-group row">
        <div class="col-lg-3">
            <label>Department : </label>
            <%--  <select  class="form-control form-control-sm">
                     <option> Gad </option>
                      <option> Revenue    </option>
                    </select>--%>
            <select id="txtdept" data-live-search="true" class="form-control form-control-sm">
            </select>
        </div>


        <div class="col-lg-2">
            <label>From </label>
            <div class="form-group">
                <div class="input-group date" id="">
                    <input type="text" id="txt_fromdate" class="form-control form-control-sm" required="" />
                    <div class="input-group-addon input-group-append">
                        <div class="input-group-text">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2">
            <label>To </label>
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" id="txt_todate" class="form-control form-control-sm" required="" />
                    <div class="input-group-addon input-group-append">
                        <div class="input-group-text">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-lg-3">
            <label>&nbsp; </label>
            <div>
                <button type="button" id="btnsearch" class="btn btn-primary btn-sm">Search </button>
            </div>

        </div>
        <div class="col-lg-2">
            <label>&nbsp; </label>
            <div>
                <button type="button" class="btn btn-primary btn-sm" id="btncreatenew">Create New </button>
            </div>

        </div>


    </div>



    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>S.No </th>
                                    <th>Department </th>
                                    <th>CR No</th>
                                    <th>Date of Cabinet Meeting</th>
                                    <th>Title </th>
                                    <th>Resolution  </th>
                                    <th>Status </th>
                                    <th>Remarks</th>
                                    <th>Actions </th>

                                </tr>
                            </thead>

                            <tbody>
                                

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="AddCabinet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <div class="col-md-6">

                                <label class="col-form-label">Date :</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <input type="text" id="txtmod_date" class="form-control form-control-sm" autocomplete="off" required="" />
                                        <div class="input-group-addon input-group-append">
                                            <div class="input-group-text">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">CR No :</label>
                                <input type="text" class="form-control" autocomplete="off" id="txtcrnum" />


                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">

                                <label class="col-form-label">Title :</label>
                                <input type="text" class="form-control" autocomplete="off" id="txttitle" />


                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Department Name :</label>
                                <select id="txtmd_dept" data-live-search="true"   class="form-control">
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Resolution     :</label>
                                <textarea class="form-control" rows="4"  id="txt_resultion"></textarea>
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Status :</label>
                                <textarea class="form-control" rows="4" id="txt_status"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <label class="col-form-label">Remarks :</label>

                                <textarea class="form-control" rows="4" id="txtremarks"></textarea>
                            </div>
                         <%--   <div class="col-md-6" id="impdate">

                                <label class="col-form-label">Implemented Date :</label>
                                <div class="form-group">
                                    <div class="input-group date" id="">
                                        <input type="text" id="txt_Res_date" class="form-control form-control-sm" required="" />
                                        <div class="input-group-addon input-group-append">
                                            <div class="input-group-text">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>--%>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" id="btn_save" class="btn btn-success btn-sm">SAVE</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditCabinet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                     

                        <div class="form-group row">
                            <div class="col-md-6">

                                <label class="col-form-label">Title :</label>
                                <input type="text" class="form-control" autocomplete="off" id="txtedtitle" />


                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Resolution     :</label>
                                <textarea class="form-control" rows="4" id="txt_edresultion"></textarea>
                            </div>
                            
                        </div>


                        <div class="form-group row">
                            

                            <div class="col-md-6">
                                <label class="col-form-label">Status :</label>
                                <textarea class="form-control" rows="4" id="txt_edstatus"></textarea>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Remarks :</label>

                                <textarea class="form-control" rows="4" id="txtedremarks"></textarea>
                            </div>
                        </div>

                        

                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" id="btn_update_save" class="btn btn-success btn-sm">UPDATE</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/Cabinet.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
