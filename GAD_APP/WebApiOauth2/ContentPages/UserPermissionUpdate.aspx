﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserPermissionUpdate.aspx.cs" Inherits="WebApiOauth2.ContentPages.UserPermissionUpdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Permission Update</title>
      <meta charset="utf-8"/>
   <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link href="../css/style.css" rel="stylesheet" />

    <link href="../css/admin.css" rel="stylesheet" />
     <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" />
     <link href="../css/bootstrap-glyphicons.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container-fluid">
        <div class="row">

     
     

      
        <div class="col-md-6">
                                <label class="label">User Names </label>
                               
                                <select id="ddl_UserName" class="form-control" data-live-search="true">
                                </select>
                            </div>

            <div class="col-md-6">
                         <label class="label">&nbsp; </label>
                            <div><button type="button" id="getpages" class="btn btn-info">Get PageNames</button></div>
                               
                            </div>


         

         
           </div>

       
           <div class="row" id="hidetable">
     

            <table style="width: 100%;" class="table table-striped table-bordered table-hover table-checkable users list dtable" id="user_table">
                <thead >
                    <tr>
                        <th>S.No</th>
                        <th>Page Name</th>

                      <%--  <th>Page ID</th>
                              <th>Group ID</th>
                        <th>User ID</th>--%>
                        <th>Action</th>
                    </tr>

                </thead>
                <tbody>
                
                </tbody>
              

            </table>

            
         
        </div>
        </div>
           <button type="button" id="updateuserdetails"  class="btn btn-danger">Delete</button>
    </form>
         <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../js/thirdparty.js"></script>
        <script src="../js/js.cookie.min.js"></script>
     <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="../vendor/datatables/dataTables.buttons.min.js"></script>
        <script src="../vendor/datatables/jszip.min.js"></script>
        <script src="../vendor/datatables/pdfmake.min.js"></script>
        <script src="../vendor/datatables/pdfmake_vfs_fonts.js"></script>
        <script src="../vendor/datatables/buttons.html5.min.js"></script>
        <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <link href="../css/select2.min.css" rel="stylesheet" />
    <script src="../js/select2.min.js"></script>`
 <script src="../JavaScripts/UserPermissionUpdate.js"></script>
</body>
</html>

