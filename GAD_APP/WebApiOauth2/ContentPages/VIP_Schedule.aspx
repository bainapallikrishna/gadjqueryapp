﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="VIP_Schedule.aspx.cs" Inherits="WebApiOauth2.ContentPages.VIP_Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Create VIP diary schedule   </h5>
    </div>


    <div class="row  justify-content-center">
        <div class="col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Choose Date</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="input-group date" id="id_4">
                                    <input type="text" value="" id="vip_date" class="form-control form-control-sm" required="" placeholder="Choose Date.." />
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">From Time</label>
                        <div class="col-sm-10">
                            <div class="form-group">

                                <div class="input-group date" id="id_2">
                                    <input type="text" name="end_time" class="form-control form-control-sm" placeholder="From time" title="" required="" id="id_start_time" />
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="far fa-clock"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">To Time </label>
                        <div class="col-sm-10">
                            <div class="input-group date" id="totime">
                                <input type="text" name="end_time" class="form-control form-control-sm" placeholder="To time" title="" required="" id="id_end_time" />
                                <div class="input-group-addon input-group-append">
                                    <div class="input-group-text">
                                        <i class="far fa-clock"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Meeting  Category </label>
                        <div class="col-sm-10">
                            <select id="ddl_cat" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Meeting Notice  </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="txt_subject" rows="2"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <button type="button" id="btn_add" class="btn btn-success btn-sm">Add </button>
                            <button type="button" id="btn_update" class="btn btn-success btn-sm">Update </button>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    

    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th     width="88px">Date </th>
                                    <th>From Time</th>
                                    <th>To Time</th>
                                    <th>Meeting Notice</th>
                                    <th>Meeting Category</th>
                                    <th>Actions </th>

                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/VIPSchedule.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>

