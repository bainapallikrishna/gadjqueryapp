﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="FTRForm16Report.aspx.cs" Inherits="WebApiOauth2.ContentPages.FTRForm16Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">FTR Form F Report </h5>
    </div>
    <div class="tbl-hdr">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Year</label>
                    <input class="form-control date" id="Fdd_yyear" />
                    <%-- <select id="ddl_year" class="form-control">
                                    </select>--%>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">Month</label>
                    <input class="form-control date" id="Fdd_mmonth" />
                    <%-- <select id="ddl_month" class="form-control">
                                    </select>--%>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xl-6 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Applied</div>
                            <a href="javascript: void (0);" class="h5 mb-0 font-weight-bold text-gray-800" id="Totalapplied"></a>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-thumbs-up fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-6 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Approved </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <a href="javascript: void (0);" class="h5 mb-0 mr-3 font-weight-bold text-gray-800" id="Totalapproved"></a>
                                </div>
                                <!--  <div class="col">
                            <div class="progress progress-sm mr-2">
                              <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div> -->
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-check fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row justify-content-center">
        <div class="col-lg-12" id="appliedtable">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <h6 align="center">FTR Form F Applied Details</h6>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-stripped" id="dataTable">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Form Number </th>
                                    <th>Department Name </th>
                                    <th>Persons count </th>
                                    <th>Created Date</th>
                                    <th>Subject </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>


        <div class="col-lg-12" id="approvedtable">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <h6 align="center">FTR Form F Approved Details</h6>
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-stripped" id="Tableapproved">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Form Number </th>
                                    <th>Department Name </th>
                                    <th>Persons count </th>
                                    <th>Created Date</th>
                                    <th>Subject </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <%-- <div class="col-lg-12" id="membertable">
            <div class="card shadow mb-4">
                 <div class="card-body">
                     <h6 align="center">FTR Form F Member Details</h6>
                     <div class="table-responsive">
                         <table class="table table-bordered table-sm table-stripped" id="Tablemember">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Member Name </th>
                                    <th>Service </th>
                                    <th>Aadhaar Number</th>
                                    <th>Department</th>
                                    <th>Designation</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                 </div>
            </div>
        </div>--%>
    </div>


    <div class="modal fade" id="Update_vehicledetails" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">FTR Form F Member Details</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-12" id="membertable">
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <h6 align="center">FTR Form F Member Details</h6>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-sm table-stripped" id="Tablemember">
                                            <thead>
                                                <tr>
                                                    <th>S.No</th>
                                                    <th>Member Name </th>
                                                    <th>Service </th>
                                                    <th>Aadhaar Number</th>
                                                    <th>Department</th>
                                                    <th>Designation</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" aria-label="Close">
                        Close
                    </button>
                    <%-- <button type="button" id="btn_update" class="btn btn-success btn-sm">Update</button>--%>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/FtrForm16report.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
