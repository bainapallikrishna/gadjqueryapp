﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="ACRApplication.aspx.cs" Inherits="WebApiOauth2.ContentPages.ACRApplication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">ACR </h5>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="frmhdr text-center">
                        <h5>ANNEXURE </h5>
                        <h6>FORM - A </h6>
                        <p>Part - I</p>

                    </div>


                    <p class="txt-upp text-center">(To be filled by the officer reported upon)  </p>

                    <div class="form-group row p-3 p-3">
                        <div class="col-lg-6">
                            <h6>Annual Confidential Report of Gazetted Officers for the year.</h6>
                        </div>
                        <div class="col-lg-3">
                            <label>From </label>
                            <div class="form-group">
                                <div class="input-group date" id="">
                                    <input type="text" id="txt_fromdate"  class="form-control form-control-sm" required=""/>
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <label>To </label>
                            <div class="form-group">
                                <div class="input-group date">
                                    <input type="text" id="txt_todate" class="form-control form-control-sm" required=""/>
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="col-form-label"><strong>1. </strong>A brief summary of duties and responsibilites (not more than 50 words). </label>

                        <textarea class="form-control" id="txt_point1" rows="2"></textarea>

                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="col-form-label"><strong>2. </strong>Please specify important items of work in order of priorrity where in quantitative / physical financial targets / objectives  / goals were set for you or set by yourself for the reporting year and achivements made.  </label>

                        <div class="table-responsive">
                            <table class="table table-bordered table-sm " width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Item of Work. </th>
                                        <th>Physical or Financial Target/Objective/goal </th>
                                        <th>Achievements </th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>1 .</td>
                                        <td>
                                            <input type="text" class="form-control" id="txt21a"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="txt21b"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="txt21c"/>
                                        </td>


                                    </tr>
                                    <tr>
                                        <td>2 .</td>
                                        <td>
                                            <input type="text" class="form-control" id="txt22a"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="txt22b"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="txt22c"/>
                                        </td>
                                        

                                    </tr>
                                    <tr>
                                        <td>3 .</td>
                                        <td>
                                            <input type="text" class="form-control" id="txt23a"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="txt23b"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="txt23c"/>
                                        </td>
                                        

                                        <tr>
                                            <td>4 .</td>
                                            <td>
                                                <input type="text" class="form-control" id="txt24a"/>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            

                                        </tr>
                                        <tr>
                                            <td>5 .</td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            

                                        </tr>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class=" col-lg-6 col-form-label">
                            <strong>3. </strong>
                            (a) In case of shortfall of expected quality / quantity of performance please sate the reasons  (not more than 200 words).
                   
                        </label>
                        <div class="col-lg-6">
                            <textarea class="form-control" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class=" col-lg-6 col-form-label">
                            <div>&nbsp;  &nbsp;   (b) Please inidicate your contribution in case of significantly higher achivement of target / goal / objective (not more than 200 words). </div>

                        </label>
                        <div class="col-lg-6">
                            <textarea class="form-control" rows="2"></textarea>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-sm-6 col-form-label"><strong>4. </strong>Date of submission of annual property returns Statement pertaining to the year</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-6 col-form-label">&nbsp; </label>
                        <div class="col-sm-6">
                            <button type="button" id="btn_Submit" class="btn btn-success">Submit </button>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>1. </strong>Name of the officer  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>&nbsp; &nbsp;  </strong>Date of Birth  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>2. </strong>Appointment held during the  year(with date) and pay and scale of pay <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>4. </strong>(a) Acceptance of otherwise of the self appraisal report of the Gazetted Officer Indicated In Part I  and if not agreed  to the reasons there for.  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">(b)  Manner in  which  the officer discharged his duties  during the year i.e, if satisfactory or other wise ( specific instances of un satisfactory work if adversely commended on to be cited with number and date of orders  passed.)  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>5. </strong>Does the Officer exhibit  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <select id="inputState" class="form-control">
                                <option selected>Choose...</option>
                                <option>Patience  </option>
                                <option>Tact   </option>
                                <option>Courtesy  </option>
                                <!-- <option> Impartiality  in his relations with the public and Subordinate Or Superior staff with whom he  comes in contact  </option> -->
                            </select>
                        </div>
                    </div>





                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>6. </strong>Is the officer   <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <select id="inputState" class="form-control">

                                <option>Of good character </option>
                                <option>Of sound constitution   </option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>7. </strong>Is  the officer    <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <select id="inputState" class="form-control">
                                <option>Physically  energetic   </option>
                                <option>Mentally alert    </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>8. </strong>How   the officer    <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <select id="inputState" class="form-control">
                                <option>Initiative and drive  </option>
                                <option>Power of control    </option>
                                <option>Powers of application   </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>9. </strong>Has the ooficer any special Characteristics and for any outstanding  mertis or abilities which  would justify his advancement and special selection for higher  appointment in the service?     <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>10. </strong>Is he confirmed in this post if not. What is his substantive post?   <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">(a)  Date of submission of annual property Returns   Statement pertaining to the year  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="yes" name="GAD" class="custom-control-input">
                                <label class="custom-control-label" for="yes">YES </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="no" name="GAD" class="custom-control-input">
                                <label class="custom-control-label" for="no">NO </label>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>11. </strong>punishments, censures or special recommendations in the period under report   <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>12. </strong>(a) Date of  communication of adverse remarks since last report.  <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">(b)  Orders on the representation if any arising from (a) above <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>13. </strong>General remarks ( comment  generally on the way the officer carried out  duties, estimate of his personality etc)    <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label"><strong>6. </strong>Grading   <span class="float-right">: </span></label>
                        <div class="col-sm-4">
                            <select id="inputState" class="form-control">

                                <option>Outsanding </option>
                                <option>Very good    </option>
                                <option>Good    </option>
                                <option>Satis factory   </option>
                                <option>Poor    </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label"></label>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-success">Submit </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
