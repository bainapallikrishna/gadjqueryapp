﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="GuestHouse_Approve.aspx.cs" Inherits="WebApiOauth2.ContentPages.GuestHouse_Approve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-3">
        <h5 class="page-heading mb-0 text-gray-800">Guest House Requests  Approval</h5>
    </div>


    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">



                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Guest Name </th>
                                    <th>Dates</th>
                                    <th>Guest House Name </th>
                                    <th>Room Type   </th>
                                    <th>Purpose Of Visit</th>
                                    <th>Vechile Allotment  </th>
                                    <th>Note </th>
                                    <th>Status </th>
                                    <th>Actions </th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>





    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Approve </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card shadow mb-4">
                                <div class="card-body">

                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label for="">Guest House  Name</label>
                                                <select id="ddl_ghnames" class="form-control">
                                                </select>
                                            </div>


                                            <div class="form-group ">
                                                <label for="">Room type</label>
                                                <select id="ddl_rmtype" class="form-control">
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label for=" ">Designation</label>
                                                <input type="text" id="txt_desig" class="form-control" />
                                            </div>

                                            <div class="form-group">
                                                <label>Guest name </label>
                                                <input type="text" id="txt_name" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Mobile No </label>
                                                <input type="text" id="txt_mobile" class="form-control" />
                                            </div>

                                            <div class="form-group">
                                                <label>Note </label>
                                                <input type="text" id="txt_note" class="form-control" />
                                            </div>



                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <label>Form Date </label>
                                                    <input type="text" class="form-control datepicker" id="from_date" />
                                                </div>

                                                <div class="col-md-6">
                                                    <label>To Date </label>
                                                    <input type="text" class="form-control datepicker" id="to_date" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Purpose of visit </label>
                                                <input type="text" id="txt_pur_visit" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Reporting Time </label>
                                                <input type="text" class="form-control datepicker" id="txt_rptime" />
                                            </div>










                                            <div class="form-group ">
                                                <label for="">Vechile Allotment</label>
                                                <select id="ddl_vel_all" class="form-control">
                                                    <option selected="selected">Choose...</option>
                                                    <option value="1">YES</option>
                                                    <option value="0">NO</option>

                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label for="">Room No </label>
                                                <input type="text" id="txt_rm_no" class="form-control" />
                                            </div>


                                            <div class="form-group ">
                                                <label for="">Room Status </label>
                                                <select id="ddl_rm_status" class="form-control">
                                                    <option selected="">Choose...</option>
                                                    <option>Availble </option>
                                                    <option>Not-Availble</option>

                                                </select>
                                            </div>


                                            <div class="form-group">
                                                <label>Vechile No </label>
                                                <input type="text" class="form-control" id="txt_vno" />
                                            </div>


                                            <div class="form-group">
                                                <label>Driver Id </label>
                                                <input type="text" class="form-control" id="txt_dr_id" />
                                            </div>

                                            <div class="form-group ">
                                                <label for=" ">Order of priority</label>
                                                <select id="ddl_order" class="form-control">
                                                    <option selected="selected">Choose...</option>
                                                    <option value="1">Minister</option>
                                                    <option value="2">MP</option>
                                                    <option value="3">MLA</option>
                                                    <option value="4">MLC</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label for=" ">Id proof type </label>
                                                <input type="text" class="form-control" id="txt_id_proof" />
                                            </div>

                                            <div class="form-group ">
                                                <label for="">ID Proof path </label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="fl_ipproof" />
                                                    <label class="custom-file-label" for="customFile" id="lblidproof">Choose file</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Admin  Note </label>
                                                <input type="text" class="form-control" id="txt_adminnote" />
                                            </div>




                                            <div class="form-group ">
                                                <br>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6"></div>
                                                <div class="col-md-6">
                                                    <button type="button" id="btn_submit" class="btn btn-success btn-block">Submit </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="Cancel_Request" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Cancel </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card shadow mb-4">
                                <div class="card-body">

                                   <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label for="">Guest House  Name</label>
                                                <textarea class="form-control" id="txt_rej_reason" rows="3"></textarea>
                                                <%--<select id="ddl_ghnames" class="form-control">
                                                </select>--%>
                                            </div>

                                           

                                        </div>

                                        
                                    </div> 
                                       <div class="form-group row">
                                                <div class="col-md-6"></div>
                                                <div class="col-md-6">
                                                    <button type="button" id="btn_submit_cancel" class="btn btn-success btn-block">Submit </button>
                                                </div>
                                            </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    


    


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="../JavaScripts/GuestHouse_Approve.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
