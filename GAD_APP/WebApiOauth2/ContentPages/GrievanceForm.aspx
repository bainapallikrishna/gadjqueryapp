﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="GrievanceForm.aspx.cs" Inherits="WebApiOauth2.ContentPages.GrievanceForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Grievance Request Form  </h5>
    </div>


    <div class="row  justify-content-center">
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Name </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="txt_name" autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Designation  </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="txt_Designation" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Who owned the request  </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="txt_request" autocomplete="off" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Date</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="input-group date" id="id_4">
                                    <input type="text" value="" id="req_date" class="form-control form-control-sm" required="" placeholder="Choose Date.." />
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Gist of the Request </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="txt_gist_pnts" rows="2"></textarea>
                        </div>
                    </div>
                    <div class="input-group">
                       <label for="inputPassword" class="col-sm-2 col-form-label">Upload File </label>
                        <div class="col-sm-10">
                            <input type="file" class="custom-file-input" id="fl_upfile"
                                aria-describedby="inputGroupFileAddon01" />
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                     <br/>
                     <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Section </label>
                        <div class="col-sm-10">
                            <select id="ddl_section" class="form-control">
                            </select>
                        </div>
                    </div>
                    <br/>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <button type="button" id="btn_submit" class="btn btn-success btn-sm">Submit </button>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/GrievanceRequestFrom.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
