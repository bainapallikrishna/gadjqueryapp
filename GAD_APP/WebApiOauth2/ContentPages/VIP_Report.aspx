﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="VIP_Report.aspx.cs" Inherits="WebApiOauth2.ContentPages.VIP_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">VIP diary Schedule Report  </h5>
    </div>
    <div class="row  justify-content-center">
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">From Date</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="input-group date" id="">
                                    <input type="text" value="" id="rp_from_date" class="form-control form-control-sm" required="" placeholder="Choose Date.." />
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label for="inputPassword" class="col-sm-2 col-form-label">To Date</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="input-group date" id="">
                                    <input type="text" value="" id="rp_to_date" class="form-control form-control-sm" required="" placeholder="Choose Date.." />
                                    <div class="input-group-addon input-group-append">
                                        <div class="input-group-text">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Meeting Category </label>
                        <div class="col-sm-10">
                            <select id="rp_ddl_cat" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <button type="button" id="btn_filter" class="btn btn-success btn-sm">Search </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div>
         <%--<input type="button" id="btn_Download" class="btn btn-secondary" value="Download Report" />--%>
    </div>
    

   
  <div id="reports">

    </div>

     

    
   <%-- <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Date </th>
                                    <th>From Time</th>
                                    <th>To Time</th>
                                    <th>Meeting Notice</th>
                                    <th>Meeting Category</th>


                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../js/html2canvas.js"></script>
    <script src="../js/jspdf.debug.js"></script>
    <script src="../JavaScripts/VIPScheduleReport.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
