﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="GHSDailyStatusReport.aspx.cs" Inherits="WebApiOauth2.ContentPages.GHSDailyStatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .buttons-pdf {
            background-color: blue;
            color: white;
        }
    </style>

    <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <h5 class="page-heading mb-0 text-gray-800">Guesthouse Daily Status</h5>
    </div>

    <div class="row">

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="txttotal"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-hotel fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Occupied</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800" id="txtoccupied"></div>
                                </div>
                                <!--  <div class="col">
                            <div class="progress progress-sm mr-2">
                              <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div> -->
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-door-closed fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-default shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-default text-uppercase mb-1">Vacant</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="txtvacant"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fa fa-door-open fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--        <div class="col-xl-2 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Condemned </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="Condemned"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
    </div>

    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="tbl-hdr">
                    <div class="row">
                        <label class="col-sm-auto col-form-label"><strong>GuesthouseName:  </strong><span class="float-right"></span></label>
                        <div class="col-sm-2">
                            <select id="ddl_Guesthouse" class="form-control form-control-sm">
                            </select>
                        </div>

                        <label class="col-sm-auto col-form-label"><strong>Date: </strong><span class="float-right"></span></label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control date" id="txt_ddate" />

                        </div>

                        <div class="col-sm-2">
                            <button type="button" id="btn_search" class="btn btn-success float-right">Search </button>
                        </div>

                        <%-- <label class="col-sm-auto col-form-label"><strong>Room Category  </strong><span class="float-right"></span></label>--%>
                        <div class="col-sm-2 float-right">
                            <button type="button" id="btn_submit" class="btn btn-primary">Add Guest </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">


                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable1" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>ID </th>
                                    <th>Floor </th>
                                    <th>Type of Room</th>
                                    <th>Room Number </th>
                                    <th>Name & Designation of the Guest  </th>
                                    <th>Date of Arrival </th>
                                    <th>Date of Departure </th>
                                    <th>Category  </th>
                                    <th>Remarks </th>
                                    <th>Actions </th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group row">
                            <%-- <div class="col-md-6">
                                <label class="col-form-label">Type of Room </label>
                                <input type="text" class="form-control" id="txtTypeofRoom">
                            </div>--%>
                            <div class="col-md-6">
                                <label class="col-form-label">Room Number </label>
                                <%--<input type="text" class="form-control" id="txtRoomNo">--%>
                                <select id="ddl_RoomNo" class="form-control">
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Name of the Guest </label>
                                <input type="text" class="form-control" id="txtNtGuest" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Designation of the Guest </label>
                                <input type="text" class="form-control" id="txtDOtGuest" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Date of Arrival </label>
                                <input type="text" class="form-control date" id="txtDOArrival" autocomplete="off">
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Date of Departure </label>
                                <input type="text" class="form-control date" id="txtDODeparture" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Category </label>
                                <select id="ddl_Category" class="form-control">
                                </select>
                                <%--  <input type="text" class="form-control" id="txtCategory">--%>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Remarks </label>
                                <input type="text" class="form-control" id="txtRemarks" autocomplete="off">
                            </div>

                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm" id="Guestadd">SAVE</button>
                    <button type="button" class="btn btn-success btn-sm" id="Guestedit">Update</button>
                    <button type="button" class="btn btn-primary btn-sm" id="GuestCheckout">CHECKOUT</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="../JavaScripts/GHSDailyReportJS.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
