﻿$(document).ready(function () {

    $("#btn_login").click(function (e) {

        var txtUserName = $('#txtusername').val();
        var txtpassword = $('#txtpwd').val();

        if (txtUserName == "") {
            _n_plain_mes_1("<strong>info!</strong>", "Please Enter Username", "danger"); $('#txtusername').focus(); return;
        }
        else if (txtpassword == "") {
            _n_plain_mes_1("<strong>info!</strong>", "Please Enter Password", "danger"); $('#txtpwd').focus(); return;
        }
        else {
            var key = CryptoJS.enc.Utf8.parse('8080808080808080');
            var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

            var encrypteduser = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(txtUserName), key,
                {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });

            var encryptedpassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(txtpassword), key,
                {
                    keySize: 128 / 8,
                    iv: iv,
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "../GetUserLogin_Details?Username=" + encrypteduser + "&Password=" + encryptedpassword,
                "method": "GET",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Cache-Control": "no-cache",
                    "Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
                }
            };
            $.ajax(settings).done(function (response) {

                if (response.status == "1") {
                    Tokengeneration(response.userId, response.pwd);
                    _n_plain_mes_1("<strong>success!</strong>", "Sign In successfully", "success"); return;
                }
                else if (response.status == "4") {
                    _n_plain_mes_1("<strong>Fail!</strong>", response.reason, "danger"); return;
                }
                else {
                    _n_plain_mes_1("<strong>fail!</strong>", "UserName or Password incorrect", "warning"); return;
                }
            }).fail(function (data, textStatus, xhr) {
                if (data.status == 401) {
                    _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
                    window.location = "Login";
                }
            });
        }


    });

   
});


function Tokengeneration(username,password) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/token",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "Postman-Token": "aabda86a-ff15-4846-b574-632a9edc88ad"
        },
        "data": {
            "grant_type": "password",
            "UserName": username,
            "Password": password
        }
    };

    $.ajax(settings).done(function (response) {
        //  console.log(response);
        Cookies.set('token', response.access_token);
        //console.log(Cookies.get('token'))
        if (response.access_token != null && response.access_token != "") {
            window.location = "Home";
        }
    });
}