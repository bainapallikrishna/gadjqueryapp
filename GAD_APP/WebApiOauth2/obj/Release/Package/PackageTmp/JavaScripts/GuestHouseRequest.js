﻿var req_obj = {
    "ghs_id": "",
    "room_type": "",
    "guest_name": "",
    "room_number": "",
    "from_date": "",
    "to_date": "",
    "category": "",
    "remarks": "",
    "RoomStatus": "",
    "OrderID":""
}
var OrderID = "";
$(document).ready(function () {
    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#GuHreid').addClass('active');

    loadGH_Names();
    load_GHRoomTypes();
    load_GH_Cat();
    load_GH_Floors();
    Room_Master_Get(0);

    $("#ddl_ghnames").change(function (e) {
        load_Rooms_GET();
    })


    $('#txt_arr_date,#txt_dept_date,#txt_search_date').datetimepicker({
        "allowInputToggle": true,
        "showClose": true,
        "showClear": true,
        "showTodayButton": true,
        "format": "DD/MM/YYYY",
    });



    $("#btn_submit").click(function () {
        if (OrderID == undefined || OrderID == "") {
            save();
        } else {
            update();
        }
    })
    $("#btn_search").click(function () {
        Room_Master_Get($("#txt_search_date").val());
    })
});


function Room_Master_Get(date) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Room_Master_Get?date="+date,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);

        if (response.responseCode == 1) {

            details = response.data;
            var i = 1;
            $('#dt_ghTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },

                    { data: "ghS_NAME" },
                    { data: "rooM_TYPE" },
                    { data: "rooM_NO" },
                    { data: "guesT_DETAILS" },
                    { data: "froM_DATE" },
                    { data: "tO_DATE" },
                    {data:"rooM_CATEGORY"},
                    { data: "rooM_FLOOR" },
                    { data: "note" },

                {
                    "data": "ordeR_ID",
                    "type": "sno",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='edit' sno='" + row.ordeR_ID + "' data-toggle='modal' data-target='#exampleModal'><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dt_ghTable').on('click', '.edit', function () {
                RoomDetails_edit($(this).attr('sno'));
            });
        } else {

            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}


function loadGH_Names() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=1",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].ghS_ID + "'>" + response.data[index].ghS_NAME + "</option>"
            }
            $("#ddl_ghnames").html(html);
        } else {
            $("#ddl_ghnames").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function load_GHRoomTypes() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=3",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //  console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_ID + "'>" + response.data[index].floors + "</option>"
            }
            $("#ddl_rmtype").html(html);
        } else {
            $("#ddl_rmtype").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function load_GH_Cat() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=6",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_CATEGORY + "'>" + response.data[index].rooM_CATEGORY + "</option>"
            }
            $("#ddl_cat").html(html);
        } else {
            $("#ddl_cat").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function load_GH_Floors() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=2",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].romM_ID + "'>" + response.data[index].floors + "</option>"
            }
            $("#ddl_rmfl").html(html);
        } else {
            $("#ddl_rmfl").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function save() {

    req_obj.ghS_ID = $("#ddl_ghnames").val();
    req_obj.rooM_Type = $("#ddl_rmtype").val();
    req_obj.room_number = $("#txt_rm").val();

    req_obj.guest_name = $("#txt_name_degi").val();
    req_obj.from_date = $("#txt_arr_date").val();
    req_obj.to_date = $("#txt_dept_date").val();
    req_obj.category = $("#ddl_cat").val();
    req_obj.remarks = $("#txt_remarks").val();
    req_obj.RoomStatus = "0";



    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_User_Request",
        "method": "POST",
        "data": JSON.stringify(req_obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage);
            window.location.reload();
        } else {
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function update() {

    req_obj.ghS_ID = $("#ddl_ghnames").val();
    req_obj.rooM_Type = $("#ddl_rmtype").val();
    req_obj.room_number = $("#txt_rm").val();

    req_obj.guest_name = $("#txt_name_degi").val();
    req_obj.from_date = $("#txt_arr_date").val();
    req_obj.to_date = $("#txt_dept_date").val();
    req_obj.category = $("#ddl_cat").val();
    req_obj.remarks = $("#txt_remarks").val();
    req_obj.RoomStatus = "0";
    req_obj.OrderID = OrderID;


    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_User_Request",
        "method": "POST",
        "data": JSON.stringify(req_obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage);
            window.location.reload();
        } else {
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function RoomDetails_edit(ordeR_ID) {
    var tempobj;
    OrderID = ordeR_ID;
    for (var index = 0; index < details.length; index++) {
        if (details[index].ordeR_ID == ordeR_ID) {
            tempobj = details[index];
            break;
        }
    }
    $("#ddl_ghnames option").each(function () {
        if (($.trim($(this).text()) == ($.trim(tempobj.ghS_NAME)))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_ghnames').val($('#ddl_ghnames').find(":selected").attr('value'));

    $("#ddl_rmtype option").each(function () {
        if (($.trim($(this).text()) == ($.trim(tempobj.rooM_TYPE)))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_rmtype').val($('#ddl_rmtype').find(":selected").attr('value'));

    $("#ddl_rmfl option").each(function () {
        if (($.trim($(this).text().toUpperCase()) == ($.trim(tempobj.rooM_FLOOR).toUpperCase()))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_rmfl').val($('#ddl_rmfl').find(":selected").attr('value'));


    $("#ddl_cat option").each(function () {
        if (($.trim($(this).text()) == ($.trim(tempobj.rooM_CATEGORY)))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_cat').val($('#ddl_cat').find(":selected").attr('value'));

   
    $("#txt_rm").val(tempobj.rooM_NO);
    $("#txt_name_degi").val(tempobj.guesT_DETAILS);
    $("#txt_arr_date").val(tempobj.froM_DATE);
    $("#txt_dept_date").val(tempobj.tO_DATE);
    $("#txt_remarks").val(tempobj.note);


//    $("#btn_submit").text("Update");
}


function load_Rooms_GET() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Rooms_GET?gid="+$("#ddl_ghnames").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_NO + "'>" + response.data[index].rooM_NO + "</option>"
            }
            $("#txt_rm").html(html);
        } else {
            $("#txt_rm").html(html);
          //  alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}