﻿

$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#collapseTwo').addClass('show');
    $('#FTRId').addClass('active');

    var getdate = null;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var todaydate = date.split('-');
    if (todaydate[1] != "10" && todaydate[1] != "11" && todaydate[1] != "12") {
        getdate = todaydate[2] + '-' + "0" + todaydate[1] + '-' + todaydate[0];
        $('#Fdd_yyear').val(todaydate[0]);
        $('#Fdd_mmonth').val("0" + todaydate[1]);
    }
    if (todaydate[1] == "10" || todaydate[1] == "11" || todaydate[1] == "12") {
        getdate = todaydate[2] + '-' + todaydate[1] + '-' + todaydate[0];
        $('#Fdd_yyear').val(todaydate[0]);
        $('#Fdd_mmonth').val(todaydate[1]);
    }
    loadGetStateGuestDetails($('#Fdd_yyear').val(), $('#Fdd_mmonth').val());

    $('#appliedtable').hide();
    $('#approvedtable').hide();
    $('#membertable').hide();
});

$("#Fdd_yyear").change(function (e) {
    if ($("#Fdd_yyear").val() != "") {
        $('#appliedtable').hide();
        $('#approvedtable').hide();
        $('#membertable').hide();
        loadGetStateGuestDetails($('#Fdd_yyear').val(), $('#Fdd_mmonth').val());

    } else {
        $('#appliedtable').hide();
        $('#approvedtable').hide();
        $('#membertable').hide();
        _n_plain_mes_1("<strong>Please select year.. !</strong>", "", "danger");
    }
});

$("#Fdd_mmonth").change(function (e) {
    if ($("#Fdd_mmonth").val() != "") {
        $('#appliedtable').hide();
        $('#approvedtable').hide();
        $('#membertable').hide();
        loadGetStateGuestDetails($('#Fdd_yyear').val(), $('#Fdd_mmonth').val());
    } else {
        $('#appliedtable').hide();
        $('#approvedtable').hide();
        $('#membertable').hide();
        _n_plain_mes_1("<strong>Please select month.. !</strong>", "", "danger");
    }
});

function loadGetStateGuestDetails(year, month) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetFTRReport_Details?year=" + year + "&month=" + month,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    };
    $.ajax(settings).done(function (response) {
        if (response.status == "Success") {
            $('#Totalapplied').text(response.applied);
            $('#Totalapproved').text(response.approved);
        } else {
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }
    });
}


$('#Totalapplied').click(function () {
    if ($("#Fdd_yyear").val() == "") {
        _n_plain_mes_1("<strong>Please select year.. !</strong>", "", "danger");
    }
    else if ($("#Fdd_mmonth").val() == "") {
        _n_plain_mes_1("<strong>Please select month.. !</strong>", "", "danger");
    }
    else {
        $('#approvedtable').hide();
        $('#membertable').hide();
        loadFtrApplieddetails($("#Fdd_yyear").val(), $("#Fdd_mmonth").val());
        $('#appliedtable').show();
    }
});

$('#Totalapproved').click(function () {
    if ($("#Fdd_yyear").val() == "") {
        _n_plain_mes_1("<strong>Please select year.. !</strong>", "", "danger");
    }
    else if ($("#Fdd_mmonth").val() == "") {
        _n_plain_mes_1("<strong>Please select month.. !</strong>", "", "danger");
    }
    else {
        $('#appliedtable').hide();
        $('#membertable').hide();
        loadFtrApproveddetails($("#Fdd_yyear").val(), $("#Fdd_mmonth").val());
        $('#approvedtable').show();
    }
});


function loadFtrApplieddetails(year, month) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetFTRappliedReport_Details?year=" + year + "&month=" + month,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    };

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            //  bdata = response.data;
            $('#dataTable').dataTable().fnClearTable();
            var i = 1;
           
            $('#dataTable').DataTable({
                //dom: 'Bfrtip',
                //"dom": '<"dt-buttons"Bf><"clear">lirtp',
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
               
                "displayLength": 100,
                //"paging": true,
                destroy: true,
                "autoWidth": true,
               
                "scrollY": "62vh",
                "scrollCollapse": true,

                data: response.ftrappliedlist,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    //{
                    //    "data": "formnum",
                    //    //"type": "sno",
                    //    "sWidth": "10%",
                    //    "render": function (data, type, row, meta) {
                    //        data = "<ul class='list-inline'><li class='list-inline-item'><a href='javascript: void (0);' class='edit' refno='" + row.refnum + "'>" + row.formnum + "</a></li></ul>";
                    //        return data;
                    //    }
                    //},
                    { data: "formnum", "sWidth": "10%" },
                    { data: "departname", "sWidth": "15%" },
                    {
                        "data": "personcount",
                        //"type": "sno",
                        "sWidth": "10%",
                        "render": function (data, type, row, meta) {
                            data = "<ul class='list-inline'><li class='list-inline-item'><a href='javascript: void (0);' class='edit' refno='" + row.refnum + "'>" + row.personcount + "</a></li></ul>";
                            return data;
                        }
                    },
                    //{ data: "personcount", "sWidth": "10%" },
                    { data: "createddate", "sWidth": "10%" },
                    { data: "subject", "sWidth": "50%" }
                ],
                "bInfo": true
            });
           
            _n_plain_mes_1("<strong>Data Loaded Successfully</strong>", "", "success");

        } else {
            $('#dataTable').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}
$('#dataTable').on('click', '.edit', function () {
    loadFtrMemberdetails($(this).attr('refno'));
    $("html, body").animate({ scrollTop: $(document).height() + $(window).height() });
    $('#membertable').show();
   
});


function loadFtrApproveddetails(year, month) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetFTRapprovedReport_Details?year=" + year + "&month=" + month,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    };

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            //  bdata = response.data;
            $('#Tableapproved').dataTable().fnClearTable();
            var i = 1;

            $('#Tableapproved').DataTable({
                //dom: 'Bfrtip',
                //"dom": '<"dt-buttons"Bf><"clear">lirtp',
                destroy: true,
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
               
                "displayLength": 100,
                //"paging": true,

                "autoWidth": true,
               
                "scrollY": "62vh",
                "scrollCollapse": true,

                data: response.ftrapprovedlist,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    //{
                    //    "data": "formnum",
                    //    //"type": "refnum",
                    //    "sWidth": "10%",
                    //    "render": function (data, type, row, meta) {
                    //        data = "<ul class='list-inline'><li class='list-inline-item'><a href='javascript: void (0);' class='appedit' refno='" + row.refnum + "'>" + row.formnum + "</a></li></ul>";
                    //        return data;
                    //    }
                    //},
                    { data: "formnum", "sWidth": "10%" },
                    { data: "departname", "sWidth": "15%" },
                    {
                        "data": "personcount",
                        //"type": "refnum",
                        "sWidth": "10%",
                        "render": function (data, type, row, meta) {
                            data = "<ul class='list-inline'><li class='list-inline-item'><a href='javascript: void (0);' class='appedit' refno='" + row.refnum + "'>" + row.personcount + "</a></li></ul>";
                            return data;
                        }
                    },
                    //{ data: "personcount", "sWidth": "10%" },
                    { data: "createddate", "sWidth": "10%" },
                    { data: "subject", "sWidth": "50%" }
                ],
                "bInfo": true
            });

            _n_plain_mes_1("<strong>Data Loaded Successfully</strong>", "", "success");

        } else {
            $('#Tableapproved').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}


$('#Tableapproved').on('click', '.appedit', function () {
    loadFtrMemberdetails($(this).attr('refno'));
    $("html, body").animate({ scrollTop: $(document).height() + $(window).height() });
    $('#membertable').show();
   
});

function loadFtrMemberdetails(refno) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetFTRgroupmembersReport_Details?formnum=" + refno,
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    };

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            //  bdata = response.data;
            $('#Tablemember').dataTable().fnClearTable();
            var i = 1;

            $('#Tablemember').DataTable({
                //dom: 'Bfrtip',
                //"dom": '<"dt-buttons"Bf><"clear">lirtp',
                destroy: true,
                aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],

                "displayLength": 100,
                //"paging": true,

                "autoWidth": true,
               
                "scrollY": "62vh",
                "scrollCollapse": true,

                data: response.ftrmemberlist,
                columns: [
                    {
                        "render": function (data, type, full, meta) {
                            return i++;
                        }
                    },
                    { data: "gdname", "sWidth": "15%" },
                    { data: "gdservice", "sWidth": "20%" },
                    { data: "gduid", "sWidth": "10%" },
                    { data: "gddepartment", "sWidth": "20%" },
                    { data: "gddesignation", "sWidth": "20%" }
                ],
                "bInfo": true
            });

            _n_plain_mes_1("<strong>Data Loaded Successfully</strong>", "", "success");

        } else {
            $('#Tablemember').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}