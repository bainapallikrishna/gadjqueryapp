﻿var obj = {
    "ghid": "",
    "roomType": "",
    "roomFloor": "",
    "roomNo": "",
    //"Category": "",
    "AMENITIES": ""
};
var details;

$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#RoommaID').addClass('active');

    loadGH_Names();
    load_GHRoomTypes();
    load_GHRoomFloor();
    load_GHRoomStatus();
    Room_Master_Get();



    $("#btn_submit").click(function (e) {
        Add_Update_Room_Master();
    });
});


function Add_Update_Room_Master() {

 

    if (/*$("#ddl_Status").val() != -1 &&*/
        $("#txt_rmnumber").val() != "" &&
        $("#ddl_rmfloor").val() != 0 &&
        $("#ddl_rmtype").val() != 0 &&
        $("#ddl_ghnames").val() != 0 ) {


        obj.ghid = $("#ddl_ghnames").val();
        obj.roomFloor = $("#ddl_rmfloor option:selected").text();
        obj.roomNo = $("#txt_rmnumber").val();
        obj.roomType = $("#ddl_rmtype option:selected").text();
      //  obj.Category = $("#ddl_Status").val();
        obj.AMENITIES = $("#txt_amts").val();

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "/Room_Master_Add",
            "method": "POST",
            "data": JSON.stringify(obj),
            "headers": {
                "Authorization": "bearer " + Cookies.get('token'),
                "Cache-Control": "no-cache",
                "Content-Type": "application/json",
            }
        }

        $.ajax(settings).done(function (response) {
            //console.log(response);
            if (response.responseCode == 1) {
                alert(response.responseMessage)
                window.location.reload();
            } else {
                alert(response.responseMessage)
            }
        }).fail(function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }

        });
    } else {
        alert("Please fill all fields .....")
    }
}
function load_GHRoomStatus(){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Category_Master?type=6",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        var html = "<option value='-1'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_CATEGORY + "'>" + response.data[index].rooM_CATEGORY + "</option>"
            }
            $("#ddl_Status").html(html);
        } else {
            $("#ddl_Status").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function loadGH_Names() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=1",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].ghS_ID + "'>" + response.data[index].ghS_NAME + "</option>"
            }
            $("#ddl_ghnames").html(html);
        } else {
            $("#ddl_ghnames").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function load_GHRoomTypes() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=3",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_ID + "'>" + response.data[index].floors + "</option>"
            }
            $("#ddl_rmtype").html(html);
        } else {
            $("#ddl_rmtype").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function load_GHRoomFloor() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=2",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        //console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].romM_ID + "'>" + response.data[index].floors + "</option>"
            }
            $("#ddl_rmfloor").html(html);
        } else {
            $("#ddl_rmfloor").html(html);
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function Room_Master_Get() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/Room_Master_Get",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        
        if (response.responseCode == 1) {

            details = response.data;

            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [

                    { data: "ghS_NAME" },
                    { data: "rooM_NO" },
                    { data: "rooM_TYPE" },
                    { data: "rooM_FLOOR" },
                    { data: "rooM_AMENITIES" },
                    //{ data: "rooM_CATEGORY" },

                {
                    "data": "rooM_NO",
                    "type": "sno",
                    "render": function (data, type, row, meta) {
                        data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='edit' sno='" + row.rooM_NO + "' data-toggle='modal' data-target='#exampleModal'><i class='far fa-edit'></i></a></li></ul>";
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                RoomDetails_edit($(this).attr('sno'));
            });
        } else {
        
            alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function RoomDetails_edit(rm_no) {
    var tempobj;
    for (var index = 0; index < details.length; index++) {
        if (details[index].rooM_NO == rm_no) {
            tempobj = details[index];
            break;
        }
    }
    $("#ddl_ghnames option").each(function () {
        if (($.trim($(this).text()) == ($.trim(tempobj.ghS_NAME)))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_ghnames').val($('#ddl_ghnames').find(":selected").attr('value'));

    $("#ddl_rmtype option").each(function () {
        if (($.trim($(this).text()) == ($.trim(tempobj.rooM_TYPE)))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_rmtype').val($('#ddl_rmtype').find(":selected").attr('value'));

    $("#ddl_rmfloor option").each(function () {
        if (($.trim($(this).text().toUpperCase()) == ($.trim(tempobj.rooM_FLOOR).toUpperCase()))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_rmfloor').val($('#ddl_rmfloor').find(":selected").attr('value'));


    $("#ddl_Status option").each(function () {
        if (($.trim($(this).text()) == ($.trim(tempobj.rooM_STATUS)))) {
            $(this).attr('selected', 'selected');
        }
    });

    $('#ddl_Status').val($('#ddl_Status').find(":selected").attr('value'));

    $("#txt_amts").val(tempobj.rooM_AMENITIES);
    $("#txt_rmnumber").val(tempobj.rooM_NO);

    $("#btn_submit").text("Update");

   
}