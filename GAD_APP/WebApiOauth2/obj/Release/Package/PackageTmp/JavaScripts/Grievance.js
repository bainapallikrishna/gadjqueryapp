﻿var obj = {
    "EmpID": "",
    "PERSON_NAME": "",
    "DESGNATION": "",
    "Category": "",
    "SubCategory": "",
    "REQUSET_OWNED": "",
    "DATE_OF_REQUEST": "",
    "REQUEST_GIST": "",
    "UPLOAD_PATH": "",
    "Letter_date": "",
    "Ref_no":""
}
var objfwd = {
    "gri_sno": "",
    "sent_by_emp": "",
    "received_by_emp": "",
    "remarks": ""
}
var filepath = "";
var curr_sno;
var mainurl ="";

$(document).ready(function () {

    loadTable();
    dept_master();
    unit_details();
    category();
    subcategory();
    $("#btn_submit").click(function (e) {
        Grievance_submit();
    })

    $("#fl_upfile").change(function (e) {
        upload_file();
    })

    $("#ddl_unit").change(function (e) {
        emp_details();
    })
    $("#btn_save").click(function (e) {
        fwd();
    })
});

function upload_file() {

    var formData = new FormData();
    var file = $('#fl_upfile')[0];
    formData.append('file', file.files[0]);


    $.ajax({
        url: mainurl+ '/SaveFile_GrievanceFile',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        },
        success: function (d) {
            console.log(d)
            if (d.status == "Success") {
                filepath = d.filepath;
                $('#lbl_file').html(file.files[0].name)
            } else {
                filepath = "";
            }
        },
        error: function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }
        }
    });
};



function Grievance_submit() {
    obj.EmpID = Cookies.get('empid');
    obj.PERSON_NAME = $("#txt_name").val();
    obj.DESGNATION = $("#txt_Designation").val();
    obj.Category = $("#txt_category").val();
    obj.SubCategory = $("#txt_subcategory").val();
    obj.REQUSET_OWNED = $("#txt_request").val();
    obj.DATE_OF_REQUEST = $("#req_date").val();
    obj.REQUEST_GIST = $("#txt_gist_pnts").val();
    obj.UPLOAD_PATH = filepath;
    obj.Letter_date = $("#let_date").val();
    obj.Ref_no = $("#txt_refno").val();

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+"/Grievance_Add",
        "method": "POST",
        "data": JSON.stringify(obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage);
            $('input').val('');
            $('textarea').val('');
            $("#lbl_file").html('');
            $("select").val(0);
            loadTable();
            //    window.location.reload();
        } else {
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });

}

function loadTable() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+ "/GetGrievanceCreatedList?created_by=" + Cookies.get('empid'),
        "method": "GET",
        "data": JSON.stringify(obj),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {

            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": false,
                "pageLength": 10,

                data: response.data,
                columns: [


                    { data: "requested_date" },
                    { data: "letter_date" },
                    { data: "letter_no" },
                    { data: "name" },
                    { data: "designation" },
                    { data: "category" },
                    { data: "sub_category" },
                    { data: "request_owned_by" },
                    { data: "request_for" },
                    { data: "completion_status" },
                {
                    "data": "id",
                    "type": "name",
                    "render": function (data, type, row, meta) {
                        if (row.is_sent == 0) {
                            data = "<ul class='list-inline'><li class='list-inline-item'><a href='#' class='edit' sno='" + row.sno + "' ><i class='fas fa-forward'></i></a></li></ul>";
                        } else {
                            data = '-';
                        }
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                fwd_details($(this).attr('sno'));
            });
        } else {
            $('#dataTable').DataTable().clear().destroy();
            alert(response.responseMessage)
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });

}


function dept_master() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+"/FH_Dept_Master",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].deptcode + "'>" + response.data[index].deptname + "</option>"
            }
            $("#ddl_dept,#ddl_dept_Req").html(html);
            $('#ddl_dept,#ddl_dept_Req').selectpicker();
        } else {
            $("#ddl_dept,#ddl_dept_Req").html(html);
            $('#ddl_dept,#ddl_dept_Req').selectpicker();
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function unit_details() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url":mainurl+ "/Unit_Details",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].unit_id + "'>" + response.data[index].unit_name + "</option>"
            }
            $("#ddl_unit").html(html);

        } else {
            $("#ddl_unit").html('');

            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function emp_details() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+"/GetEmpForGrievanceSend?unit=" + $("#ddl_unit").val() + "&dept_code=" + $("#ddl_dept").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].sno + "'>" + response.data[index].emp_name + "</option>"
            }
            $("#ddl_emp").html(html);
            $('#ddl_emp').selectpicker();
            $("#ddl_emp").selectpicker('refresh');
        } else {
            $("#ddl_emp").html('');
            $('#ddl_emp').selectpicker();
            $("#ddl_emp").selectpicker('refresh');
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function fwd_details(sno) {
    curr_sno = sno;
    $("#fwdModal").modal('show');
}

function fwd() {


    objfwd.gri_sno = curr_sno;
    objfwd.sent_by_emp = Cookies.get('empid');
    objfwd.received_by_emp = $("#ddl_emp").val();
    objfwd.remarks = $("#txtnotes").val();

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+"/SendGrievanceToEmp",
        "method": "POST",
        "data": JSON.stringify(objfwd),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage);
            $("#fwdModal").modal('hide');
            loadTable();
        } else {

            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });

}

function category() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+"/Category_Get",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].sno + "'>" + response.data[index].category_name + "</option>"
            }
            $("#txt_category").html(html);


        } else {
            $("#txt_category").html(html);
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function subcategory() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": mainurl+"/SubCategory_Get",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.responseCode == 1) {
            var html = "<option value='0'>Choose...</option>";
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].sno + "'>" + response.data[index].sub_category_name + "</option>"
            }
            $("#txt_subcategory").html(html);


        } else {
            $("#txt_subcategory").html(html);
            alert(response.responseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

