﻿var data;
var filepath;
var currid;
var obj_appr = {

    "ID":"",
    "ghS_ID": "",
    "rooM_Type": "",
    "guest_name": "",
    "designation": "",
    "mobile_no": "",
    "from_date": "",
    "to_date": "",
    "purpose_of_visit": "",
    "vechile_allotment": "",
    "reporting_time": "",
    "note": "",
    "room_no": "",
    "room_status": "",
    "v_no": "",
    "driver_id": "",
    "order": "",
    "id_proof_type": "",
    "id_proof": "",
    "adminnote": ""
}

$(document).ready(function () {

    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#GuAppid').addClass('active');

    Load_Request()
    loadGH_Names();
    load_GHRoomTypes();

    $('#from_date,#to_date').datetimepicker({
        "allowInputToggle": true,
        "showClose": true,
        "showClear": true,
        "showTodayButton": true,
        "format": "DD/MM/YYYY",
    });

    $('#txt_rptime').datetimepicker({
        "allowInputToggle": true,
        "showClose": true,
        "showClear": true,
        "showTodayButton": true,
        "format": "DD-MM-YYYY HH:mm:ss",
    });

    $("#fl_ipproof").change(function (e) {
        upload_file();
    })

    $("#btn_submit").click(function (e) {
        Approve();
    })
    $("#btn_submit_cancel").click(function (e) {
        cancel_guest_house();
    })
});



function upload_file() {

    var formData = new FormData();
    var file = $('#fl_ipproof')[0];
    formData.append('file', file.files[0]);


    $.ajax({
        url: '/SaveFile_GrievanceFile',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        },
        success: function (d) {
           // console.log(d)
            if (d.status == "Success") {
                filepath = d.filepath;
                $("#lblidproof").html(file.files[0].name);
            } else {
                filepath = "";
            }
        },
        error: function (data, textStatus, xhr) {
            if (data.status == 401) {
                window.location = "login";
            }
        }
    });
};



function Load_Request() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_RequestsList",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        if (response.responseCode == 1) {

            _n_plain_mes_1("<strong>alert!</strong>", response.ResponseMessage, "warning"); 
            // alert(response.ResponseMessage);
            data = response.data;

            $('#dataTable').DataTable({
                destroy: true,
                paging: true,
                bInfo: false,
                "ordering": true,
                "pageLength": 10,
                data: response.data,
                columns: [
                    { data: "guesT_NAME" },
                    { data: "dates" },
                    { data: "ghS_NAME" },
                    { data: "rooM_TYPE" },
                    { data: "purposE_OF_VISIT" },
                    { data: "vechilE_ALLOTMENT_1" },
                    {data:"note"},
                    { data: "statuS1" },



                {
                    "data": "ghS_ID",
                    "type": "name",
                    "render": function (data, type, row, meta) {
                        if (row.status == "1") {
                            data = "<ul class='list-inline'><li class='list-inline-item'><a href='' class='edit' ghS_ID='" + row.ghS_ID + "' data-toggle='modal' data-target='#exampleModal'><i class='fa fa-check'></i></a></li><li class='list-inline-item'><a href='' class='cancel' ghS_ID='" + row.ghS_ID + "' data-toggle='modal' data-target='#Cancel_Request'><i class='fa fa-times'></i></a></li></ul>";
                        } else {
                            data = "";
                        }
                        return data;
                    }
                }
                ]
            });

            $('#dataTable').on('click', '.edit', function () {
                Guest_house_Approve($(this).attr('ghS_ID'));
            });


            $('#dataTable').on('click', '.cancel', function () {
                Guest_house_Cancel($(this).attr('ghS_ID'));
            });
        } else {
            _n_plain_mes_1("<strong>alert!</strong>", response.ResponseMessage, "warning"); 
            //alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function loadGH_Names() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=1",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].ghS_ID + "'>" + response.data[index].ghS_NAME + "</option>"
            }
            $("#ddl_ghnames").html(html);
        } else {
            $("#ddl_ghnames").html(html);
           // alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}

function load_GHRoomTypes() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Master?type=3",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        var html = "<option value='0'>Choose...</option>";
        if (response.responseCode == 1) {
            for (var index = 0; index < response.data.length; index++) {
                html += "<option value='" + response.data[index].rooM_ID + "'>" + response.data[index].floors + "</option>"
            }
            $("#ddl_rmtype").html(html);
        } else {
            $("#ddl_rmtype").html(html);
          //  alert(response.ResponseMessage);
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}


function Guest_house_Approve(id) {
    currid = id;
    //alert(id)
    $("#exampleModal").modal('show');
    var obj;
    for (var index = 0; index < data.length; index++) {
        if (data[index].ghS_ID == id) {
            obj = data[index];
        }
    }


    $("#ddl_ghnames").val(obj.id);
    $("#ddl_rmtype").val(obj.rooM_TYPE_ID);
    $("#txt_name").val(obj.guesT_NAME);
    $("#txt_desig").val(obj.designation);
    $("#txt_mobile").val(obj.mobilE_NO);
     $("#from_date").val(obj.froM_DATE);
     $("#to_date").val(obj.tO_DATE);
    $("#txt_pur_visit").val(obj.purposE_OF_VISIT);
    $("#txt_rptime").val(obj.reportinG_TIME);
    $("#ddl_vel_all").val(obj.vechilE_ALLOTMENT);
    $("#txt_note").val(obj.note);


}

function cancel_guest_house() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Request_Cancel?id=" + currid + "&reason=" + $("#txt_rej_reason").val(),
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",

        }
    }

    $.ajax(settings).done(function (response) {
      //  console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage);
            $("#Cancel_Request").modal('hide');
            window.location.reload();
        } else {
          //  alert(response.responseMessage);
            _n_plain_mes_1("<strong>alert!</strong>", response.responseMessage, "warning"); 
        }

    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}
function Guest_house_Cancel(id) {
    currid = id;
    $("#Cancel_Request").modal('show');
}

function Approve() {
    obj_appr.ID = currid;
    obj_appr.ghS_ID = $("#ddl_ghnames").val();
    obj_appr.rooM_Type = $("#ddl_rmtype").val();
    obj_appr.guest_name = $("#txt_name").val();
    obj_appr.designation = $("#txt_desig").val();
    obj_appr.mobile_no = $("#txt_mobile").val();
    obj_appr.from_date = $("#from_date").val();
    obj_appr.to_date = $("#to_date").val();
    obj_appr.purpose_of_visit = $("#txt_pur_visit").val();
    obj_appr.reporting_time = $("#txt_rptime").val();
    obj_appr.vechile_allotment = $("#ddl_vel_all").val();
    obj_appr.note = $("#txt_note").val();

    obj_appr.room_no=$("#txt_rm_no").val();
    obj_appr.room_status = $("#ddl_rm_status").val();
    obj_appr.v_no = $("#txt_vno").val();
    obj_appr.driver_id = $("#txt_dr_id").val();
    obj_appr.order = $("#ddl_order").val();
    obj_appr.id_proof_type = $("#txt_id_proof").val();
    obj_appr.id_proof = filepath;
    obj_appr.adminnote = $("#txt_adminnote").val();


    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/GuestHouse_Admin_approve",
        "method": "POST",
        "data": JSON.stringify(obj_appr),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json",
        }
    }

    $.ajax(settings).done(function (response) {
       // console.log(response);
        if (response.responseCode == 1) {
            alert(response.responseMessage);
            $("#exampleModal").modal('hide');
            Load_Request();
        } else {
           // alert(response.responseMessage);
            _n_plain_mes_1("<strong>alert!</strong>", response.responseMessage, "warning"); 
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            window.location = "login";
        }

    });
}