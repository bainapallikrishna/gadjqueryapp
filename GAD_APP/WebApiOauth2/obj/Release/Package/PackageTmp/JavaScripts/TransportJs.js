﻿$(document).ready(function () {
    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#vehiclestsid').addClass('active');
    loadVehicleCountsDetails();
    loadVehicleStatusDetails();

     var getdate = null;
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var todaydate = date.split('-');
    if (todaydate[1] != "10" && todaydate[1] != "11" && todaydate[1] != "12") {
        getdate = todaydate[2] + '-' + "0" + todaydate[1] + '-' + todaydate[0];
        if (todaydate[2] == "1" || todaydate[2] == "2" || todaydate[2] == "3" || todaydate[2] == "4" || todaydate[2] == "5" || todaydate[2] == "6" || todaydate[2] == "7" || todaydate[2] == "8" || todaydate[2] == "9") {
            getdate = "0"+ todaydate[2] + '-' + "0" + todaydate[1] + '-' + todaydate[0];
        }
        $('#txtdate').text(getdate);
    }
    if (todaydate[1] == "10" || todaydate[1] == "11" || todaydate[1] == "12") {
        getdate = todaydate[2] + '-'  + todaydate[1] + '-' + todaydate[0];
        if (todaydate[2] == "1" || todaydate[2] == "2" || todaydate[2] == "3" || todaydate[2] == "4" || todaydate[2] == "5" || todaydate[2] == "6" || todaydate[2] == "7" || todaydate[2] == "8" || todaydate[2] == "9") {
            getdate = "0" + todaydate[2] + '-' + todaydate[1] + '-' + todaydate[0];
        }
        $('#txtdate').text(getdate);
    }


});

var _Udata = {
    "vehicleno": "",
    "vehiclemodel": "",
    "driver": "",
    "allotedname": "",
    "remark": "",
    "vehicletype": ""
}

function loadVehicleCountsDetails() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetTransportVehiclesCounts?type=1",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }
    $.ajax(settings).done(function (response) {
        if (response.status == "Success") {
            $('#Totalvehicle').text(response.total);
            $('#Operationvehicle').text(response.operationalvehicle);
            $('#Underrepair').text(response.under_repair);
            $('#Condemned').text(response.condemnation);
            $('#Tobecondemned').text(response.to_be_condemned);
        }
        else {
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });
}



function loadVehicleStatusDetails() {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetTransportStatusDetails",
        "method": "GET",
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache"
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            //  bdata = response.data;
            $('#dataTable1').dataTable().fnClearTable();
            var groupColumn = 5;
            var table = $('#dataTable1').DataTable({
                "dom": '<"dt-buttons"Bf><"clear">lirtp',
                 aLengthMenu: [
                    [100, 200, 300, 400, -1],
                    [100, 200, 300, 400, "All"]
                ],
                "order": [[groupColumn, 'DESC']],
                "displayLength": 100,
                "columnDefs": [
                     { "visible": false, "targets": groupColumn }
                ],
               
                destroy: true,
                "scrollY": "62vh",
                "scrollCollapse": true,
                //paging: true,
                //bInfo: true,
                "ordering": false,
                //"pageLength": 25,
                data: response.vehicleStatuslist,
                columns: [

                    { data: "vehicleno", "sWidth": "10%" },
                    { data: "vehiclemodel", "sWidth": "10%" },
                    { data: "driver" },
                    { data: "allotedname" },
                    { data: "remark" },
                    { data: "vehicletype" },
                    {
                        "mData": null,
                        "mRender": function (data, type, s) {
                            return '<ul class="list-inline"><li class="list-inline-item"><a href="javascript: void (0);" class="EditAttributes" Evehicleno="' + s["vehicleno"] + '" Evehiclemodel="' + s["vehiclemodel"] + '" Edriver="' + s["driver"] + '" Eallotedname="' + s["allotedname"] + '" Eremark="' + s["remark"] + '"><i class="far fa-edit" ></i></a></li></ul>'
                        }
                    }
                ],
                "bInfo": true,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="6" align="center" class="tbl-mdhdr">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                },
                "buttons": [
                   {

                       extend: 'pdfHtml5',
                       text: 'Download Pdf',
                       filename: 'Vehicles_Status',
                       title: 'Vehicles Status',
                       exportOptions: {
                           columns: [0, 1, 2, 3, 4, 5],
                           search: 'applied',
                           order: 'applied'
                       },
                       customize: function (doc) {

                           var now = new Date();
                           var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();

                           doc['header'] = (function (page, pages) {
                               return {
                                   columns: [
                                       {
                                           alignment: 'left',
                                           text: ['Created on: ', { text: jsDate.toString() }]
                                       },
                                       {
                                           alignment: 'right',
                                           text: ['page ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                       }
                                   ],
                                   margin: 20
                               }
                           });


                           var tblBody = doc.content[1].table.body;
                           for (var i = 0; i < tblBody[0].length; i++) {
                               tblBody[0][i].fillColor = '#FFFFFF';
                               tblBody[0][i].color = 'black';
                           }

                           var objLayout = {};
                           objLayout['hLineWidth'] = function (i) { return .5; };
                           objLayout['vLineWidth'] = function (i) { return .5; };
                           objLayout['hLineColor'] = function (i) { return '#aaa'; };
                           objLayout['vLineColor'] = function (i) { return '#aaa'; };
                           objLayout['paddingLeft'] = function (i) { return 4; };
                           objLayout['paddingRight'] = function (i) { return 4; };
                           doc.content[0].layout = objLayout;
                           doc.content[1].layout = 'Borders';

                       }
                       //exportOptions: {
                       //    modifier: {
                       //        page: 'current'
                       //    }
                       //}
                   }
                ]

            });
            $('#dataTable1 tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === groupColumn && currentOrder[1] === 'DESC') {
                    table.order([groupColumn, 'asc']).draw();
                }
                else {
                    table.order([groupColumn, 'DESC']).draw();
                }
            });
           

        } else {
            $('#dataTable1').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}

$('#dataTable1').on('click', '.EditAttributes', function () {
    UpdateVehicleStatus($(this).attr('Evehicleno'), $(this).attr('Evehiclemodel'), $(this).attr('Edriver'), $(this).attr('Eallotedname'), $(this).attr('Eremark'));
});

function UpdateVehicleStatus(Evehicleno, Evehiclemodel, Edriver, Eallotedname, Eremark) {

    $('#txtvehicleno').val(Evehicleno);
    $('#txtvehiclemodel').val(Evehiclemodel);
    if (Edriver == "--") {
        $('#txtdriver').val("");
    }
    if (Edriver != "--") {
        $('#txtdriver').val(Edriver);
    }
    if (Eallotedname == "--") {
        $('#txtallotement').val("");
    }
    if (Eallotedname != "--") {
        $('#txtallotement').val(Eallotedname);
    }
    if (Eremark == "--") {
        $('#txtremark').val("");
    }
    if (Eremark != "--") {
        $('#txtremark').val(Eremark);
    }
    $('#exampleModal').modal('show');

}

$('#vehicleedit').click(function () {
    _Udata.vehicleno = $('#txtvehicleno').val();
    _Udata.vehiclemodel = $('#txtvehiclemodel').val();
    _Udata.driver = $('#txtdriver').val();
    _Udata.allotedname = $('#txtallotement').val();
    _Udata.remark = $('#txtremark').val();
    _Udata.vehicletype = "";

    UpdateVehicledata(_Udata);
});



function UpdateVehicledata(details) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../UpdateVehiclestatusDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    };
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            $("#exampleModal").modal('hide');
            loadVehicleCountsDetails();
            loadVehicleStatusDetails();
            _n_plain_mes_1("<strong>success!</strong>", "Data Updated successfully", "success"); return;
        } else {
            $("#exampleModal").modal('hide');
            _n_plain_mes_1("<strong>fail!</strong>", "Data Updated fail..!", "warning"); return;
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }
    });
}