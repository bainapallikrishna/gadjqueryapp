﻿
var GetOBJ = {
    "year": "",
    "month": "",
    "date": ""
}


$(document).ready(function () {
    $('.nav-item').removeClass('active');
    $('#PROTOCOL').addClass('show');
    $('#STatguestid').addClass('active');

    var today = new Date();
    GetOBJ.year = today.getFullYear();
    GetOBJ.date = "0";
    GetOBJ.month = "0";
    loadGetStateGuestDetails(GetOBJ);
});

var _data = {

    "DateOfOrder": "",
    "Guests": "",
    "VisitPlaces": "",
    "Fromdate": "",
    "ToDate": "",
    "Remark": ""
}

var _Udata = {
    "id": "",
    "DateOfOrder": "",
    "Guests": "",
    "VisitPlaces": "",
    "Fromdate": "",
    "ToDate": "",
    "Remark": ""
}

function loadGetStateGuestDetails(details) {

    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../GetStateGuestsDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }

    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {
            //  bdata = response.data;
            $('#dataTable').dataTable().fnClearTable();
            $('#dataTable').DataTable({
                //aLengthMenu: [
                //    [100, 200, 300, 400, -1],
                //    [100, 200, 300, 400, "All"]
                //],
                destroy: true,
                paging: true,
                bInfo: true,
                "ordering": true,
                "pageLength": 25,
                data: response.stateGuestlists,
                columns: [

                    { data: "dateOfOrder" },
                    { data: "guests", "sWidth": "40%" },
                    { data: "visitPlaces", "sWidth": "20%" },
                    { data: "fromdate" },
                    { data: "toDate" },
                {
                    "mData": null,
                    "mRender": function (data, type, s) {
                        return '<ul class="list-inline"><li class="list-inline-item"><a href="javascript: void (0);" class="EditAttributes" Eid="' + s["id"] + '" EdateOfOrder="' + s["dateOfOrder"] + '" Eguests="' + s["guests"] + '" EvisitPlaces="' + s["visitPlaces"] + '" Efromdate="' + s["fromdate"] + '" EtoDate="' + s["toDate"] + '" Eremark="' + s["remark"] + '" ><i class="far fa-edit" ></i></a></li></ul>'
                    }
                }
                ]
            });

            //$('#dataTable').on('click', '.edit', function () {
            //    CabinetDetails_edit($(this).attr('sno'));
            //});
        } else {
            $('#dataTable').dataTable().fnClearTable();
            _n_plain_mes_1("<strong>no data found!</strong>", "", "danger");
        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}

$('#dataTable').on('click', '.EditAttributes', function () {
    UpdateGuests($(this).attr('Eid'), $(this).attr('EdateOfOrder'), $(this).attr('Eguests'), $(this).attr('EvisitPlaces'), $(this).attr('Efromdate'), $(this).attr('EtoDate'), $(this).attr('Eremark'));
});

function UpdateGuests(Eid, EdateOfOrder, Eguests, EvisitPlaces, Efromdate, EtoDate, Eremark) {


    sessionStorage.setItem("UId", Eid);
    $('#txtDATEofOrder').val(EdateOfOrder);
    $('#txtGuests').val(Eguests);
    $('#txtvisitpalce').val(EvisitPlaces);
    $('#txtFDate').val(Efromdate);
    $('#txtTDate').val(EtoDate);
    $('#txtremark').val(Eremark);
    $('#btnsubmit').hide();
    $('#btnUpdate').show();
    $('#addnewguest').modal('show');

}

function ADDNEW() {
    $('#txtDATEofOrder').val('');
    $('#txtGuests').val('');
    $('#txtvisitpalce').val('');
    $('#txtFDate').val('');
    $('#txtTDate').val('');
    $('#txtremark').val('');
    $('#btnsubmit').show();
    $('#btnUpdate').hide();
    $('#addnewguest').modal('show');
}

$('#btnsubmit').click(function () {
    if ($('#txtDATEofOrder').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select Date Of Order", "warning"); $("#txtDATEofOrder").focus(); return;
    }
    else if ($('#txtGuests').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Guests", "warning"); $("#txtGuests").focus(); return;
    }
    else if ($('#txtvisitpalce').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Visit Places", "warning"); $("#txtvisitpalce").focus(); return;
    }
    else if ($('#txtFDate').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select From Date", "warning"); $("#txtFDate").focus(); return;
    }
    else if ($('#txtTDate').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select To Date", "warning"); $("#txtTDate").focus(); return;
    }

    else {

        _data.DateOfOrder = $('#txtDATEofOrder').val();
        _data.Guests = $('#txtGuests').val();
        _data.VisitPlaces = $('#txtvisitpalce').val();
        _data.Fromdate = $('#txtFDate').val();
        _data.ToDate = $('#txtTDate').val();
        _data.Remark = $('#txtremark').val();
        InsertGuestData(_data);
    }

});

$('#btnUpdate').click(function () {
    if ($('#txtDATEofOrder').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select Date Of Order", "warning"); $("#txtDATEofOrder").focus(); return;
    }
    else if ($('#txtGuests').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Guests", "warning"); $("#txtGuests").focus(); return;
    }
    else if ($('#txtvisitpalce').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Enter Visit Places", "warning"); $("#txtvisitpalce").focus(); return;
    }
    else if ($('#txtFDate').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select From Date", "warning"); $("#txtFDate").focus(); return;
    }
    else if ($('#txtTDate').val() == "") {
        _n_plain_mes_1("<strong>alert!</strong>", "please Select To Date", "warning"); $("#txtTDate").focus(); return;
    }
    else {
        _Udata.id = sessionStorage.getItem("UId");
        _Udata.DateOfOrder = $('#txtDATEofOrder').val();
        _Udata.Guests = $('#txtGuests').val();
        _Udata.VisitPlaces = $('#txtvisitpalce').val();
        _Udata.Fromdate = $('#txtFDate').val();
        _Udata.ToDate = $('#txtTDate').val();
        _Udata.Remark = $('#txtremark').val();
        UpdateGuestData(_Udata);
    }

});


function InsertGuestData(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../InsertStateGuestsDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#addnewguest").modal('hide');
            GetOBJ.year = "0";
            GetOBJ.month = "0";
            GetOBJ.date = response.fromDate;
            loadGetStateGuestDetails(GetOBJ);
            _n_plain_mes_1("<strong>success!</strong>", "Data Insertion successfully", "success"); return;
        } else {
            $("#addnewguest").modal('hide');
            _n_plain_mes_1("<strong>fail!</strong>", "Data Insertion fail..!", "warning"); return;

        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}


function UpdateGuestData(details) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "../UpdateStateGuestsDetails",
        "method": "POST",
        "data": JSON.stringify(details),
        "headers": {
            "Authorization": "bearer " + Cookies.get('token'),
            "Cache-Control": "no-cache",
            "Content-Type": "application/json"
        }
    }
    $.ajax(settings).done(function (response) {

        if (response.status == "Success") {

            $("#addnewguest").modal('hide');
            GetOBJ.year = "0";
            GetOBJ.month = "0";
            GetOBJ.date = response.fromDate;
            loadGetStateGuestDetails(GetOBJ);
            _n_plain_mes_1("<strong>success!</strong>", "Data Updated successfully", "success"); return;
        } else {
            $("#addnewguest").modal('hide');
            _n_plain_mes_1("<strong>fail!</strong>", "Data Updated fail..!", "warning"); return;

        }
    }).fail(function (data, textStatus, xhr) {
        if (data.status == 401) {
            _n_plain_mes_1("<strong>Session Expire!</strong>", "please re-Login session expire", "danger");
            window.location = "Login";
        }

    });

}

