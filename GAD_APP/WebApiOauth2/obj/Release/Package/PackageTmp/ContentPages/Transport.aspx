﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Transport.aspx.cs" Inherits="WebApiOauth2.ContentPages.VehicleStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .buttons-pdf {
            background-color: blue;
            color: white;
        }
    </style>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Protocol Transport</h5>
    </div>

    <div class="row">
       
        <div class="col-xl-2 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Vehicles </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="Totalvehicle"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Operational Vehicles </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800" id="Operationvehicle"></div>
                                </div>
                                <!--  <div class="col">
                            <div class="progress progress-sm mr-2">
                              <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div> -->
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-2 col-md-6 mb-4">
            <div class="card border-left-default shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-default text-uppercase mb-1">Under Repair </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="Underrepair"> </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-2 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Condemned </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="Condemned"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">To Be Condemned </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="Tobecondemned"></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="tbl-hdr">
                    <div class="row">
                        <div class="col-sm-auto "><strong>Date: </strong></div>
                        <div class="col-sm-2">
                            <strong id="txtdate"></strong>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm" id="dataTable1" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Vehicle No </th>
                                    <th>Make / Year </th>
                                    <th>Maintained by  driver</th>
                                    <th>To whom Alloted</th>
                                    <th>Remarks </th>
                                    <th>Vehicle type </th>
                                    <th>Actions </th>

                                </tr>
                            </thead>

                            <tbody>
                                <%--<tr>
                                    <td>1 </td>
                                    <td>qwe</td>
                                    <td>qw</td>

                                    <td>1 </td>
                                    <td>1 </td>
                                    <td>1 </td>


                                    <td>
                                        <ul class="list-inline">
                                            <li class="list-inline-item"><a href="" data-toggle="modal" data-target="#exampleModal"><i class="far fa-edit"></i></a></li>
                                            <li class="list-inline-item">
                                                <a href=""><i class="far fa-trash-alt"></i></a>
                                            </li>
                                        </ul>
                                    </td>

                                </tr>--%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Vehicle No </label>
                                <input type="text" class="form-control" id="txtvehicleno" readonly disabled>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Make / Year  :</label>
                                <input type="text" class="form-control" id="txtvehiclemodel" readonly disabled>
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="col-form-label">Maintained by  driver :</label>
                                <input type="text" class="form-control" id="txtdriver">
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">To whom Alloted :</label>
                                <input type="text" class="form-control" id="txtallotement">
                            </div>
                            <%--   <div class="col-md-6">
                                <label class="col-form-label">Date of Allotment   :</label>
                                <input type="text" class="form-control date" id="txtDOAllot">
                            </div>--%>
                            <div class="col-md-6">
                                <label class="col-form-label">Remarks :</label>
                                <input type="text" class="form-control" id="txtremark">
                            </div>
                            <!--<div class="col-md-6">
                                  <label  class="col-form-label">  Date of Departure   :</label>
                                  <input type="text" class="form-control" >
                                </div>

                                <div class="col-md-6">
                                  <label  class="col-form-label"> Category   :</label>
                                  <input type="text" class="form-control" >
                                </div>

                                <div class="col-md-6">
                                  <label  class="col-form-label"> Remarks   :</label>
                                  <input type="text" class="form-control" >
                                </div>-->

                        </div>

                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-success btn-sm" id="vehicleedit">SAVE</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/TransportJs.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>

