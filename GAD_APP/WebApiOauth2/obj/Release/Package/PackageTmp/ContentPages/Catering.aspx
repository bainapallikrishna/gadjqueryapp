﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Catering.aspx.cs" Inherits="WebApiOauth2.ContentPages.Catering" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-2">
        <h5 class="page-heading mb-0 text-gray-800">Catering </h5>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="form-row">
                        <div class="col-md-6">

                            <%--<div class="form-group ">
                                <label for=" ">Food type </label>
                                <select id="ddl_foodtype" class="form-control">
                                    <option selected="selected">Choose...</option>
                                    <option>Food</option>
                                    <option>Snacks</option>
                                    <option>Drinks</option>
                                </select>
                            </div>--%>
                            <div class="form-group">
                                <label>VEG Meals </label>
                                <input type="text" id="txt_meals" class="form-control" />
                            </div>

                            <div class="form-group">
                                <label>Non-VEG Meals </label>
                                <input type="text" class="form-control" id="txt_non_meals" />
                            </div>

                            <div class="form-group">
                                <label>Snacks  </label>
                                <input type="text" class="form-control" id="txt_snacks" />
                            </div>
                            <div class="form-group">
                                <label>Drinks </label>
                                <input type="text" class="form-control" id="txt_drink" />
                            </div>

                            <div class="form-group">
                                <label>Tea </label>
                                <input type="text" class="form-control" id="txt_tea" />
                            </div>


                            <div class="form-group">
                                <label>Coffee </label>
                                <input type="text" class="form-control" id="txt_coffee" />
                            </div>

                            <div class="form-group">
                                <label>Others </label>
                                <input type="text" class="form-control" id="txt_others" />
                            </div>

                            <%--<div class="form-group row">
                                <div class="col-md-4"></div>
                                <div class="col-md-2">
                                    <button type="button" id="btn_add" class="btn btn-success btn-block"> Add</button>
                                </div>
                            </div>--%>
                        </div>

                        <div class="col-md-6">


                            <div class="form-group">
                                <label>Contact Person Name </label>
                                <input type="text" class="form-control" id="txt_name" />
                            </div>

                            <div class="form-group">
                                <label>Contact Person Mobile Number </label>
                                <input type="text" class="form-control" id="txt_mobile" />
                            </div>


                            <div class="form-group ">
                                <label for="">Purpose of Meeting</label>
                                <select id="ddl_meeting" class="form-control">
                                    <option selected="selected">Choose...</option>
                                    <option>Meeting </option>
                                    <option>Video conference  </option>
                                    <option>CS Video conference  </option>
                                    <option>Others  </option>
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="" id="ddl_label_others">Purpose of Meeting Others</label>
                                <input type="text" class="form-control" id="ddl_meeting_others" />
                            </div>
                            <div class="form-group">
                                <label>Meeting Date </label>
                                <input type="text" class="form-control" id="txt_date" />
                            </div>
                            <div class="form-group">
                                <label>Meeting Time </label>
                                <input type="text" class="form-control" id="txt_time" />
                            </div>

                            <div class="form-group">
                                <label>Venue </label>
                                <input type="text" class="form-control" id="txt_venue" />
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3">
                                    <button type="button" id="btn_add" class="btn btn-success btn-block">Submit </button>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
      <div class="card shadow mb-4">
                <div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Contact Person Name </th>
                    <th>Mobile Number</th>
                    <th>Purpose of Meeting </th>
                    <th>Meeting Date</th>
                    <th>Metting Time</th>
                    <th>Venue</th>
                    <th>Items</th>
                </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
    </div>

</div>
          </div>
    <div class="modal fade" id="itemsdetails" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Items </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable_modal" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Item Quantity</th>
                                    
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="button" id="btn_submit" class="btn btn-success btn-sm">SAVE</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/Catering.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>

