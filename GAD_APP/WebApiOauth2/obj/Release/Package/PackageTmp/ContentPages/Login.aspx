﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApiOauth2.ContentPages.Login" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GAD  </title>

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="../css/style.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="../css/admin.css" rel="stylesheet">
</head>

<body class="bg-gradient" style="background: #f3f3f3;">
    <section class="login-hdr shadow">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <img src="../img/gad.png" alt="LOGO">
                </div>
                <div class="col-lg-3">
                    <div class="text-right mt-2">
                        <a href="https://www.gad.ap.gov.in" target="_blank">
                            <img src="../img/gadhome.png" alt="LOGO">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body wd-card shadow-login">
                        <div class="row">
                            <div class="col-lg-1">
                                <img src="../img/hrms.png" alt="LOGO">
                            </div>
                            <div class="col-lg-11">
                                <div class="wd-sec">
                                    <h5 class="wd-tilte">HRMS </h5>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">Officers  </li>
                                        <li class="list-inline-item">Postings & Vacancies  </li>
                                        <li class="list-inline-item">Leaves </li>
                                        <li class="list-inline-item">ACR </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-1">
                                <img src="../img/finance.png" alt="LOGO">
                            </div>
                            <div class="col-lg-11">
                                <div class="wd-sec">
                                    <h5 class="wd-tilte">FINANCE </h5>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">Budget  </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-1">
                                <img src="../img/material.png" alt="LOGO">
                            </div>
                            <div class="col-lg-11">
                                <div class="wd-sec">
                                    <h5 class="wd-tilte">MATERIAL </h5>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">Inventory  </li>

                                    </ul>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-lg-1">
                                <img src="../img/general.png" alt="LOGO">
                            </div>
                            <div class="col-lg-11">
                                <div class="wd-sec">
                                    <h5 class="wd-tilte">GENERAL </h5>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">Secretariat accommodation </li>
                                        <li class="list-inline-item">Secretariat attendance  </li>
                                        <li class="list-inline-item">e-Office </li>
                                    </ul>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">Cabinet decisions    </li>
                                        <li class="list-inline-item">VIP diary schedule     </li>
                                        <li class="list-inline-item">Grievance   </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-1">
                                <img src="../img/protocol.png" alt="LOGO">
                            </div>
                            <div class="col-lg-11">
                                <div class="wd-sec">
                                    <h5 class="wd-tilte">PROTOCOL </h5>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">Catering        </li>
                                        <li class="list-inline-item">VIP Visits  </li>
                                        <li class="list-inline-item">Guest House </li>
                                        <li class="list-inline-item">Transport </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="login-card">

                    <div class="card o-hidden border-0 shadow-login">
                        <div class="card-body">
                            <div>
                                <div class="login-title">
                                    Sign In 
                                </div>
                                <form class="user">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control"
                                            placeholder="Enter your Files Code" id="txtusername">
                                    </div>

                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" class="form-control"
                                            placeholder="Enter your Password" id="txtpwd">
                                    </div>

                                    <%--  <div class="form-group">
                                        <label>Name  </label>
                                        <input type="text" class="form-control "
                                            placeholder="Name" readonly>
                                    </div>--%>
                                    
                                    <div class="form-group">

                                    <a href="#" id="btn_login" class="btn btn-success btn-rounded mt-3">Sign In
                    </a>
                                        </div>
                                 

                                    <div class="form-group">
                                        <a href="javascript:void(0);" id="btn_forget">Forget Password
                    </a>
                                    </div>


                                </form>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="login-ftr">
        <div class="container">
            <div class="row">
                <div class="col-lg-1">
                    <img src="../img/ftrlogo.png" alt="LOGO">
                </div>
                <div class="col-lg-11">


                    <ul class="list-inline mt-2">
                        <li class="list-inline-item"><a href="javascript: void (0);">Contact  </a></li>
                        |
           
                        <li class="list-inline-item"><a href="javascript: void (0);">Feedback   </a></li>
                        |
           
                        <li class="list-inline-item"><a href="javascript: void (0);">Copyright  </a></li>
                        |
           
                        <li class="list-inline-item"><a href="javascript: void (0);">Hyperlinking  </a></li>
                        |
           
                        <li class="list-inline-item"><a href="javascript: void (0);">Terms and Conditions  </a></li>
                        |
           
                        <li class="list-inline-item"><a href="javascript: void (0);">Privacy Policy   </a></li>
                    </ul>
                    <div class="mt-0">© 2019 ఆంధ్రప్రదేశ్ సాధారణ పరిపాలన శాఖ.</div>
                </div>
            </div>
        </div>
    </section>






    <!-- Bootstrap core JavaScript-->
    <script src="../vendor/jquery/jquery.min.js"></script>
     <script src="../js/thirdparty.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>
    <script src="../js/js.cookie.min.js"></script>
    <script src="../JavaScripts/aes.js"></script>
    <script src="../JavaScripts/Login.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>

</body>

</html>
