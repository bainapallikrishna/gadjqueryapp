﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="POL_Indent_Report.aspx.cs" Inherits="WebApiOauth2.ContentPages.POL_Indent_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .buttons-pdf {
            background-color: blue;
            color: white;
        }
    </style>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">POL Indent_report</h5>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="form-row">

                        <div class="col-sm-3">
                            <div class="form-group ">
                                <label for="">Year</label>
                                <input class="form-control date" id="ddll_yyear" />
                                <%-- <select id="ddl_year" class="form-control">
                                    </select>--%>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group ">
                                <label for="">Month</label>
                                <input class="form-control date" id="ddll_mmonth" />
                                <%-- <select id="ddl_month" class="form-control">
                                    </select>--%>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-stripped" id="dataTable">
                            <thead>
                                <tr>

                                    <th>Vehicle No </th>

                                    <th>Officer Using the vehicle </th>

                                    <th>Consumption of petrol </th>
                                    <th>Consumption of Diesel  </th>
                                    <th>Engine / Coolant Oil </th>
                                    <th>Ceiling limit fixed  </th>
                                    <th>Excess consumption  </th>
                                    <th>Amount in Rs. </th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot align="left">
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                        </table>
                    </div>





                </div>
            </div>
        </div>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/POL_Indent_Report.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
