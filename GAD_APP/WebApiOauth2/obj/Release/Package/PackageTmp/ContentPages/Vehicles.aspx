﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Vehicles.aspx.cs" Inherits="WebApiOauth2.ContentPages.Vehicles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h5 class="page-heading mb-0 text-gray-800">Vehicles </h5>
        </div>



        <div class="row  justify-content-center">
            <div class="col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>

                                        <th>Description of the Govt. Vehicle with Vehicle Number </th>
                                        <th>Vehicle Model  </th>
                                        <th>Petrol / Diesel</th>
                                        <th>GAD Vehicle / Other Dept’s Vehicle </th>
                                        <th>Designation of the User</th>
                                        <th>Ceiling Limit</th>
                                        <th>Name & Mobile No. of the Outsourcing  Drivers</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <!-- /.container-fluid -->

    </div>
    <div class="modal fade" id="addnew_vehicledetails" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">POL Indent</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Expenditure</a>
                        </li>


                    </ul>
                    <%-- <h5 class="modal-title" id="exampleModalLabel">POL Indent </h5>--%>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <form id="vehicles_modal" data-parsley-validate="parsley">

                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="">Vehicle No </label>
                                            <input type="text" id="txt_vehicleno" class="form-control" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label>Name of Petrol bunk</label>
                                            <input type="text" id="txt_ptrlbunk_name" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Name Of Petrol Bunk">
                                        </div>
                                        <div class="form-group">
                                            <label>Date  </label>
                                            <div class="form-group">
                                                <div class="input-group date" id="">
                                                    <input type="text" id="txt_date" class="form-control form-control-sm" />
                                                    <div class="input-group-addon input-group-append">
                                                        <div class="input-group-text">
                                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">

                                            <label>Coupon No</label>
                                            <input type="text" id="txt_couponno" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Coupon Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>POL Type  </label>
                                            <select id="ddl_poltype" class="form-control form-control" data-parsley-required="true" data-parsley-error-message="Please Select POL Type">
                                                <option value="">--Choose--</option>
                                                <option value="1">PETROL</option>
                                                <option value="2">DIESEL</option>
                                                <option value="3">ENGINE OIL</option>
                                                <option value="4">COOLANT OIL</option>
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label>Quantity </label>
                                            <input type="text" id="txt_quantity" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Qunatity" placeholder="0">
                                        </div>
                                        <div class="form-group">
                                            <label>Price(per litre in Rupees)  </label>
                                            <input type="text" id="txt_price" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Price Per ltr" placeholder="0">
                                        </div>
                                        <div class="form-group">
                                            <label>Total Cost(in rupees)   </label>
                                            <input type="text" id="txt_totalcost" class="form-control" readonly="readonly" placeholder="0">
                                        </div>


                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" id="btn_save" class="btn btn-success btn-sm">SAVE</button>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <form id="expenditure_modal" data-parsley-validate="parsley">

                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="">Vehicle No </label>
                                        <input type="text" id="txt_vehicleno1" class="form-control" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label>Date  </label>
                                        <div class="form-group">
                                            <div class="input-group date" id="">
                                                <input type="text" id="txt_date1" class="form-control form-control-sm" />
                                                <div class="input-group-addon input-group-append">
                                                    <div class="input-group-text">
                                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Replaced/Servicing:</label>
                                        <input type="text" id="txt_replace_service1" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Name Of Petrol Bunk">
                                    </div>



                                    <div class="form-group">

                                        <label>Name of the garage:</label>
                                        <input type="text" id="txt_garage_name1" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Coupon Number">
                                    </div>
                                    <div class="form-group">

                                        <label>Amount:</label>
                                        <input type="text" id="txt_amount1" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Coupon Number">
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" id="btn_save1" class="btn btn-success btn-sm">SAVE</button>
                                </div>
                            </form>
                        </div>


                    </div>


                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="Update_vehicledetails" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Edit Vehicle Details</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="vehicles_modal_edit" data-parsley-validate="parsley">
                        <%-- <div class="form-row">--%>

                        <div class="form-group ">
                            <label for="">Vehicle No </label>
                            <input type="text" id="txt_vehicleno_edit" class="form-control" readonly="readonly">
                        </div>



                        <div class="form-group">

                            <label>GAD Vehicle / Other Dept’s Vehicle </label>
                            <input type="text" id="txt_dept_edit" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Department Name">
                        </div>



                        <div class="form-group">
                            <label>Designation of the User </label>
                            <input type="text" id="txt_designation_edit" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Designation Of The User">
                        </div>
                        <div class="form-group">
                            <label>Ceiling Limit</label>
                            <input type="text" id="txt_ceilinglimit" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Ceiling Limit">
                        </div>

                        <div class="form-group">
                            <label>Name of the Outsourcing  Drivers</label>
                            <input type="text" id="txt_name_edit" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Name of the Outsourcing  Drivers">
                        </div>
                        <div class="form-group">
                            <label>Mobile No. </label>
                            <input type="text" id="txt_mobile_edit" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Mobile No. of the Outsourcing  Drivers" onkeypress='validate(event)' maxlength="10">
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <input type="text" id="txt_reamrks_edit" class="form-control" data-parsley-required="true" data-parsley-error-message="Enter Remarks">
                        </div>


                        <%--</div>--%>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" id="btn_update" class="btn btn-success btn-sm">Update</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/Vehicles.js"></script>


</asp:Content>
