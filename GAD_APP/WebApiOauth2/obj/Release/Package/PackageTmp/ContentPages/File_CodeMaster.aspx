﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="File_CodeMaster.aspx.cs" Inherits="WebApiOauth2.ContentPages.File_CodeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">File Code Mapping Form </h5>
    </div>


    <div class="row  justify-content-center">
        <div class="col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-body">


                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Department </label>
                        <div class="col-sm-10">
                            <select id="ddl_dept" data-live-search="true" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Employee</label>
                        <div class="col-sm-10">
                            <select id="ddl_emp"  data-live-search="true" class="form-control">
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Unit </label>
                        <div class="col-sm-10">
                            <select id="ddl_unit"  class="form-control">
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">FileCodes</label>
                        <div class="col-sm-10">
                            <select id="ddl_filecodes" data-live-search="true" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Is Peshi</label>
                        <div class="col-sm-10">
                            <select id="ddl_Peshi" data-live-search="true" class="form-control">
                                <option value="">Select</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>





                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <button type="button" id="btn_submit" class="btn btn-success btn-sm">Map </button>
                            <button type="button" id="btn_update" class="btn btn-success btn-sm">Update </button>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>First Name </th>
                                    <th>Last Name</th>
                                    <th>Designation</th>
                                    <th>Unit Name</th>
                                    <th>File Code</th>
                                    <th>Actions </th>

                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/FileCodeMaster.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
