﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="WebApiOauth2.ContentPages.Inventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Inventory </h5>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="tbl-hdr">
                    <div class="form-group row">
                        <div class="col-md-10">Create New Items </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-success btn-block btn-sm" id="btnaddvendor">Add New  </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm" id="dataTable1">
                            <thead>
                                <tr>
                                    <th rowspan="2">S.NO</th>
                                    <th rowspan="2">Item category </th>
                                    <th rowspan="2">Item Name</th>
                                    <th colspan="3" style="text-align: center"><span id="VENDOR1"></span> </th>
                                    <th colspan="3" style="text-align: center"><span id="VENDOR2"></span></th>
                                    <th rowspan="2">Actions </th>
                                </tr>
                                <tr>
                                  <%--  <th>VendorName</th>--%>
                                    <th>Maker </th>
                                    <th>Price  </th>
                                    <th>Status   </th>
                                    <%--<th>VendorName</th>--%>
                                    <th>Maker </th>
                                    <th>Price  </th>
                                    <th>Status   </th>
                                </tr>

                            </thead>

                            <tbody>
                                <%--<tr>
                      <td> 155 </td>
                      <td>sefse rwr</td>
                      <td>werwrr</td>
                       <td> werwerewrewr </td>
                        <td>werwrr</td>
                       <td> werwerewrewr </td>
                        <td> 11 </td>
                        <td> 55</td>
                        <td> 55</td>
                        <td> <ul class="list-inline">
                        <li class="list-inline-item"> <a href="#" data-toggle="modal" data-target="#exampleModal">   <i class="far fa-edit"></i>   </a>   </li> 
                        <li class="list-inline-item">
                           <i class="far fa-trash-alt"></i>   </li> 

                      </ul> </td>
                      </tr>--%>
                            </tbody>
                        </table>

                    </div>
                    



                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-row">
                            <div class="col-md-6">

                                <div class="form-group ">
                                    <label for=" ">Item category </label>
                                    <select id="ddlCategory" class="form-control">
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label>Item  </label>
                                    <input type="text" class="form-control" id="txtitem">
                                </div>
                                <div class="form-group">
                                    <label>Vendor1 Price  </label>
                                    <input type="text" class="form-control" id="txtVendor1Price" onkeypress='validate(event)'>
                                </div>
                                
                                <div class="form-group ">
                                    <label for=" ">Vendor2 </label>
                                   <input type="text" class="form-control" id="txtvendor2">
                                </div>
                                 <div class="form-group">
                                    <label>Vendor2 Price  </label>
                                    <input type="text" class="form-control" id="txtVendor2Price" onkeypress='validate(event)'>
                                </div>
                              
                            </div>

                            <div class="col-md-6">

                                <div class="form-group ">
                                    <label for=" ">Vendor1 </label>
                                   <input type="text" class="form-control" id="txtvendor1">
                                </div>
                                <div class="form-group">
                                    <label>Vendor1 Maker  </label>
                                    <input type="text" class="form-control" id="txtVendor1Maker">
                                </div>
                                <div class="form-group">
                                    <label>Vendor1 Status </label>
                                    <input type="text" class="form-control" id="txtVendor1Status">
                                </div>
                                 <div class="form-group">
                                    <label>Vendor2 Maker   </label>
                                    <input type="text" class="form-control" id="txtVendor2Maker">
                                </div>
                                 <div class="form-group">
                                    <label>Vendor2 Status </label>
                                    <input type="text" class="form-control" id="txtVendor2Status">
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm" id="btnvendorSave">Add </button>
                    <button type="button" class="btn btn-success btn-sm" id="btnvendorUpdate">Update </button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/InventoryJs.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
