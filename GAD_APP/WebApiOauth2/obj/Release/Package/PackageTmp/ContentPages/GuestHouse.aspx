﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="GuestHouse.aspx.cs" Inherits="WebApiOauth2.ContentPages.GuestHouse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Room allotment</h5>
    </div>
         <div class="card mb-4 shadow-sm">
     <div class="tbl-hdr">
    <div class="row">



        <div class="col-lg-auto">
            <label>Date </label>
            </div>
             <div class="col-lg-3">
                <div class="input-group date">
                    <input type="text" id="txt_search_date" class="form-control form-control-sm" required="" />
                    <div class="input-group-addon input-group-append">
                        <div class="input-group-text">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
        



        <div class="col-lg-2">
             
            <div>
                <button type="button" id="btn_search" class="btn btn-primary btn-sm">Search </button>
            </div>
        </div>

         <div class="col-lg-6">
             
            <div>
             <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Add New </button>
            </div>
        </div>
    </div>
         </div>
       <div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered table-sm " id="dt_ghTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>S.No </th>
                    <th>Guest House Name</th>
                    <th>Type Of Room </th>
                    <th>Room No </th>
                    <th>Name & Designation of the Guest </th>
                    <th>Date of Arrival </th>
                    <th>Date of Departure </th>
                    <th>Category  </th>
                    <th>Floor</th>
                    <th>Remarks </th>
                    <th>Actions </th>

                </tr>
            </thead>

            <tbody>
            </tbody>
            <%--<tbody>
                    <tr>
                      <td> 1 </td>
                      <td>qwe</td>
                      <td>qw</td>
                       <td> wer </td>
                        <td> 1 </td>
                        
                         
                            <td> 1 </td>
                            <td> 1 </td>
                            <td> 1 </td>
                      <td> ef</td>
                      <td> <ul class="list-inline">
                        <li class="list-inline-item">  <a href="" data-toggle="modal" data-target="#exampleModal">  <i class="far fa-edit" ></i>   </a>   </li> 
                        <li class="list-inline-item">
                           <a href=""> <i class="far fa-trash-alt"></i>  </a>   </li> 

                      </ul> </td>
                      
                    </tr>

                  </tbody>--%>
        </table>
    </div>
             </div>
             </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>

                        <div class="form-group row">

                            <div class="col-md-6">
                                <label class="col-form-label">Guest Name     :</label>
                                <select id="ddl_ghnames" class="form-control form-control-sm">
                                </select>
                            </div>


                            <div class="col-md-6">
                                <label class="col-form-label">Room Type     :</label>
                                <select id="ddl_rmtype" class="form-control form-control-sm">
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Floor :</label>
                                <select id="ddl_rmfl" class="form-control form-control-sm">
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Room No   :</label>
                              
                                 <select id="txt_rm" class="form-control form-control-sm">
                                </select>
                                  <%--<input type="text" id="" class="form-control" />--%>
                            </div>

                        </div>



                        <div class="form-group row">



                            <div class="col-md-6">
                                <label class="col-form-label">Name & Designation of the Guest :</label>
                                <input type="text" id="txt_name_degi" class="form-control" />
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Date of Arrival  :</label>
                                <input type="text" id="txt_arr_date" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Date of Departure   :</label>
                                <input type="text" id="txt_dept_date" class="form-control" />
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Category   :</label>
                                <select id="ddl_cat" class="form-control form-control-sm">
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label class="col-form-label">Remarks   :</label>
                                <input type="text" id="txt_remarks" class="form-control" />
                            </div>


                        </div>

                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" id="btn_submit" class="btn btn-success btn-sm">SAVE</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/GuestHouseRequest.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
