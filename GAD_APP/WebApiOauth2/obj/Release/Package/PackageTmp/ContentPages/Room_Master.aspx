﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="Room_Master.aspx.cs" Inherits="WebApiOauth2.ContentPages.Room_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h5 class="page-heading mb-0 text-gray-800">Room Master</h5>
    </div>


    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-body">


                    <div class="form-group ">
                        <label for="">Guest House  Name</label>
                        <select id="ddl_ghnames" class="form-control">
                        </select>
                    </div>


                    <div class="form-group ">
                        <label for="">Room Type</label>
                        <select id="ddl_rmtype" class="form-control">
                        </select>
                    </div>



                    <div class="form-group ">
                        <label for="">Room Floor</label>
                        <select id="ddl_rmfloor" class="form-control">
                        </select>
                    </div>

                    <div class="form-group ">
                        <label for=" ">Room Number</label>
                        <input type="text" id="txt_rmnumber" class="form-control" />
                    </div>
                    <%--<div class="form-group " id="div_ddl_Status">
                        <label for="">Room Category</label>
                        <select id="ddl_Status" class="form-control">
                        </select>
                    </div>--%>
                    <div class="form-group ">
                        <label for=" ">ROOM AMENITIES</label>
                        <input type="text" id="txt_amts" class="form-control" />
                    </div>


                    <div class="form-group row">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <button type="button" id="btn_submit" class="btn btn-success btn-block">Submit </button>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="form-group row">
                        <div class="col-md-9"></div>
                        <%--  <div class="col-md-3">
                                    <button type="button" id="btn_submit" class="btn btn-success btn-block">Submit </button>
                                </div>
                            </div>--%>
                    </div>
                </div>

            </div>

        </div>
    </div>



    <div class="row  justify-content-center">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Guest House </th>
                                    <th>Room Number</th>
                                    <th>Room Type </th>
                                    <th>Room Floor</th>
                                    <th>Room Amenities</th>
                                  <%--  <th>Room Category </th>--%>
                                    <th>Actions </th>

                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script src="../JavaScripts/Room_Master.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
</asp:Content>
