﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Dashboard.Master" AutoEventWireup="true" CodeBehind="StateGuests.aspx.cs" Inherits="WebApiOauth2.ContentPages.StateGuests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                    <div class="tbl-hdr">
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success btn-sm" onclick="ADDNEW();" style="float: right">Add New </button>
                            </div>
                        </div>
                    </div>
                <div class="card-body">
                
                      <div class="table-responsive">
                    <table id="dataTable" class="table table-bordered table-sm table-stripped">

                        <thead>
                            <tr>
                                <th>Date Of Order</th>
                                <th>Guests </th>
                                <th>Visit Places </th>
                                <th>From  Date </th>
                                <th>To Date </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%-- <tr>
                        <td>data</td>
                           <td>data</td>
                              <td>data</td>
                                 <td>data</td>
                                 <td>data</td>
                                 <td> <ul class="list-inline">
                        <li class="list-inline-item">  <a href="" data-toggle="modal" data-target="#addnewguest">  <i class="far fa-edit" ></i>   </a>   </li> 
                        <li class="list-inline-item">
                           <a href=""> <i class="far fa-trash-alt"></i>  </a>   </li> 

                      </ul> </td>
                      </tr>
                      --%>
                        </tbody>
                    </table>
                          </div>

                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="addnewguest" tabindex="-1" role="dialog" aria-labelledby="addnewguest" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Record </h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Date Of Order<span style="color:red">*</span></label>
                                <input type="text" class="form-control date" id="txtDATEofOrder">
                            </div>
                            <div class="form-group">
                                <label>Guests <span style="color:red">*</span> </label>
                                <input type="text" class="form-control" id="txtGuests">
                            </div>
                            <div class="form-group">
                                <label>Visit Places<span style="color:red">*</span> </label>
                                <input type="text" class="form-control" id="txtvisitpalce">
                            </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group">

                                <label>From Date<span style="color:red">*</span> </label>
                                <input type="text" class="form-control date" id="txtFDate">

                               
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">

                                 <label>To Date <span style="color:red">*</span></label>
                                <input type="text" class="form-control date" id="txtTDate">
                            </div>
                        </div>

                            
                        <div class="col-md-12">
                             <div class="form-group">
                               <label>Remarks</label>
                                <input type="text" id="txtremark" class="form-control" >
                            </div>

                        </div>
                           

                        
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success btn-sm" type="button" id="btnsubmit">Save </button>
                     <button class="btn btn-success btn-sm" type="button" id="btnUpdate">Update </button>
                </div>
            </div>
        </div>
    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script src="../JavaScripts/StateGuestJs.js?id=<%=ConfigurationSettings.AppSettings["Version"].ToString()%>"></script>
    

</asp:Content>
